<?php include("../app/inc/set_quiz.php");

$ans = (isset($_POST['answer']))?$_POST['answer']:1;

$resultData = array(
    1 => array(
        'ans' => 'false',
        'rank' => -1,
        'point' => -1,
    ),
    2 => array(
        'ans' => 'true',
        'rank' => 5,
        'point' => 1,
    ),
    3 => array(
        'ans' => 'true',
        'rank' => 1,
        'point' => 10000,
    ),
    4 => array(
        'ans' => 'true',
        'rank' => -1,
        'point' => -1,
    ),
);
?><!DOCTYPE html>
<html lang="ja_JP">
<head>
<?php inc("quiz_head");?>
</head>
<body class="index">
    <!-- js-t-wrapper -->
    <div class="js-t-wrapper">

        <!-- header -->
        <?php inc("header");?>
        <!-- /header -->

        <!-- ad -->
        <div class="t-ad--sp">
        <?php inc("adSP_android");?>
        </div>
        <!-- /ad -->

        <!-- contens -->
        <div class="gq-contens">

            <!-- main contents (with bg)-->
            <div class="gq-contents-body">

                <!-- main title -->
                <section class="gq-practice-title">
                    <h2 class="gq-hidden-text">auゲームQuiz</h2>
                </section>
                <!-- /main title -->

                <!-- peogress -->
                <div class="gq-progress">
                    <!--
                        以下3つのうちどれか一つが表示される
                    -->
                    <div class="gq-progress1 gq-hidden-text" style="display: none;">練習問題1</div>
                    <div class="gq-progress2 gq-hidden-text">練習問題2</div>
                    <div class="gq-progress3 gq-hidden-text" style="display: none;">練習問題3</div>
                </div>
                <!-- /progress -->

                <!-- main body -->
                <section class="gq-quiz-main">
                    <div class="gq-quiz-main__inner">
                        <div class="gq-title__text-q">

                            <img class="gq-title__appicon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png" alt="モンスターストライク" width="30">
                            <p class="gq-title_appname">モンスターストライク</p>

                        </div>
                        <div class="gq-quiz-main__title">
                            <div class="gq-quiz_result">

                                <!-- アニメーション枠 -->
                                <div class="gq-quiz__result-frame"></div>
                                <!-- /アニメーション枠 -->

                            </div>
                        </div>
                        <div class="gq-quiz-main__body">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/icon_q.png" width="74" alt="Q">
                            <p class="gq-quiz-main__body-text">
                                モンストのチュートリアルで貰える最初のモンスター<br>
                                その属性は三種類ですがその種類はどの組み合わせは？
                            </p>
                        </div>
                        <!--
                            以下2つのうちどれか一つが表示される
                        -->
                        <!-- 画像パターン -->
                        <div class="gq-quiz-main__body" style="display:none;">
                            <img class="gq-quiz-main__figure" src="<?php echo $PATH['url_game'];?>img/quiz/monst.png" alt="モンスターストライク">
                        </div>
                        <!-- /画像パターン -->
                        <!-- 動画パターン -->
                        <div class="gq-quiz-main__body gq-quiz-main__movie">

                            <script> var srcUrl = encodeURIComponent('https://www.youtube.com/embed/ilHdHsEC1-o');
                            document.write('<iframe width="280" height="158" src="./fiframe.html?u=' + srcUrl + '" class="gq-quiz-main__fiframe"></iframe>')
                            </script>

                        </div>
                        <!-- /動画パターン -->

                        <div class="gq-quiz-main__line"></div>
                        <!--
                            以下4つのうちどれか一つが表示される
                        -->
                        <!-- 解答パターン 動画の上に文章 -->
                        <div class="gq-quiz-main__body" style="display: none;">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/icon-a.png" width="74" alt="A">
                            <p class="gq-quiz-main__body-text">
                                組み合わせは「②光闇火」となります。
                                <span class="gq-quiz-main__body-desc">
                                    ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト
                                </span>
                            </p>
                        </div>
                        <div class="gq-quiz-main__body gq-quiz-main__movie" style="display: none;">

                            <script> var srcUrl = encodeURIComponent('https://www.youtube.com/embed/ilHdHsEC1-o');
                            document.write('<iframe width="280" height="158" src="./fiframe.html?u=' + srcUrl + '" class="gq-quiz-main__fiframe"></iframe>')
                            </script>

                        </div>
                        <!-- /解答パターン 動画の上に文章 -->
                        <!-- 解答パターン 動画の下に文章 -->
                        <div class="gq-quiz-main__body">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/icon-a.png" width="74" alt="A">
                            <p class="gq-quiz-main__body-text">
                                組み合わせは「②光闇火」となります。
                            </p>
                        </div>
                        <div class="gq-quiz-main__body gq-quiz-main__movie">

                            <script> var srcUrl = encodeURIComponent('https://www.youtube.com/embed/kH36bA09xAA');
                            document.write('<iframe width="280" height="158" src="./fiframe.html?u=' + srcUrl + '" class="gq-quiz-main__fiframe"></iframe>')
                            </script>

                        </div>
                        <div class="gq-quiz-main__body">
                            <p class="gq-quiz-main__body-text">
                                <span class="gq-quiz-main__body-desc">
                                    ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト
                                </span>
                            </p>
                        </div>
                        <!-- /解答パターン 動画の下に文章 -->
                        <!-- 解答パターン 画像の上に文章 -->
                        <div class="gq-quiz-main__body" style="display: none;">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/icon-a.png" width="74" alt="A">
                            <p class="gq-quiz-main__body-text">
                                組み合わせは「②光闇火」となります。
                                <span class="gq-quiz-main__body-desc">
                                    ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト
                                </span>
                            </p>
                        </div>
                        <div class="gq-quiz-main__body" style="display: none;">

                            <img class="gq-quiz-main__figure" src="<?php echo $PATH['url_game'];?>img/quiz/monst.png" alt="モンスターストライク">

                        </div>
                        <!-- /解答パターン 画像の上に文章 -->
                        <!-- 解答パターン 画像の下に文章 -->
                        <div class="gq-quiz-main__body" style="display: none;">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/icon-a.png" width="74" alt="A">
                            <p class="gq-quiz-main__body-text">
                                組み合わせは「②光闇火」となります。
                            </p>
                        </div>
                        <div class="gq-quiz-main__body" style="display: none;">

                            <img class="gq-quiz-main__figure" src="<?php echo $PATH['url_game'];?>img/quiz/monst.png" alt="モンスターストライク">

                        </div>
                        <div class="gq-quiz-main__body" style="display: none;">
                            <p class="gq-quiz-main__body-text">
                                <span class="gq-quiz-main__body-desc">
                                    ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト
                                </span>
                            </p>
                        </div>
                        <!-- /解答パターン 画像の下に文章 -->
                        <div class="gq-btn__next">
                            <a class="gq-btn__next-link" href="#">
                                <img src="<?php echo $PATH['url_game'];?>img/quiz/btn_next.png" width="215" alt="次の練習問題へ">
                            </a>
                        </div>
                        <div class="gq-btn__challenge-prod">

                            <a href="question.php" class="gq-btn__challenge-item" title="このクイズに挑戦!" onclick="trEventBe(this,'auゲーム','クイズ練習結果','モンスターストライク_本番クイズへ',event);">
                                <div class="gq-btn__challenge-wrapper">
                                    <div class="gq-btn__challenge-left"></div>
                                    <div class="gq-btn__challenge-center"></div>
                                    <div class="gq-btn__challenge-right"></div>
                                </div>
                                <img class="gq-btn__challenge-text" src="<?php echo $PATH['url_game'];?>img/quiz/btn_challenge02.png" width="133">
                            </a>

                        </div>
                        <div class="gq-quiz-main__bottom">

                            <!-- play game button -->
                            <a href="./" class="gq-btn__base gq-btn__play" onclick="trEventBe(this,'auゲーム','クイズ練習結果','モンスターストライク_詳細',event);">
                                <span class="gq-btn__text">
                                    モンスターストライクで遊ぶ
                                </span>
                            </a>
                            <!-- /play game button -->

                            <!-- gametop button -->
                            <a href="./" class="gq-btn__toquiztop gq-btn__base" onclick="trEventBe(this,'auゲーム','クイズ練習結果','クイズトップ',event);">
                                <span class="gq-btn__text">auゲームQuizトップへ戻る</span>
                            </a>
                            <!-- /gametop button -->

                        </div>
                    </div>
                </section>
                <!-- /main body -->

                <!-- check it up -->
                <?php include("../app/inc/quiz_checkitup.php");?>
                <!-- check it up -->

                <!-- point -->
                <section>
                <?php inc("quiz_point");?>
                </section>
                <!-- /point -->

                <!-- terms -->
                <section>
                <?php inc("quiz_terms");?>
                </section>
                <!-- /terms -->

            </div>
            <!-- /main contents (with bg)-->

            <!-- bottom -->
            <section class="gq-bottom">
            <?php inc("quiz_bottom");?>
            <?php inc("quiz_gametop");?>
            </section>
            <!-- /bottom -->

        </div>
        <!-- /contens -->

        <!-- ad -->
        <div class="gq-ad--rect">
            <?php inc("adRect_android");?>
        </div>
        <!-- /ad -->

        <!-- footer -->
        <?php inc("footer");?>
        <!-- /footer -->

    </div>
    <!-- /js-t-wrapper -->

    <?php inc("quiz_script");?>
    <script src="<?php echo $PATH['url_game'];?>js/jquery.a3d.min.js"></script>
    <script src="<?php echo $PATH['url_game'];?>js/game_quiz_anim_prac.min.js"></script>

    <script>
    /**
     * 結果データ
     *
     * @param {boolean} ans      解答結果      正解：true、不正解：false
     */
    var resultData = {
        'ans': <?php echo $resultData[$ans]['ans'];?>
    };
    </script>

    <?php inc("gtm");?>

</body>
</html>
