<?php include("../app/inc/set_quiz.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
<?php inc("quiz_head");?>
</head>
<body class="index">
    <!-- js-t-wrapper -->
    <div class="js-t-wrapper">

        <!-- header -->
        <?php inc("header");?>
        <!-- /header -->

        <!-- ad -->
        <div class="t-ad--sp">
        <?php inc("adSP_android");?>
        </div>
        <!-- /ad -->

        <!-- contens -->
        <div class="gq-contens">

            <!-- main contents (with bg)-->
            <div class="gq-contents-body">

                <!-- main title -->
                <section class="gq-error-title">
                    <h2 class="gq-hidden-text">auゲームQuiz</h2>
                    <div class="gq-error-title__status">
                        <p class="gq-error-title__text">エラー</p>
                    </div>
                </section>
                <!-- /main title -->

                <!-- main body -->
                <section class="gq-error-main">
                    <div class="gq-error-main__inner">
                        <div class="gq-error-main__top">

                            <h3 class="gq-error-main__title">通信エラーが発生いたしました。</h3>
                            <div class="gq-error-main__message">
                                <p>ポイントの当選結果については、<a href="#" class="gq-link">ポイント獲得・利用履歴ページ</a>でご確認ください。ポイント獲得履歴にauゲームQuizに回答した日のポイントが付与されていない場合は、ハズレとなります。</p>
                            </div>
                            <div class="gq-error-main__network"></div>

                        </div>
                        <div class="gq-error-main__bottom">

                            <!-- gametop button -->
                            <a href="./" class="gq-btn__toquiztop gq-btn__base" onclick="trEventBe(this,'auゲーム','ゲームトップ','auゲームQuizトップ',event);">
                                <span class="gq-btn__text">auゲームQuizトップへ戻る</span>
                            </a>
                            <!-- /gametop button -->

                        </div>
                    </div>
                </section>
                <!-- /main body -->
            </div>
            <!-- /main contents (with bg)-->

            <!-- bottom -->
            <section class="gq-bottom">
            <?php inc("quiz_gametop");?>
            </section>
            <!-- /bottom -->

        </div>
        <!-- /contens -->

        <!-- ad -->
        <div class="gq-ad--rect">
            <?php inc("adRect_android");?>
        </div>
        <!-- /ad -->

        <!-- footer -->
        <?php inc("footer");?>
        <!-- /footer -->

    </div>
    <!-- /js-t-wrapper -->

    <?php inc("quiz_script");?>
    <?php inc("gtm");?>

</body>
</html>
