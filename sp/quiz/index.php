<?php include("../app/inc/set_quiz.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
<?php inc("quiz_head");?>
<style>
.js-t-drawer__bot {
    visibility: hidden;
}
</style>
</head>
<body class="index">

    <!-- js-t-wrapper -->
    <div class="js-t-wrapper">

        <!-- header -->
        <?php inc("header");?>
        <!-- /header -->

        <!-- ad -->
        <div class="t-ad--sp">
        <?php inc("adSP_android");?>
        </div>
        <!-- /ad -->

        <!-- contens -->
        <div class="gq-contens">

            <!-- main contents (with bg)-->
            <div class="gq-contents-body">

                <!-- ローディング用オーバーレイ -->
                <div class="gq-loading js-gq-loading">
                    <img class="gq-loading__image" src="<?php echo $PATH['url_game'];?>img/quiz/loading.gif">
                </div>
                <!-- /ローディング用オーバーレイ -->

                <!-- main title -->
                <section class="gq-main-title">
                    <h2 class="gq-hidden-text">auゲームQuiz</h2>
                    <!--
                        以下3つのうちどれか一つが表示される
                    -->
                    <!-- ログインしてる時 -->
                    <div class="gq-main-title__inner--login">
                        <img src="<?php echo $PATH['url_game'];?>img/quiz/top_main_login.png" alt="毎日出題されるクイズに正解すると抽選でポイントプレゼント！チャンスは1日1問。あなたはどのゲームでチャレンジする!?">
                    </div>
                    <!-- /ログインしてる時 -->
                    <!-- プレイ不可(auIdが法人)の時 -->
                    <div class="gq-main-title__inner--notallowed" style="display: none;">
                        <img src="<?php echo $PATH['url_game'];?>img/quiz/top_main_notallowed.png" alt="お客様がご利用のau IDは、auゲームQuizをご利用いただけません。auゲームQuizのご利用には個人契約のau携帯・固定サービスが登録されたau IDが必要です。">
                    </div>
                    <!-- /プレイ不可(auIdが法人)の時 -->
                    <!-- ログインしてない時 -->
                    <div class="gq-main-title__inner--logout-bg" style="display: none;">
                        <div class="gq-main-title__inner gq-main-title__inner--logout">
                            <h2 class="gq-hidden-text">ログインして今すぐゲームを始めよう！</h2>
                            <div class="gq-btn__main-login">
                                <a href="https://connect.auone.jp/net/vwc/cca_lg_eu_nets/login?targeturl=https%3A%2F%2Fgame.auone.jp%2Fquiz" onclick="trEventBe(this,'auゲーム','クイズトップ','ログインボタン',event);">
                                    <img src="<?php echo $PATH['url_game'];?>img/quiz/btn_login.png" width="188" alt="au ID ログイン"></a>
                            </div>
                        </div>
                    </div>
                    <!-- /ログインしてない時 -->
                </section>
                <!-- /main title-->

                <!-- quiz list -->
                <section>

                    <!-- heading quiz list-->
                    <div class="gq-title__text">
                        <h2 class="gq-title__quiz-list gq-hidden-text">クイズ一覧</h2>
                    </div>
                    <!-- /heading quiz list-->

                    <!-- list -->
                    <div class="gq-quiz-list">
                        <ul>
                            <li>
                                <!-- 汎用 -->
                                <div class="gq-quiz-list__appicon">
                                    <img src="<?php echo $PATH['url_game'];?>img/quiz/icon_nongenre.png" alt="汎用" width="55">
                                </div>
                                <div class="gq-quiz-list__desc">
                                    <p>汎用アイコン案</p>
                                    <div class="gq-quiz-list__btns">
                                        <div class="gq-quiz-list__btn-quiz">

                                            <a href="question.php" class="gq-btn__challenge-item" title="このクイズに挑戦!" onclick="trEventBe(this,'auゲーム','クイズトップ','挑戦問題_汎用',event);">
                                                <div class="gq-btn__challenge-wrapper">
                                                    <div class="gq-btn__challenge-left"></div>
                                                    <div class="gq-btn__challenge-center"></div>
                                                    <div class="gq-btn__challenge-right"></div>
                                                </div>
                                                <img class="gq-btn__challenge-text" src="<?php echo $PATH['url_game'];?>img/quiz/btn_challenge.png" width="117">
                                            </a>

                                        </div>
                                        <div class="gq-quiz-list__btn-practice">

                                            <a href="practice.php" class="gq-btn__practice" title="練習問題" onclick="trEventBe(this,'auゲーム','クイズトップ','練習問題_汎用',event);">
                                                <div class="gq-btn__practice-wrapper">
                                                    <div class="gq-btn__practice-left"></div>
                                                    <div class="gq-btn__practice-center"></div>
                                                    <div class="gq-btn__practice-right"></div>
                                                </div>
                                                <img class="gq-btn__practice-text" src="<?php echo $PATH['url_game'];?>img/quiz/btn_practice.png" width="31">
                                            </a>

                                        </div>
                                    </div>
                                </div>
                                <!-- /汎用 -->
                            </li>
                            <li>
                                <!-- モンスターストライク -->
                                <div class="gq-quiz-list__appicon">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png" alt="モンスターストライク" width="55">
                                </div>
                                <div class="gq-quiz-list__desc">
                                    <p>モンスターストライク</p>
                                    <div class="gq-quiz-list__btns">
                                        <div class="gq-quiz-list__btn-quiz">

                                            <a href="question.php" class="gq-btn__challenge-item" title="このクイズに挑戦!" onclick="trEventBe(this,'auゲーム','クイズトップ','挑戦問題_モンスターストライク',event);">
                                                <div class="gq-btn__challenge-wrapper">
                                                    <div class="gq-btn__challenge-left"></div>
                                                    <div class="gq-btn__challenge-center"></div>
                                                    <div class="gq-btn__challenge-right"></div>
                                                </div>
                                                <img class="gq-btn__challenge-text" src="<?php echo $PATH['url_game'];?>img/quiz/btn_challenge.png" width="117">
                                            </a>

                                        </div>
                                        <div class="gq-quiz-list__btn-practice">

                                            <a href="practice.php" class="gq-btn__practice" title="練習問題" onclick="trEventBe(this,'auゲーム','クイズトップ','練習問題_モンスターストライク',event);">
                                                <div class="gq-btn__practice-wrapper">
                                                    <div class="gq-btn__practice-left"></div>
                                                    <div class="gq-btn__practice-center"></div>
                                                    <div class="gq-btn__practice-right"></div>
                                                </div>
                                                <img class="gq-btn__practice-text" src="<?php echo $PATH['url_game'];?>img/quiz/btn_practice.png" width="31">
                                            </a>

                                        </div>
                                    </div>
                                </div>
                                <!-- /モンスターストライク -->
                            </li>
                            <li>
                                <!-- 僕らの甲子園!ポケット for auスマートパス -->
                                <div class="gq-quiz-list__appicon">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4526600000001.png" alt="僕らの甲子園!ポケット for auスマートパス" width="55">
                                </div>
                                <div class="gq-quiz-list__desc">
                                    <p>僕らの甲子園!ポケット for auスマートパス</p>
                                    <div class="gq-quiz-list__btns">
                                        <div class="gq-quiz-list__btn-quiz">

                                            <a href="question.php" class="gq-btn__challenge-item" title="このクイズに挑戦!" onclick="trEventBe(this,'auゲーム','クイズトップ','挑戦問題_僕らの甲子園!ポケット for auスマートパス',event);">
                                                <div class="gq-btn__challenge-wrapper">
                                                    <div class="gq-btn__challenge-left"></div>
                                                    <div class="gq-btn__challenge-center"></div>
                                                    <div class="gq-btn__challenge-right"></div>
                                                </div>
                                                <img class="gq-btn__challenge-text" src="<?php echo $PATH['url_game'];?>img/quiz/btn_challenge.png" width="117">
                                            </a>

                                        </div>
                                        <div class="gq-quiz-list__btn-practice">

                                            <a href="practice.php" class="gq-btn__practice" title="練習問題" onclick="trEventBe(this,'auゲーム','クイズトップ','練習問題_僕らの甲子園!ポケット for auスマートパス',event);">
                                                <div class="gq-btn__practice-wrapper">
                                                    <div class="gq-btn__practice-left"></div>
                                                    <div class="gq-btn__practice-center"></div>
                                                    <div class="gq-btn__practice-right"></div>
                                                </div>
                                                <img class="gq-btn__practice-text" src="<?php echo $PATH['url_game'];?>img/quiz/btn_practice.png" width="31">
                                            </a>

                                        </div>
                                    </div>
                                </div>
                                <!-- /僕らの甲子園!ポケット for auスマートパス -->
                            </li>
                            <li>
                                <!-- 魔法使いと黒猫のウィズ -->
                                <div class="gq-quiz-list__appicon">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000022.png" alt="魔法使いと黒猫のウィズ" width="55">
                                </div>
                                <div class="gq-quiz-list__desc">
                                    <p>魔法使いと黒猫のウィズ</p>
                                    <div class="gq-quiz-list__btns">
                                        <div class="gq-quiz-list__btn-quiz">

                                            <p class="gq-btn__challenge--disabled">このクイズに挑戦!</p>

                                        </div>
                                        <div class="gq-quiz-list__btn-practice">

                                            <a href="practice.php" class="gq-btn__practice" title="練習問題" onclick="trEventBe(this,'auゲーム','クイズトップ','練習問題_魔法使いと黒猫のウィズ',event);">
                                                <div class="gq-btn__practice-wrapper">
                                                    <div class="gq-btn__practice-left"></div>
                                                    <div class="gq-btn__practice-center"></div>
                                                    <div class="gq-btn__practice-right"></div>
                                                </div>
                                                <img class="gq-btn__practice-text" src="<?php echo $PATH['url_game'];?>img/quiz/btn_practice.png" width="31">
                                            </a>

                                        </div>
                                    </div>
                                </div>
                                <!-- /魔法使いと黒猫のウィズ -->
                            </li>
                            <li>
                                <!-- 星の島のにゃんこ -->
                                <div class="gq-quiz-list__appicon">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000039.png" alt="星の島のにゃんこ" width="55">
                                </div>
                                <div class="gq-quiz-list__desc">
                                    <p>星の島のにゃんこ</p>
                                    <div class="gq-quiz-list__btns">
                                        <div class="gq-quiz-list__btn-quiz">

                                            <p class="gq-btn__challenge--disabled">今日は回答済みです</p>

                                        </div>
                                        <div class="gq-quiz-list__btn-practice">

                                            <a href="practice.php" class="gq-btn__practice" title="練習問題" onclick="trEventBe(this,'auゲーム','クイズトップ','練習問題_星の島のにゃんこ',event);">
                                                <div class="gq-btn__practice-wrapper">
                                                    <div class="gq-btn__practice-left"></div>
                                                    <div class="gq-btn__practice-center"></div>
                                                    <div class="gq-btn__practice-right"></div>
                                                </div>
                                                <img class="gq-btn__practice-text" src="<?php echo $PATH['url_game'];?>img/quiz/btn_practice.png" width="31">
                                            </a>

                                        </div>
                                    </div>
                                </div>
                                <!-- /星の島のにゃんこ -->
                            </li>
                        </ul>
                    </div>
                    <!-- /list -->

                </section>
                <!-- /quiz list -->

                <!-- point -->
                <section>
                <?php inc("quiz_point");?>
                </section>
                <!-- /point -->

                <!-- tutorial banner-->
                <div class="gq-banner--tutorial js-gq-banner--tutorial">
                    <a href="../app/quiz/tutorial/" onclick="trEventBe(this,'auゲーム','クイズトップ','チュートリアルバナー',event);">
                        <img src="<?php echo $PATH['url_game'];?>img/quiz/tutorial_bnr.png" alt="auゲームクイズの遊び方" width="288">
                    </a>
                </div>
                <!-- tutorial banner-->

                <!-- terms -->
                <section>
                <?php inc("quiz_terms");?>
                </section>
                <!-- /terms -->

            </div>
            <!-- /main contents (with bg)-->

            <!-- bottom -->
            <section class="gq-bottom">
            <?php inc("quiz_bottom");?>
            <?php inc("quiz_gametop");?>
            </section>
            <!-- /bottom -->

        </div>
        <!-- /contens -->

        <!-- ad -->
        <div class="gq-ad--rect">
            <?php inc("adRect_android");?>
        </div>
        <!-- /ad -->

        <!-- footer -->
        <?php inc("footer");?>
        <!-- /footer -->

    </div>
    <!-- /js-t-wrapper -->

    <?php inc("quiz_script");?>
    <?php inc("gtm");?>

<script>
    setTimeout(function() {
        clearOverlay();
    }, 3000);
</script>

</body>
</html>
