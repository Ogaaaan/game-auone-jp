<?php include("../app/inc/set_quiz.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
<?php inc("quiz_head");?>
</head>
<body class="index">
    <!-- js-t-wrapper -->
    <div class="js-t-wrapper">

        <!-- header -->
        <?php inc("header");?>
        <!-- /header -->

        <!-- ad -->
        <div class="t-ad--sp">
        <?php inc("adSP_android");?>
        </div>
        <!-- /ad -->

        <!-- contens -->
        <div class="gq-contens">

            <!-- main contents (with bg)-->
            <div class="gq-contents-body">

                <!-- main title -->
                <section class="gq-practice-title">
                    <h2 class="gq-hidden-text">auゲームQuiz</h2>
                </section>
                <!-- /main title -->

                <!-- peogress -->
                <div class="gq-progress">
                    <!--
                        以下3つのうちどれか一つが表示される
                    -->
                    <div class="gq-progress1 gq-hidden-text" style="display: none;">練習問題1</div>
                    <div class="gq-progress2 gq-hidden-text">練習問題2</div>
                    <div class="gq-progress3 gq-hidden-text" style="display: none;">練習問題3</div>
                </div>
                <!-- /progress -->

                <!-- main body -->
                <section class="gq-quiz-main">
                    <div class="gq-quiz-main__inner">
                        <div class="gq-title__text-q">

                            <img class="gq-title__appicon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png" alt="モンスターストライク" width="30">
                            <p class="gq-title_appname">モンスターストライク</p>

                        </div>
                        <div class="gq-quiz-main__body">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/icon_q.png" width="74" alt="Q">
                            <p class="gq-quiz-main__body-text">
                                モンストのチュートリアルで貰える最初のモンスター<br>
                                その属性は三種類ですがその種類はどの組み合わせは？
                            </p>
                        </div>
                        <!--
                            以下2つのうちどれか一つが表示される
                        -->
                        <!-- 画像パターン -->
                        <div class="gq-quiz-main__body" style="display:none;">
                            <img class="gq-quiz-main__figure" src="<?php echo $PATH['url_game'];?>img/quiz/monst.png" alt="モンスターストライク">
                        </div>
                        <!-- /画像パターン -->
                        <!-- 動画パターン -->
                        <div class="gq-quiz-main__body gq-quiz-main__movie">

                            <script> var srcUrl = encodeURIComponent('https://www.youtube.com/embed/ilHdHsEC1-o');
                            document.write('<iframe width="280" height="158" src="./fiframe.html?u=' + srcUrl + '" class="gq-quiz-main__fiframe"></iframe>')
                            </script>

                        </div>
                        <!-- /動画パターン -->

                        <div>

                            <form action="./practice_result.php" method="post">

                            <ul class="gq-quiz-main__answers">
                                <li>
                                    <label class="gq-btn__ans-base gq-btn__ans-1">
                                        <input type="radio" name="answer" value="1" class="gq-hidden-form js-gq-hidden-form">火水光
                                        <i class="gq-btn__ans-icon"></i>
                                    </label>
                                </li>
                                <li>
                                    <label class="gq-btn__ans-base gq-btn__ans-2">
                                        <input type="radio" name="answer" value="2" class="gq-hidden-form js-gq-hidden-form">光闇火
                                        <i class="gq-btn__ans-icon"></i>
                                    </label>
                                </li>
                                <li>
                                    <label class="gq-btn__ans-base gq-btn__ans-3">
                                        <input type="radio" name="answer" value="3" class="gq-hidden-form js-gq-hidden-form">水木闇
                                        <i class="gq-btn__ans-icon"></i>
                                    </label>
                                </li>
                                <li>
                                    <label class="gq-btn__ans-base gq-btn__ans-4">
                                        <input type="radio" name="answer" value="4" class="gq-hidden-form js-gq-hidden-form">つまり、上の条件を満たすように左右マージンを計算すると、左右中央に配置されます値という話。
                                        <i class="gq-btn__ans-icon"></i>
                                    </label>
                                </li>
                            </ul>

                            <div class="gq-quiz-main__submit">
                                <img class="gq-btn__ans-submit--disabled" src="<?php echo $PATH['url_game'];?>img/quiz/btn_ans_disabled.png" width="216" alt="これで解答する">
                                <input class="gq-btn__ans-submit" type="image" src="<?php echo $PATH['url_game'];?>img/quiz/btn_ans.png" width="216" onclick="trEventBe(this,'auゲーム','クイズ練習','モンスターストライク_回答',event);" alt="これで解答する">
                            </div>

                            </form>

                        </div>
                        <div class="gq-quiz-main__bottom">

                            <!-- gametop button -->
                            <a href="./" class="gq-btn__base gq-btn__hint" onclick="trEventBe(this,'auゲーム','クイズ練習','モンスターストライク_ヒント',event);">
                                <span class="gq-btn__text gq-btn__text--small">分からなければこちらでチェック</span>
                            </a>
                            <!-- /gametop button -->

                        </div>
                    </div>
                </section>
                <!-- /main body -->

                <!-- point -->
                <section>
                <?php inc("quiz_point");?>
                </section>
                <!-- /point -->

                <!-- terms -->
                <section>
                <?php inc("quiz_terms");?>
                </section>
                <!-- /terms -->

            </div>
            <!-- /main contents (with bg)-->

            <!-- bottom -->
            <section class="gq-bottom">
            <?php inc("quiz_bottom");?>
            <?php inc("quiz_gametop");?>
            </section>
            <!-- /bottom -->

        </div>
        <!-- /contens -->

        <!-- ad -->
        <div class="gq-ad--rect">
            <?php inc("adRect_android");?>
        </div>
        <!-- /ad -->

        <!-- footer -->
        <?php inc("footer");?>
        <!-- /footer -->

    </div>
    <!-- /js-t-wrapper -->

    <?php inc("quiz_script");?>
    <?php inc("gtm");?>

</body>
</html>
