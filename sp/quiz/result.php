<?php include("../app/inc/set_quiz.php");

$ans = (isset($_POST['answer']))?$_POST['answer']:1;

$resultData = array(
    1 => array(
        'ans' => 'false',
        'rank' => -1,
        'point' => -1,
    ),
    2 => array(
        'ans' => 'true',
        'rank' => 5,
        'point' => 1,
    ),
    3 => array(
        'ans' => 'true',
        'rank' => 1,
        'point' => 10000,
    ),
    4 => array(
        'ans' => 'true',
        'rank' => -1,
        'point' => -1,
    ),
);
?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
<script>var createjs = window;</script>
<?php inc("quiz_head");?>
</head>
<body class="index">
    <!-- js-t-wrapper -->
    <div class="js-t-wrapper">

        <!-- header -->
        <?php inc("header");?>
        <!-- /header -->

        <!-- ad -->
        <div class="t-ad--sp">
        <?php inc("adSP_android");?>
        </div>
        <!-- /ad -->

        <!-- contens -->
        <div class="gq-contens">

            <!-- main contents (with bg)-->
            <div class="gq-contents-body">

                <!-- main title -->
                <section class="gq-question-title">
                    <h2 class="gq-hidden-text">auゲームQuiz</h2>
                </section>
                <!-- /main title -->

                <!-- main body -->
                <section class="gq-quiz-main">
                    <div class="gq-quiz-main__inner">
                        <div class="gq-title__text-q">

                            <img class="gq-title__appicon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png" alt="モンスターストライク" width="30">
                            <p class="gq-title_appname">モンスターストライク</p>

                        </div>
                        <div class="gq-quiz-main__body">
                            <div class="gq-quiz_result">

                                <!-- 抽選のお知らせ(正解の時のみ表示)-->
                                <img src="<?php echo $PATH['url_game'];?>img/quiz/messageboard01.gif"  width="280" height="50" alt="ポイントゲットチャ〜ンス♪" class="gq-quiz__lottery_bnr">
                                <img src="<?php echo $PATH['url_game'];?>img/quiz/messageboard02.gif"  width="280" height="50" alt="今日のチャレンジクイズはここまで!" class="gq-quiz__challenge_bnr" style="display: none">

                                <!-- /抽選のお知らせ(正解の時のみ表示)-->

                                <!-- アニメーション枠 -->
                                <div class="gq-quiz__result-frame"></div>
                                <!-- /アニメーション枠 -->

                            </div>
                        </div>
                        <div class="gq-quiz-main__body">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/icon_q.png" width="74" alt="Q">
                            <p class="gq-quiz-main__body-text">
                                モンストのチュートリアルで貰える最初のモンスター<br>
                                その属性は三種類ですがその種類はどの組み合わせは？
                            </p>
                        </div>
                        <!--
                            以下2つのうちどれか一つが表示される
                        -->
                        <!-- 画像パターン -->
                        <div class="gq-quiz-main__body" style="display:none;">
                            <img class="gq-quiz-main__figure" src="<?php echo $PATH['url_game'];?>img/quiz/monst.png" alt="モンスターストライク">
                        </div>
                        <!-- /画像パターン -->
                        <!-- 動画パターン -->
                        <div class="gq-quiz-main__body gq-quiz-main__movie">

                            <script> var srcUrl = encodeURIComponent('https://www.youtube.com/embed/ilHdHsEC1-o');
                            document.write('<iframe width="280" height="158" src="./fiframe.html?u=' + srcUrl + '" class="gq-quiz-main__fiframe"></iframe>')
                            </script>

                        </div>
                        <!-- /動画パターン -->
                        <div class="gq-quiz-main__line"></div>
                        <!--
                            以下4つのうちどれか一つが表示される
                        -->
                        <!-- 解答パターン 動画の上に文章 -->
                        <div class="gq-quiz-main__body" style="display: none;">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/icon-a.png" width="74" alt="A">
                            <p class="gq-quiz-main__body-text">
                                組み合わせは「②光闇火」となります。
                                <span class="gq-quiz-main__body-desc">
                                    ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト
                                </span>
                            </p>
                        </div>
                        <div class="gq-quiz-main__body gq-quiz-main__movie" style="display: none;">

                            <script> var srcUrl = encodeURIComponent('https://www.youtube.com/embed/ilHdHsEC1-o');
                            document.write('<iframe width="280" height="158" src="./fiframe.html?u=' + srcUrl + '" class="gq-quiz-main__fiframe"></iframe>')
                            </script>

                        </div>
                        <!-- /解答パターン 動画の上に文章 -->
                        <!-- 解答パターン 動画の下に文章 -->
                        <div class="gq-quiz-main__body" style="display: none;">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/icon-a.png" width="74" alt="A">
                            <p class="gq-quiz-main__body-text">
                                組み合わせは「②光闇火」となります。
                            </p>
                        </div>
                        <div class="gq-quiz-main__body gq-quiz-main__movie" style="display: none;">

                            <script> var srcUrl = encodeURIComponent('https://www.youtube.com/embed/ilHdHsEC1-o');
                            document.write('<iframe width="280" height="158" src="./fiframe.html?u=' + srcUrl + '" class="gq-quiz-main__fiframe"></iframe>')
                            </script>

                        </div>
                        <div class="gq-quiz-main__body" style="display: none;">
                            <p class="gq-quiz-main__body-text">
                                <span class="gq-quiz-main__body-desc">
                                    ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト
                                </span>
                            </p>
                        </div>
                        <!-- /解答パターン 動画の下に文章 -->
                        <!-- 解答パターン 画像の上に文章 -->
                        <div class="gq-quiz-main__body">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/icon-a.png" width="74" alt="A">
                            <p class="gq-quiz-main__body-text">
                                組み合わせは「②光闇火」となります。
                                <span class="gq-quiz-main__body-desc">
                                    ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト
                                </span>
                            </p>
                        </div>
                        <div class="gq-quiz-main__body">

                            <img class="gq-quiz-main__figure" src="<?php echo $PATH['url_game'];?>img/quiz/monst.png" alt="モンスターストライク">

                        </div>
                        <!-- /解答パターン 画像の上に文章 -->
                        <!-- 解答パターン 画像の下に文章 -->
                        <div class="gq-quiz-main__body" style="display: none;">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/icon-a.png" width="74" alt="A">
                            <p class="gq-quiz-main__body-text">
                                組み合わせは「②光闇火」となります。
                            </p>
                        </div>
                        <div class="gq-quiz-main__body" style="display: none;">

                            <img class="gq-quiz-main__figure" src="<?php echo $PATH['url_game'];?>img/quiz/monst.png" alt="モンスターストライク">

                        </div>
                        <div class="gq-quiz-main__body" style="display: none;">
                            <p class="gq-quiz-main__body-text">
                                <span class="gq-quiz-main__body-desc">
                                    ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト
                                </span>
                            </p>
                        </div>
                        <!-- /解答パターン 画像の下に文章 -->

                        <div class="gq-quiz-main__bottom">

                            <!-- play game button -->
                            <a href="./" class="gq-btn__base gq-btn__play" onclick="trEventBe(this,'auゲーム','クイズ結果','モンスターストライク_詳細',event);">
                                <span class="gq-btn__text">
                                    モンスターストライクで遊ぶ
                                </span>
                            </a>
                            <!-- /play game button -->

                            <!-- gametop button -->
                            <a href="./" class="gq-btn__toquiztop gq-btn__base" onclick="trEventBe(this,'auゲーム','ゲームトップ','auゲームQuizトップ',event);">
                                <span class="gq-btn__text">auゲームQuizトップへ戻る</span>
                            </a>
                            <!-- /gametop button -->

                        </div>
                    </div>
                </section>
                <!-- /main body -->

                <!-- check it up -->
                <?php include("../app/inc/quiz_checkitup.php");?>
                <!-- check it up -->

                <!-- point -->
                <section>
                <?php inc("quiz_point");?>
                </section>
                <!-- /point -->

                <!-- terms -->
                <section>
                <?php inc("quiz_terms");?>
                </section>
                <!-- /terms -->

            </div>
            <!-- /main contents (with bg)-->

            <!-- bottom -->
            <section class="gq-bottom">
            <?php inc("quiz_bottom");?>
            <?php inc("quiz_gametop");?>
            </section>
            <!-- /bottom -->

        </div>
        <!-- /contens -->

        <!-- ad -->
        <div class="gq-ad--rect">
            <?php inc("adRect_android");?>
        </div>
        <!-- /ad -->

        <!-- footer -->
        <?php inc("footer");?>
        <!-- /footer -->

    </div>
    <!-- /js-t-wrapper -->

    <?php inc("quiz_script");?>
    <script src="<?php echo $PATH['url_game'];?>js/jquery.a3d.min.js"></script>
    <script src="<?php echo $PATH['url_game'];?>js/game_quiz_anim.min.js"></script>

    <script>
    /**
     * 結果データ
     *
     * @param {boolean} ans      解答結果      正解：true、不正解：false
     * @param {integer} rank     抽選結果      ハズレ：-1
     * @param {integer} point    獲得ポイント   ハズレ、不正解：-1
     */
    var resultData = {
        'ans': <?php echo $resultData[$ans]['ans'];?>,
        'rank': <?php echo $resultData[$ans]['rank'];?>,
        'point': <?php echo $resultData[$ans]['point'];?>,
    };
    </script>

    <?php inc("gtm");?>

</body>
</html>
