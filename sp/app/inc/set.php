<?php
// htmlへ記入するためのglobal設定
$PATH;
// url出し分け
if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false ) {
    // php builtin server (local)
    $PATH['url_game'] = '//'.$_SERVER['HTTP_HOST'].'/src/';
    $PATH['url_tentomushi'] = '//cdn-img.auone.jp/pass/asset/tentomushi/';
} else if ($_SERVER['HTTP_HOST'] === 'mediba.jpn.org') {
    // pixeltoy
    $PATH['url_game'] = '//mediba.jpn.org/game.auone.jp/src/';
    $PATH['url_tentomushi'] = '//cdn-img.auone.jp/pass/asset/tentomushi/';
} else if (strpos($_SERVER['HTTP_HOST'], 'stg.game') !== false
|| strpos($_SERVER['HTTP_HOST'], 'dev.game') !== false) {
    // dev or ステージング環境
    $PATH['url_game'] = '//cdn-img.mdev.auone.jp/pass/asset/sp/game/';
    $PATH['url_tentomushi'] = '//cdn-img.mdev.auone.jp/pass/asset/tentomushi/';
} else {
    // デフォルト商用
    $PATH['url_game'] = '//cdn-img.auone.jp/pass/asset/sp/game/';
    $PATH['url_tentomushi'] = '//cdn-img.auone.jp/pass/asset/tentomushi/';
}

function inc($file){
    global $PATH;
    include(dirname(__FILE__) . "/" . $file . ".php");
}
