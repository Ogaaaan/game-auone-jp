<!-- terms of service -->
<div class="gq-terms js-gq-terms">
    <dl>
        <dt class="gq-terms__title">参加ルール</dt>
        <dd class="gq-terms__chapter">
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>1-1</span>
                </div>
                <div class="gq-terms__desc">
                    <p>auゲームQuizは下記に該当する方ならどなたでもご参加できます。</p>
                    <ul class="gq-terms__list">
                        <li>個人名義の契約であること</li>
                        <li>スマートパス会員であること</li>
                        <li>au IDに契約回線が紐づいていること</li>
                        <li>au IDログイン履歴がある、又はau WALLET カードを持っていること</li>
                    </ul>
                </div>
            </div>
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>1-2</span>
                </div>
                <div class="gq-terms__desc">
                    <p>一つのau IDに対しクイズに答えられるのは全対象クイズで1問のみとなります。</p>
                </div>
            </div>
        </dd>
        <dt class="gq-terms__title">ポイント付与について</dt>
        <dd class="gq-terms__chapter">
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>2-1</span>
                </div>
                <div class="gq-terms__desc">
                    <p>獲得したWALLET ポイントは、応募したau IDのみに対して付与されます。</p>
                </div>
            </div>
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>2-2</span>
                </div>
                <div class="gq-terms__desc">
                    <p>ポイント付与に時間がかかる場合があります。</p>
                </div>
            </div>
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>2-3</span>
                </div>
                <div class="gq-terms__desc">
                    <p>ポイント付与の対象者はポイント付与時に参加条件1-1を満たしている必要があるため、結果画面が表示されても付与されないケースがあります。</p>
                </div>
            </div>
        </dd>
        <dt class="gq-terms__title">免責事項</dt>
        <dd class="gq-terms__chapter">
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>3-1</span>
                </div>
                <div class="gq-terms__desc">
                    <p>メンテナンスなどの関係でクイズに答えられない場合があります。その場合、参加権利を翌日に繰り越すことはできません。</p>
                </div>
            </div>
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>3-2</span>
                </div>
                <div class="gq-terms__desc">
                    <p>本サービスは予告なく終了させていただく場合があります。</p>
                </div>
            </div>
        </dd>
        <dt class="gq-terms__title">その他の注意事項</dt>
        <dd class="gq-terms__chapter">
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>4-1</span>
                </div>
                <div class="gq-terms__desc">
                    <p>当選者は厳正な抽選のうえで選出を行います。</p>
                </div>
            </div>
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>4-2</span>
                </div>
                <div class="gq-terms__desc">
                    <p>クイズに参加する権利を次回以降に繰り越すことはできません。</p>
                </div>
            </div>
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>4-3</span>
                </div>
                <div class="gq-terms__desc">
                    <p>獲得したポイント数とポイント獲得履歴は<a href="https://id.auone.jp/point/info/index.html" class="gq-link" onclick="trEventBe(this,'auゲーム','クイズ_参加ルール','ポイント履歴',event);">こちらのページ</a>からau IDでログイン後ご覧いただけます。</p>
                </div>
            </div>
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>4-4</span>
                </div>
                <div class="gq-terms__desc">
                    <p>獲得したWALLET ポイントは「1pt=1円」で以下でご利用いただけます。</p>
                    <ul>
                        <li>au WALLET カードへチャージして、世界約3,810万のMasterCard（R）加盟店※やWebMoney加盟サイトで使う</li>
                        <li>au通信料への充当に使う</li>
                        <li>各種商品やデジタルコンテンツの購入代金に使う</li>
                    </ul>
                    <p>※詳しくは<a href="https://point.auone.jp/usetop/" class="gq-link" onclick="trEventBe(this,'auゲーム','クイズ_参加ルール','ポイント利用',event);">こちらのページ</a>でご覧ください。</p>
                </div>
            </div>
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>4-5</span>
                </div>
                <div class="gq-terms__desc">
                    <p>獲得したWALLET ポイントを他のau IDへ譲渡したり、au ID間で共有することはできません。</p>
                </div>
            </div>
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>4-6</span>
                </div>
                <div class="gq-terms__desc">
                    <p>先着に関するお問い合わせにはお答えできません。</p>
                </div>
            </div>
            <div class="gq-terms__section">
                <div class="gq-terms__num">
                    <span>4-7</span>
                </div>
                <div class="gq-terms__desc">
                    <p>通信状態の影響で正しく結果ページが表示されなかった場合、ポイント獲得・利用履歴ページでご確認ください。ポイント獲得履歴にauゲームQuizに回答した日のポイントが付与されていない場合は、ハズレとなります。</p>
                </div>
            </div>
        </dd>

    </dl>
</div>
<!-- /terms of service -->

<!-- rule button -->
<div class="gq-contents-body__bottom">
<button class="gq-btn__base js-gq-btn__rule gq-btn__rule" onclick="trEventBe(this,'auゲーム','クイズトップ','参加ルール',event);">
    <span class="gq-btn__text gq-btn__arrow--down">参加ルール</span>
</button>
</div>
<!-- /rule button -->
