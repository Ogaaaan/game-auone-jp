<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-title" content="auゲーム">
<link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
<link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
<link rel="stylesheet" href="<?php print($PATH['url_tentomushi']);?>css/t.css">
<link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/bootstrap.min.css">
<link rel="stylesheet" href="<?php print($PATH['url_tentomushi']);?>css/drawer.min.css">
<link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/style.css">
<script src="//cdn-img.auone.jp/pass/asset/sp/common/js/CheckDeteredDevice.js"></script>
