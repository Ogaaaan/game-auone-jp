<meta charset="UTF-8">
<title>ゲームクイズでポイントゲット！ - auゲーム</title>
<meta name="keywords" content="ゲーム,クイズ,au WALLET,ポイント,スマートパス,おトク">
<meta name="description" content="auゲームのクイズに答えて、ポイントをゲットしよう！">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-title" content="auゲーム">
<link rel="shortcut icon" href="<?php echo $PATH['url_common'];?>img/favicon/favicon.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo $PATH['url_common'];?>img/favicon/favicon.png">
<link rel="stylesheet" href="<?php echo $PATH['url_tentomushi'];?>css/t.css">
<link rel="stylesheet" href="<?php echo $PATH['url_tentomushi'];?>css/drawer.min.css">
<link rel="stylesheet" href="<?php echo $PATH['url_game'];?>css/style.css">
<style>@import url(<?php echo $PATH['url_game'];?>css/game_quiz.min.css);</style>
<script src="<?php echo $PATH['url_common'];?>js/CheckDeteredDevice.js"></script>
