<?php
// htmlへ記入するためのglobal設定
$PATH;
// url出し分け
if ($_SERVER['HTTP_HOST'] === 'mediba.jpn.org') {

    // pixeltoy
    $PATH = array(
        // url_gameは商用では cdn-img.auone.jp/game
        'url_game' =>       '//mediba.jpn.org/game.auone.jp/src/',
        'url_tentomushi' => '//cdn-img.mdev.auone.jp/pass/asset/tentomushi/',
        'url_common' =>     '//cdn-img.mdev.auone.jp/pass/asset/sp/common/',
    );

} else if (strpos($_SERVER['HTTP_HOST'], 'dev.game') !== false) {

    // dev
    $PATH = array(
        // url_gameは商用では cdn-img.auone.jp/game
        'url_game' =>       '//cdn-img.mdev.auone.jp/game/',
        'url_tentomushi' => '//cdn-img.mdev.auone.jp/pass/asset/tentomushi/',
        'url_common' =>     '//cdn-img.mdev.auone.jp/pass/asset/sp/common/',
    );

} else if (strpos($_SERVER['HTTP_HOST'], 'stg.game') !== false) {

    // dev
    $PATH = array(
        // url_gameは商用では cdn-img.auone.jp/game
        'url_game' =>       '//stg.cdn-img.auone.jp/game/',
        'url_tentomushi' => '//stg.cdn-img.auone.jp/pass/asset/tentomushi/',
        'url_common' =>     '//stg.cdn-img.auone.jp/pass/asset/sp/common/',
    );

} else {

    // 上記以外の環境用
    $PATH = array(
        'url_game' =>       '//'.$_SERVER['HTTP_HOST'].'/src/',
        'url_tentomushi' => '//cdn-img.auone.jp/pass/asset/tentomushi/',
        'url_common' =>     '//cdn-img.auone.jp/pass/asset/sp/common/',
    );

}

function inc($file){
    global $PATH;
    include(dirname(__FILE__) . "/" . $file . ".php");
}
