<?php include("../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
<meta charset="UTF-8">
<title>初めての方へ - auゲーム</title>
<meta name="keywords" content="ゲーム,auゲーム,初めての方へ,ゲーム初心者,auゲームの遊び方,auスマートパス,ポイントプレゼント,ポイント還元,WALLETポイント" />
<meta name="description" content="auゲームの初心者向けページ。人気のゲームアプリもauゲームならau WALLETポイントが貯まる！" />

<?php inc("head"); ?>

<link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/game-howto.css">

</head>

<body class="all-app">
<div class="js-t-wrapper">
<div class="contens-body">

<?php inc("header"); ?>
<?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all_wrapper">
        <div class="game-contents_wrapper">
            <div class="howto_mainimg">
                <h2>auゲームとは？</h2>
            </div>
            <nav>
                <ul class="howto_nav">
                    <li><a href="#merit" onclick="trEventBe(this,'auゲーム','初めての方へ','ナビボタン左',event);"><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto/nav_howto01.png" width="74" height="30" alt="auゲームでできること"></a></li>
                    <li><a href="#use" onclick="trEventBe(this,'auゲーム','初めての方へ','ナビボタン中',event);"><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto/nav_howto02.png" width="74" height="30" alt="つかいかた"></a></li>
                    <li><a href="#play" onclick="trEventBe(this,'auゲーム','初めての方へ','ナビボタン右',event);"><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto/nav_howto03.png" width="100" height="30" alt="とってもおトクなあそびかた"></a></li>
                </ul>
            </nav>
            <div class="game-contents" id="merit">
                <h3 class="merit_title">auゲームでできること</h3>
                <div class="game-contents incontents">
                    <ul>
                        <li><a class="description" href="https://game.auone.jp/ranking?category_id=001" onclick="trEventBe(this,'auゲーム','初めての方へ','できること1',event);">auスマートパス会員なら500本以上のゲームアプリが<span class="em">無料でダウンロードできる！</span></a></li>
                        <li><a class="description" href="https://game.auone.jp/list/" onclick="trEventBe(this,'auゲーム','初めての方へ','できること2',event);">対象ゲームは、<span class="em">10％のWALLETポイント</span>がたまる！</a></li>
                        <li><a class="description" href="https://game.auone.jp/app/howto_transfer/" onclick="trEventBe(this,'auゲーム','初めての方へ','できること3',event);">プレイ中ゲームのレベル・状態そのままで、auゲームへ<span class="em">データ引継が可能！</span></a></li>
                        <li><a class="description" href="https://game.auone.jp/app/howto_transfer/#exchenge-items" onclick="trEventBe(this,'auゲーム','初めての方へ','できること4',event);">ためたWALLETポイントは、auゲーム内の<span class="em">アイテムと交換</span>できるからおトク！</a></li>
                        <li><a class="description" href="https://game.auone.jp/list/" onclick="trEventBe(this,'auゲーム','初めての方へ','できること5',event);"><span class="em">auユーザー限定アイテム</span>がもらえる！</a></li>
                    </ul>
                </div>
            </div>


            <div class="game-contents" id="use">
                <h3 class="use_title">つかいかた</h3>
                <div class="game-contents incontents">
                    <div class="howto">
                        <h4><p class="light"><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto/h4_howto01.png" width="168" alt="新しくゲームを始める"></p></h4>
                        <div class="box">
                            <img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto/img_howto01.png" class="wd100">
                            <ul>
                                <li class="small"><a href="https://game.auone.jp/ranking?category_id=001" class="all-games_btn" onclick="trEventBe(this,'auゲーム','初めての方へ','すべてのゲームから探す',event);">すべてのゲームから探す</a></li>
                                <li class="small"><a href="https://game.auone.jp/list/" class="point10_btn" onclick="trEventBe(this,'auゲーム','初めての方へ','ポイント還元ゲームから探す',event);">10%ポイント還元ゲームから探す</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="howto">
                        <h4><p class="heavy"><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto/h4_howto02.png" width="218"></p></h4>
                        <div class="box">
                            <img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto/img_howto02.png" class="wd100">
                            <ul>
                                <li><a href="https://game.auone.jp/app/howto_transfer/" class="chenge_btn" onclick="trEventBe(this,'auゲーム','初めての方へ','乗りかえる',event);">auゲームに乗りかえる</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>


            <div class="game-contents" id="play">
                <h3 class="play_title">とってもおトクなあそびかた</h3>
                <div class="game-contents incontents">
                    <div class="howto">
                        <h4><p class="otoku"><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto/h4_howto03.png" width="218"></p></h4>
                        <div class="box">
                            <img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto/img_howto03.png" class="wd100">
                            <ul>
                                <li class="small"><a href="https://game.auone.jp/app/howto_transfer/#play-point" class="enjoy-point_btn" onclick="trEventBe(this,'auゲーム','初めての方へ','楽しくためよう',event);">WALLETポイントを楽しくためよう</a></li>
                                <li class="small"><a href="https://game.auone.jp/app/howto_transfer/#exchenge-items" class="exchenge-items_btn" onclick="trEventBe(this,'auゲーム','初めての方へ','アイテム交換',event);">たまったポイントでアイテム交換</a></li>
                            </ul>
                        </div>
                        <div class="wallet_check"><p><a href="https://wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','初めての方へ','WALLETポイントをチェック',event);">WALLETポイントをチェック</a></p></div>
                    </div>
                    <div class="howto">
                        <h4><p class="only"><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto/h4_howto04.png" width="195"></p></h4>
                        <div class="box">
                            <img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto/img_howto04.png" class="wd100">
                            <ul>
                                <li><a href="https://game.auone.jp/list/" class="gift_btn" onclick="trEventBe(this,'auゲーム','初めての方へ','ギフト付ゲームをチェック',event);">alt="おトクなギフト付ゲームをチェック"</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


             <div class="qa">
                 <a href="https://game.auone.jp/app/faq/" onclick="trEventBe(this,'auゲーム','初めての方へ','よくある質問',event);">よくある質問</a>
             </div>
        </div><!--game-contents_wrapper-->
      <?php inc("adRect_android"); ?>
    </div><!--all_wrapper-->

<?php inc("footer"); ?>

</div><!--contens-body-->
</div>

<?php inc("script"); ?>
<?php inc("gtm"); ?>

</body>
</html>