<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
<meta charset="UTF-8">
    <title>auゲーム</title>
    <meta name="keywords" content="ゲーム,アプリ,スマートパス,スマパス,ポイント,お得,取り放題,乗り換え" />
    <meta name="description" content="ゲーム内で課金した金額の10％がWALLETポイントで還元される、お得なゲームアプリをご紹介。すでにお使いのゲームでも、レベルはそのままでお得に遊べます。" />
    <?php inc("head");?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/game-top.css">
</head>

<body class="index">
<!-- js-t-wrapper -->
<div class="js-t-wrapper">
    <!--//////////////////////
      nav
    //////////////////////  -->
<?php inc("header"); ?>

    <div class="contens-body">
        <div class="header-margin">



            <!--bx
            <div class="js-t-slider01 t-bx__slide">
                <div>
                    <a href="/app/lp/20150120_pointcam/" class="item"
                    onclick="trEventBe(this,'auゲーム','トップページ','大バナー_ポイントキャンペーン',event);">
                    <img width="100%" src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/banner_large_pointcam_20150120.jpg" alt="ポイントキャンペーン" />                    </a>
                </div>
            </div>
            bx -->
        </div>
        <!-- top -->
        <div class="top clearfix">
            <h2>「お得」が見つかるauゲーム！</h2>
                <div class="banner-medium banner">
                    <a href="/app/lp/20150120_pointcam/" class="item"
                    onclick="trEventBe(this,'auゲーム','トップページ','大バナー_ポイントキャンペーン',event);">
                    <img width="100%" src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/banner_large_pointcam_20150120.jpg" alt="ポイントキャンペーン" />                    </a>
                </div>
                <div class="banner-medium banner">
                <a href="//pass.auone.jp/app/detail?app_id=4698010000038&rf=gameportal"
                onclick="trEventBe(this,'auゲーム','トップページ','中バナー_白猫プロジェクト',event);">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/banner_shironeko.png" alt="" /></a>
            </div>
            <div class="banner-medium banner">
                <a href="/app/howto/" onclick="trEventBe(this,'auゲーム','トップページ','中バナー_auゲームの使い方',event);">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/banner_480_80_01.png">
              </a>
          </div>
        </div>
        <!-- /top -->
        <!-- special-games -->
        <div class="special-games">

            <!-- point-back -->
            <div class="point-back">
                <div class="text-title">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/special-games-text-mini.png">
                    <h2>auスマパス会員ならWALLETポイントがもらえちゃうゲーム</h2>
                </div>
                <!-- animation -->
                <div class="animation height_min">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/cannon.png" class="cannon">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/ball.png" class="ball" id="ball">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/explosion.png" class="explosion" id="explosion">
                </div>
                <!-- /animation -->

                <!-- list -->
                <div class="list">
                    <!-- headline -->
                    <div class="headline">
                        <div class="middle">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/game.png">
                        </div>
                        <h3>10%還元タイトル</h3>
                        <div class="t-crosshead__ex">
                            <a href="//game.auone.jp/list/" onclick="trEventBe(this,'auゲーム','トップページ','10％還元タイトル_もっとみる',event);" class="t-crosshead__button">もっとみる</a>
                        </div>
                        <!--<a href="https://game.auone.jp/app/list/" class="more-btn" onclick="trEventBe(this,'auゲーム','トップページ','10％還元タイトル_もっとみる',event);">もっとみる</a>-->
                    </div>
                    <!-- /headline -->


                    <!-- grid -->
                    <div class="t-grid top-grid badge-icon">
                        <ul class="t-grid__3columns top-grid">
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=4698010000038&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_白猫プロジェクト',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000038.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-new">NEW</div>
                                            <div class="grid__inner-right"><p>白猫プロジェクト</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=6002000000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_モンスターストライク',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-favored">人気</div>
                                            <div class="grid__inner-right"><p>モンスターストライク</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=4698010000022&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_魔法使いと黒猫のウィズ',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000022.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-favored">人気</div>
                                            <div class="grid__inner-right"><p>魔法使いと黒猫のウィズ</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            </ul><ul class="t-grid__3columns top-grid">
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=5235400000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_サウザンドメモリーズ',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_5235400000001.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-recommend">オススメ</div>
                                            <div class="grid__inner-right"><p>サウザンドメモリーズ</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=1597710000012&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_バーコードフットボーラー',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_1597710000012.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-sports">スポーツ</div>
                                            <div class="grid__inner-right"><p>バーコードフットボーラー</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=4698010000030&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_蒼の三国志',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000030.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-battle">バトル</div>
                                            <div class="grid__inner-right"><p>蒼の三国志</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            </ul><ul class="t-grid__3columns top-grid">
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=4698010000035&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_スリングショットブレイブズ',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000035.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-recommend">オススメ</div>
                                            <div class="grid__inner-right"><p>スリングショットブレイブズ</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=4526600000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_ぼくらの甲子園！ポケット for auスマートパス',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4526600000001.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-new">NEW</div>
                                            <div class="grid__inner-right"><p>ぼくらの甲子園！ポケット</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=4990700000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_誓いのキスは突然に',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4990700000001.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-lady">女子</div>
                                            <div class="grid__inner-right"><p>誓いのキスは突然に</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            </ul><ul class="t-grid__3columns top-grid">
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=9189400000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_ポケコロ',event);">
                                    <div class="t-grid__inner">
                                        <img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9189400000001.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-lady">女子</div>
                                            <div class="grid__inner-right"><p>ポケコロ</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=7132500000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_新章イケメン大奥◆禁じられた恋',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7132500000001.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-lady">女子</div>
                                            <div class="grid__inner-right"><p>新章イケメン大奥◆禁じられた恋</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=4698000000006&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_プロ野球PRIDE',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000006.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-sports">スポーツ</div>
                                            <div class="grid__inner-right"><p>プロ野球PRIDE</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            </ul><ul class="t-grid__3columns top-grid">
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=4698010000018&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_ディズニーマジシャンズクロニクル',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000018.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-rpg">RPG</div>
                                            <div class="grid__inner-right"><p>ディズニーマジシャンズクロニクル</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=4698000000007&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_恐竜ドミニオン',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000007.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-battle">バトル</div>
                                            <div class="grid__inner-right"><p>恐竜ドミニオン</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=7937300000003&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_sword of phantasia',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7937300000003.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-rpg">RPG</div>
                                            <div class="grid__inner-right"><p>sword of phantasia</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            </ul><ul class="t-grid__3columns top-grid">
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=4698010000005&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_秘宝探偵キャリー',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000005.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-battle">バトル</div>
                                            <div class="grid__inner-right"><p>秘宝探偵キャリー</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=9005100000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_戦国X（センゴククロス）',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9005100000001.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-battle">バトル</div>
                                            <div class="grid__inner-right"><p>戦国X（センゴククロス）</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="t-grid__column">
                                <a href="//pass.auone.jp/app/detail?app_id=4585600000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','DLボタン_ドラゴンファング',event);">
                                    <div class="t-grid__inner">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4585600000001.png" alt="">
                                        <div class="grid__inner-wrap">
                                            <div class="grid__inner-left badge-recommend">オススメ</div>
                                            <div class="grid__inner-right"><p>ドラゴンファング</p></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- /grid -->
                </div>
                <!-- /list -->
            </div>
            <!-- /point-back -->

            <!-- manual -->
            <div class="manual">
                <!-- headline -->
                <div class="headline">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/manual.png">
                    <h3>データ引き継ぎ方法</h3>
                    <div class="t-crosshead__ex">
                     <a href="//game.auone.jp/app/transfer/" onclick="trEventBe(this,'auゲーム','トップページ','引き継ぎ方法はこちら',event);" class="t-crosshead__button">詳細はコチラ</a>
                    </div>
                    <!--<a href="//game.auone.jp/app/transfer/" class="more-btn" onclick="trEventBe(this,'auゲーム','トップページ','引き継ぎ方法はこちら',event);">詳細はコチラ</a>-->
                </div>
                <!-- /headline -->
                <div class="text">
                    <ul>
                        <li><a href="//game.auone.jp/app/transfer/6002000000001/" onclick="trEventBe(this,'auゲーム','トップページ','引き継ぎ方法_モンスターストライク',event);">モンスターストライク</a></li>
                        <li><a href="//game.auone.jp/app/transfer/4698010000022/" onclick="trEventBe(this,'auゲーム','トップページ','引き継ぎ方法_魔法使いと黒猫のウィズ',event);">魔法使いと黒猫のウィズ</a></li>
                    </ul>
                </div>
            </div>
            <!-- /manual -->

            <!-- border -->
            <div class="border-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/border.png">
            </div>
            <!-- /border -->

            <!-- gift -->
            <div class="gift">
                <div class="gift-title">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift-title.png">
                    <h2>auスマパス会員なら限定ギフトがもらえる！</h2>
                </div>
                <!-- /animation -->
                <div class="animation">
                    <div class="container-min">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/soccer.png" class="bounce-ball" id="bounce-ball">
                    </div>
                </div>
                <!-- animation -->

                <!-- list-gift -->
                <div class="list-gift">
                    <!-- headline -->
                    <div class="headline">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png">
                        <h3>限定ギフトがもらえるタイトル</h3>
                        <div class="t-crosshead__ex">
                            <a href="/list/" onclick="trEventBe(this,'auゲーム','トップページ','限定ギフトがもらえる_もっとみる',event);" class="t-crosshead__button">もっとみる</a>
                        </div>
                    </div>
                    <!-- /headline -->
                    <ul>
                            <li class="first">
                                <a href="//pass.auone.jp/app/detail?app_id=4698010000038&rf=gameportal"
                                onclick="trEventBe(this,'auゲーム','トップページ','ギフトDLボタン_白猫プロジェクト',event);">
                                    <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000038.png" alt="白猫プロジェクト" />                                </a>
                            </li>
                            <li>
                                <a href="//pass.auone.jp/app/detail?app_id=4698010000022&rf=gameportal"
                                onclick="trEventBe(this,'auゲーム','トップページ','ギフトDLボタン_魔法使いと黒猫のウィズ',event);">
                                    <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000022.png" alt="魔法使いと黒猫のウィズ" />                                </a>
                            </li>
                            <li>
                                <a href="//pass.auone.jp/app/detail?app_id=4698010000030&rf=gameportal"
                                onclick="trEventBe(this,'auゲーム','トップページ','ギフトDLボタン_蒼の三国志',event);">
                                    <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000030.png" alt="蒼の三国志" />                                </a>
                            </li>
                        </ul>
                </div>
                <!-- /list-gift -->
            </div>
            <!-- /gift -->
        </div>
        <!-- /special-games -->

        <!-- all-games -->
        <div class="all-games">

            <!-- ranking -->
            <div class="ranking mb0">
                <div class="text-title">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/rank-title-mini.png">
                    <h2>auスマパス会員なら取り放題約500タイトルで遊びたい放題！</h2>
                </div>

                <!-- animation -->
                <div class="animation">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/castle.png" class="castle">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/dragon.png" class="dragon">
                    <div class="fire fire-disappear" id="fire">
                        <br>
                    </div>
                    <div class="hero-box hero-box-attack" id="hero-box">
                        <br>
                    </div>
                </div>
                <!-- /animation -->

                <!-- application/list -->
                <div id="application" class="list">
                    <!-- /headline -->
                    <div class="headline">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/rank.png">
                        <h3>総合ランキング</h3>
                        <div class="t-crosshead__ex">
                            <a href="/ranking/" onclick="trEventBe(this,'auゲーム','トップページ','ランキング_もっとみる',event);" class="t-crosshead__button">もっとみる</a>
                        </div>
                        <!--<a href="ranking/" class="more-btn" onclick="trEventBe(this,'auゲーム','トップページ','ランキング_もっとみる',event);">もっとみる</a>-->
                    </div>
                    <!-- /headline -->

                    <!-- ajax -->
                    <ul class="apps">
                    </ul>
                    <!-- /ajax -->
                </div>
                <!-- /application/list -->
            </div>
            <!-- /ranking -->

            <!-- category -->
            <div class="category">
                <!-- list -->
                <div class="list">
                    <!-- headline -->
                    <div class="headline">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/cat.png">
                        <h3>カテゴリー</h3>
                    </div>
                    <!-- /headline -->
                    <!-- toggle -->
                    <ul>
                        <li>
                            <a href="/index.php/ranking/index?category_id=001&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_すべて',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_all.png" class="cat-icon">
                                    <span>すべて</span>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="/index.php/ranking/index?category_id=001010&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_アクション',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_thunder.png" class="cat-icon">
                                    <span>アクション</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/index.php/ranking/index?category_id=001007&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_ロールプレイング',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_chart.png" class="cat-icon">
                                    <span>ロールプレイング</span>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="/index.php/ranking/index?category_id=001013&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_シミュレーション・ストラテジー',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_graph.png" class="cat-icon">
                                    <span>シミュレーション・ストラテジー</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/index.php/ranking/index?category_id=001008&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_スポーツ',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_baseball.png" class="cat-icon">
                                    <span>スポーツ</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <ul class="toggle">
                        <li>
                            <div class="border">
                                <span>カテゴリーをもっとみる</span>
                            </div>
                        </li>
                    </ul>
                    <ul class="toggle-open">
                        <li>
                            <a href="/index.php/ranking/index?category_id=001016&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_恋愛ゲーム',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_heart.png" class="cat-icon">
                                    <span>恋愛ゲーム</span>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="/index.php/ranking/index?category_id=001001&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_アクションパズル',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_sword.png" class="cat-icon">
                                    <span>アクションパズル</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/index.php/ranking/index?category_id=001002&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_パズル',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_puzzel.png" class="cat-icon">
                                    <span>パズル</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/index.php/ranking/index?category_id=001003&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_脳トレ',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_brain.png" class="cat-icon">
                                    <span>脳トレ</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/index.php/ranking/index?category_id=001004&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_トランプ・カード',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_spade.png" class="cat-icon">
                                    <span>トランプ・カード</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/index.php/ranking/index?category_id=001005&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_リバーシ・ボードゲーム',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_dice.png" class="cat-icon">
                                    <span>リバーシ・ボードゲーム</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/index.php/ranking/index?category_id=001006&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_将棋・囲碁・マージャン',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_pai.png" class="cat-icon">
                                    <span>将棋・囲碁・マージャン</span>
                                </div>
                            </a>
                    </li>

                        <li>
                            <a href="/index.php/ranking/index?category_id=001009&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_シューティング',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_gun.png" class="cat-icon">
                                    <span>シューティング</span>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a href="/index.php/ranking/index?category_id=001011&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_カジュアル',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_controller.png" class="cat-icon">
                                    <span>カジュアル</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/index.php/ranking/index?category_id=001012&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_アドベンチャー',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_fireball.png" class="cat-icon">
                                    <span>アドベンチャー</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/index.php/ranking/index?category_id=001014&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_パチンコ・パチスロ',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_slot.png" class="cat-icon">
                                    <span>パチンコ・パチスロ</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="/index.php/ranking/index?category_id=001015&ranking_kind=DAY" onclick="trEventBe(this,'auゲーム','トップページ','カテゴリ_レーシング',event);">
                                <div class="border">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/icon_racingcar.png" class="cat-icon">
                                    <span>レーシング</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /toggle -->
                </div>
                <!-- list -->
            </div>
            <!-- /category  -->

            <!-- bottom -->
            <div class="bottom">
                <div class="banner-medium banner">
                    <a href="http://rd.gree.net/c/585r" onclick="trEventBe(this,'auゲーム','トップページ','フッター中バナー_GREEコインプレゼント',event);"><img src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/banner_medium_coin_20141211.jpg"></a>
                </div>
                <div class="banner-medium banner">
                    <a href="http://market.gree.net/?action=au_point_scratch_introducton" onclick="trEventBe(this,'auゲーム','トップページ','フッター中バナー_GREEスクラッチ',event);"><img src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/banner_medium_02.png"></a>
                </div>
                <div class="banner-medium banner">
                    <a href="//web.lobi.co/?utm_source=augame&utm_medium=banner_medium&utm_content=basic&utm_campaign=web_top" onclick="trEventBe(this,'auゲーム','トップページ','フッター中バナー_Lobi',event);"><img src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/banner_medium_03.jpg"></a>
                </div>
                <div class="banner-medium banner">
                    <a href="http://gamegift.jp/360/" onclick="trEventBe(this,'auゲーム','トップページ','フッター中バナー_ゲームギフト',event);"><img src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/banner_medium_04.jpg"></a>
                </div>
                <div class="banner-medium banner">
                    <a href="http://gamebiz.jp/?p=137556" onclick="trEventBe(this,'auゲーム','トップページ','フッター中バナー_ソーシャルゲームインフォ',event);"><img src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/banner_SGI.jpg"></a>
                </div>
            <div class="banner-medium banner">
                <a href="//pass.auone.jp/app/detail?app_id=1597710000012&rf=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','フッター中バナー_BFB_コナンコラボ',event);">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/banner_konan.jpg">
              </a>
          </div>
          <?php inc("adRect_android"); ?>
            </div>
            <!-- /bottom -->
        </div>
        <!-- /all-games -->

        <!-- animation -->
        <div class="animation">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/flag.png" class="flag">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/car.png" class="car" id="car1">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/car.png" class="car" id="car2">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/car.png" class="car" id="car3">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/car.png" class="car" id="car4">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/car_orange.png" class="car" id="car5">
        </div>
        <!-- /animation -->

        <?php inc("footer"); ?>
    </div>
    <!-- /contens-body -->
</div>
<!-- /js-t-wrapper -->

<?php inc("script"); ?>

<script src="<?php print($PATH['url_game']);?>js/top.js"></script>
<script src="<?php print($PATH['url_tentomushi']);?>js/jquery.bxslider.custom.min.js"></script>

<script>
    $(function(){
        var bxslideOptions={
            slideWidth: 320,
            slideheight:119,
            auto: true,
            infiniteLoop:true,
            centering:true,
            pager:true
        }
        var deviceWidth = $(window).width();
        if(deviceWidth > 479){
            bxslideOptions.slideWidth = 480;
            bxslideOptions.slideheight=179;
        }
        $('.js-t-slider01').bxSlider(bxslideOptions);
    });
</script>

<?php inc("gtm"); ?>

</body>
</html>