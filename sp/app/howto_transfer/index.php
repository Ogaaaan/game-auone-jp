<?php include("../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
<meta charset="UTF-8">
<title>auゲームに乗りかえよう！ - auゲーム</title>
<meta name="keywords" content="ゲーム,auゲーム,データ移行,データ引き継ぎ,auゲームにデータを引き継ぐ方法,auゲームに乗り換え方法,auスマートパス,ポイント還元,WALLETポイント貯まる" />
<meta name="description" content="auゲームにデータを引き継ぐと、遊んだ分だけWALLETポイントが貯まる！" />

<?php inc("head"); ?>

<link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/game-howto.css">

</head>

<body class="all-app">
<div class="js-t-wrapper">
<div class="contens-body">

<?php inc("header"); ?>
<?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all_wrapper">        
        <div class="game-contents_wrapper">
            <div class="howto_transfar_mainimg">
                <h2>auゲームとは？</h2>
            </div>
            <nav>
                <ul class="howto_nav">
                    <li><a href="#takeover" onclick="trEventBe(this,'auゲーム','乗りかえよう','ナビボタン左',event);"><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto_transfer/nav_howto_transfer01.png" width="70" height="30" alt="データの引継とは?"></a></li>
                    <li><a href="#play-point" onclick="trEventBe(this,'auゲーム','乗りかえよう','ナビボタン中',event);"><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto_transfer/nav_howto_transfer02.png" width="100" height="30" alt="あそんでたまる！WALLETポイント"></a></li>
                    <li><a href="#exchenge-items" onclick="trEventBe(this,'auゲーム','乗りかえよう','ナビボタン右',event);"><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto_transfer/nav_howto_transfer03.png" width="100" height="30" alt="ためたポイントでアイテム交換！"></a></li>
                </ul>
            </nav>
            <div class="game-contents" id="takeover">
                <h3 class="takeover_title">データの引継とは</h3>
                <div class="game-contents incontents">
                  <div class="howto_transfer">
                      <p>プレイ中のゲームと同じタイトルがauゲーム内にあれば、データの移行が可能。<span class="em">時間をかけて大事に育てたキャラやアイテム、一生懸命進めたレベルがそのまま維持できます！</span></p>
                      <img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto_transfer/img_howto_transfer01.png" class="mb15 wd100">
                      <h4><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto_transfer/h4_howto_transfer01.png" width="115" height="20" alt="方法はかんたん"></h4>
                      <p class="mb15">手順にしたがってデータの引継を行い、ホームのアプリアイコンに「auロゴ」が表示されていれば手続き完了！</p>
                      <ul>
                        <li class="mb15"><a href="https://game.auone.jp/app/transfer/" class="howto-chenge_btn" onclick="trEventBe(this,'auゲーム','乗りかえよう','データ移行方法',event);">auゲームへのデータ移行方法"</a></li>
                      </ul>
                      <a href="https://game.auone.jp/list/" onclick="trEventBe(this,'auゲーム','乗りかえよう','限定アイテム詳細',event);"><img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto_transfer/img_howto_transfer02.png" class="wd100" alt="引継いだら限定アイテムもらえる"></a>
                  </div>
                </div>
            </div>


            <div class="game-contents" id="play-point">
                <h3 class="play-point_title">あそんでたまる！WALLETポイント</h3>
                <div class="game-contents incontents">
                    <div class="howto_transfer">
                      <p>auゲームにデータを移行すれば、とってもおトク！対象ゲームであれば、課金した金額に応じて10％分がWALLETポイントで還元されます。</p>
                      <img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto_transfer/img_howto_transfer03.png" class="mb15 wd100">
                      <ul>
                        <li><a href="https://game.auone.jp/app/transfer/" class="gametitle-list_btn" onclick="trEventBe(this,'auゲーム','乗りかえよう','データ引継タイトル一覧',event);">データ引継が可能なタイトル一覧</a></li>
                      </ul>         
                    </div>
                </div>
            </div>


            <div class="game-contents mb30" id="exchenge-items">
                <h3 class="exchenge-items_title">ためたポイントでアイテム交換！</h3>
                <div class="game-contents incontents">
                    <div class="howto_transfer">
                          <p>あそんでたまったポイントは、auゲーム内のアイテム交換にも使えます。いっぱいためて、どんどん使おう！</p>
                          <img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/howto_transfer/img_howto_transfer04.png" class="mb15 wd100">
                          <ul>
                            <li><a href="https://game.auone.jp/list/" class="pointback-list_btn" onclick="trEventBe(this,'auゲーム','乗りかえよう','ポイント還元タイトル一覧',event);">データ引継が可能なタイトル一覧</a></li>
                          </ul>     
                    </div>
                </div>
            </div>
        </div><!--contents_wrapper-->
      <?php inc("adRect_android"); ?>
    </div><!--all_wrapper-->

<?php inc("footer"); ?>

</div><!--contens-body-->
</div>

<?php inc("script"); ?>
<?php inc("gtm"); ?>

</body>
</html>