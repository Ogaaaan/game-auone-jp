<?php include("../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
<meta charset="UTF-8">
<title>FAQ - auゲーム</title>
<meta name="keywords" content="FAQ,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
<meta name="description" content="auゲームのFAQページです。auゲームに関するよくある質問の一覧です。" />

<?php inc("head"); ?>

</head>

  <body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">
    <!--//////////////////////
      nav
    //////////////////////  -->

<?php inc("header"); ?>
<?php inc("adSP_android"); ?>
    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games">

      <!-- ranking -->
      <div class="ranking faq">
        <div class="text-title">
          <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/faq-text.png">
          <h2>FAQ</h2>
        </div>

        <div class="list first">
          <div class="headline">
            <h3>よくある質問</h3>
          </div><!-- headline -->
          <ul>
            <li>
              <div class="box q">
                <p class="marker">
                  Q
                </p>
                <p class="text">
                  以前遊んでいたゲームのデータは引き継げますか？
              </div>
              <div class="box a">
                <p class="marker">
                  A
                </p>
                <p class="text">
                はい。引継ぎ可能です。引継ぎに関しては<a href="https://game.auone.jp/app/transfer/">コチラ</a>をご確認ください。
                </p>
              </div>
            </li>
          </ul>
        </div><!-- list -->

        <div class="list">
          <div class="headline">
            <h3>10%還元対象ゲームについて</h3>
          </div><!-- headline -->
          <ul>
            <li>
              <div class="box q">
                <p class="marker">
                  Q
                </p>
                <p class="text">
                  どうすれば10%のWALLET ポイントが還元されますか？
                </p>
              </div>
              <div class="box a">
                <p class="marker">
                  A
                </p>
                <p class="text">
                    STEP1：「"auロゴ"がついているアプリ<span class="t-caption">(※1)</span>」を「アプリ」もしくは「auゲーム」ポータルからDL<br>
                    <span class="t-caption">※1・・・「プロ野球PRIDE」「ディズニーマジシャンズクロニクル」のみ枠がオレンジ色のもの(auロゴなし)</span><br>
					STEP2：DLしたアプリでアイテムを購入する。
                </p>
              </div>
            </li>
            <li>
              <div class="box q">
                <p class="marker">
                  Q
                </p>
                <p class="text">
                  10%のポイントが還元されるのはいつですか？
                </p>
              </div>
              <div class="box a">
                <p class="marker">
                  A
                </p>
                <p class="text">
                  翌月上旬です。
                </p>
              </div>
            </li>
            <li>
              <div class="box q">
                <p class="marker">
                  Q
                </p>
                <p class="text">
                  10%のポイント還元がされたかはどのように確認できますか？
                </p>
              </div>
              <div class="box a">
                <p class="marker">
                  A
                </p>
                <p class="text">
                  ポイント獲得履歴にて確認ができます。
                </p>
              </div>
            </li>
            <li>
              <div class="box q">
                <p class="marker">
                  Q
                </p>
                <p class="text">
                  PCでアイテム購入した場合も10%還元対象となりますか？
                </p>
              </div>
              <div class="box a">
                <p class="marker">
                  A
                </p>
                <p class="text">いいえ、対象にはなりません。アプリDLページに記載されている対応機種でアイテム購入された時のみが対象となります。
                </p>
              </div>
            </li>
            <li>
              <div class="box q">
                <p class="marker">
                  Q
                </p>
                <p class="text">
                  支払方法は何が選べますか？
                </p>
              </div>
              <div class="box a">
                <p class="marker">
                  A
                </p>
                <p class="text">「お支払方法」の選択画面で、以下４つの支払い方法が選べます。<br>
                    ・通信料金と合算<br>
                    ・au WALLETカード<br>
                    ・WebMoney<br>
                    ・クレジットカード<br>
                    また、いずれの支払方法でも10%還元されます。
                </p>
              </div>
            </li>
          </ul>
        </div><!-- list -->


        <div class="list">
          <div class="headline">
            <h3>ゲームギフトについて</h3>
          </div><!-- headline -->
          <ul>
            <li>
              <div class="box q">
                <p class="marker">
                  Q
                </p>
                <p class="text">
                  ゲームギフトとは？
                </p>
              </div>
              <div class="box a">
                <p class="marker">
                  A
                </p>
                <p class="text">
                  大人気ゲームのアイテム無料配信を中心に、攻略情報や特集レビュー、新作ゲーム事前登録情報など、様々なお得 (ギフト) を提供する新感覚のゲームメディアサービスです。<br>
                  詳しくは<a href="http://www.au.kddi.com/mobile/service/smartphone/entertainment/gamegift/">コチラ</a>をご確認ください。
                </p>
              </div>
            </li>
            <li>
              <div class="box q">
                <p class="marker">
                  Q
                </p>
                <p class="text">
                  ギフトは既に対象ゲームを利用しているユーザでももらえますか？
                </p>
              </div>
              <div class="box a">
                <p class="marker">
                  A
                </p>
                <p class="text">
                  はい。既に対象ゲームをプレイしていただいている方でもご利用いただけます。ただし、auスマートパス会員限定ギフトはauスマートパスから対象ゲームをダウンロード、ご利用いただいている方が対象となっております。
                </p>
              </div>
            </li>
            <li>
              <div class="box q">
                <p class="marker">
                  Q
                </p>
                <p class="text">
                  ゲームギフトの使い方を教えてください。
                </p>
              </div>
              <div class="box a">
                <p class="marker">
                  A
                </p>
                <p class="text">
                	ゲームギフトはAppBroadCastが提供するアプリです。<br>詳しくは<a href="http://gamegift.jp/345/">コチラ</a>をご確認ください。
                </p>
              </div>
            </li>
            <li>
              <div class="box q">
                <p class="marker">
                  Q
                </p>
                <p class="text">
                  ギフトがもらえない。
                </p>
              </div>
              <div class="box a">
                <p class="marker">
                  A
                </p>
                <p class="text">
                	ゲームギフトはAppBroadCastが提供するアプリです。<br>詳しくは<a href="http://gamegift.jp/contact/">コチラ</a>をご確認ください。
                </p>
              </div>
            </li>
            <li>
              <div class="box q">
                <p class="marker">
                  Q
                </p>
                <p class="text">
                  もらったギフトが使えない。
                </p>
              </div>
              <div class="box a">
                <p class="marker">
                  A
                </p>
                <p class="text">
                	ゲームギフトはAppBroadCastが提供するアプリです。<br>詳しくは<a href="http://gamegift.jp/contact/">コチラ</a>をご確認ください。
                </p>
              </div>
            </li>
          </ul>
        </div><!-- list -->
      </div><!-- ranking -->
      <?php inc("adRect_android"); ?>
    </div>

<?php inc("footer"); ?>

</div><!--contens-body-->
</div>

<?php inc("script"); ?>
<?php inc("gtm"); ?>

</body>
</html>