<?php include("../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
    <meta charset="UTF-8">
    <title>au IDログインについて - auゲーム</title>
    <meta name="keywords" content="ゲーム,アプリ,スマートパス,スマパス,コラム" />
    <meta name="description" content="au IDログインについてのよくある質問" />

    <?php inc("head"); ?>

    <!-- style.css簡略化の為の個別指定 -->
    <style type="text/css">
    <!--
    .all-app .auid {margin-top: 16px;margin-bottom: 16px;}
    .all-app .auid .text-title {text-align: center;}
    .auid .text-title img {max-width: 480px;width: 90%;padding: 32px 0px;height: auto;}
    .all-app .auid .text-title img {padding-top: 16px;}
    .video-container {position: relative;padding-bottom: 56.25%;padding-top: 30px;height: 0;overflow: hidden;}
    .video-container iframe {position: absolute;top: 0;left: 0;width: 100%;height: 100%;}
    .auid .headline{padding:0;}
    .auid .headline a {display: block;height: 39px;padding: 8px;}
    .max-w{max-width: 480px;}
    .pd_10{padding: 10px;}
    .mb_20 {margin-bottom: 20px !important;}
    .mt_10 {margin-top: 10px;}
     -->
    </style>
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <?php inc("adSP_android"); ?>

            <!--//////////////////////
            all-games
            //////////////////////  -->
            <div class="all-games">

                <!-- auid -->
                <div class="auid">
                    <div class="text-title">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/auid.png">
                        <h2>au ID ログインについて</h2>
                    </div><!-- text-title -->

                    <div class="list mb_20">
                        <div class="headline">
                            <a href="https://id.auone.jp/id/sp/help/auid/2.html" onclick="trEventBe(this,'auゲーム','auIDログイン','auIDを登録するには',event);">
                            <h3 class="no_img">au IDを登録するには</h3>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/arrow-down.png" class="arrow">
                            </a>
                        </div><!-- headline -->
                        <ul class="apps">
                            <li>
                                <a href="#" class="pd_10 mt_10">
                                    <div class="video-container">
                                    <iframe src="https://www.youtube.com/embed/fGFrnWSqB4Q?showinfo=0" frameborder="0"></iframe>
                                    </div><!-- video-container -->
                                </a>
                            </li>
                        </ul>
                    </div><!-- list mb_20" -->

                    <div class="list mb_20">
                        <div class="headline">
                            <a href="https://id.auone.jp/id/sp/help/howto/1.html" onclick="trEventBe(this,'auゲーム','auIDログイン','auIDを忘れてしまった',event);">
                                <h3 class="no_img">au IDを忘れてしまった</h3>
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/arrow-down.png" class="arrow">
                            </a>
                        </div><!-- headline -->
                        <div class="headline">
                            <a href="https://id.auone.jp/id/sp/help/howto/2.html" onclick="trEventBe(this,'auゲーム','auIDログイン','auIDのパスワードを忘れてしまった',event);">
                                <h3 class="no_img">au IDのパスワードを忘れてしまった</h3>
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/arrow-down.png" class="arrow">
                            </a>
                        </div><!-- headline -->
                        <ul class="apps">
                            <li>
                                <a href="#" class="pd_10 mt_10">
                                    <div class="video-container">
                                    <iframe src="https://www.youtube.com/embed/VYKzcyru7aM?showinfo=0" frameborder="0"></iframe>
                                    </div><!-- video-container -->
                                </a>
                            </li>
                        </ul>
                    </div><!-- list mb_20 -->

                    <div class="list mb_20">
                        <div class="headline bottom_noline">
                            <a href="https://id.auone.jp/id/sp/help/pass/11.html" onclick="trEventBe(this,'auゲーム','auIDログイン','暗証番号を忘れてしまった',event);">
                                <h3 class="no_img">暗証番号を忘れてしまった</h3>
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/arrow-down.png" class="arrow">
                            </a>
                        </div><!-- headline bottom_noline -->
                    </div><!-- list mb_20 -->

                    <div class="operation-btn-box mb_20">
                        <a href="https://id.auone.jp/" class="operation-button max-w" onclick="trEventBe(this,'auゲーム','auIDログイン','auIDについて詳しくはコチラ',event);">au IDについて詳しくはコチラ</a>
                    </div><!-- operation-btn-box mb_20 -->

                </div><!-- auid -->

                <?php inc("adRect_android"); ?>

            </div><!-- all-games -->

            <?php inc("footer"); ?>

        </div><!-- contens-body -->

    </div><!-- js-t-wrapper -->

    <?php inc("script"); ?>
    <?php inc("gtm"); ?>
</body>
</html>