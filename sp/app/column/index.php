<?php include("../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja-JP">
<head>
<meta charset="UTF-8">
    <title>ゲームコラム一覧 - auゲーム</title>
    <meta name="keywords" content="ゲーム,アプリ,スマートパス,スマパス,コラム" />
    <meta name="description" content="auスマートパスのおすすめゲームアプリのコラムをご紹介。" />

    <?php inc("head"); ?>
  </head>

  <body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">
    <!--//////////////////////
      nav
    //////////////////////  -->
        <?php inc("header"); ?>
        <?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-app">

      <!-- ranking -->
      <div class="ranking">
        <div class="text-title">
          <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/colomun-title-img.png">
          <h2>ゲームコラム一覧</h2>
        </div>

        <div class="list first" style="margin-bottom: 16px;">
          <ul class="apps">
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http://octoba.net/archives/20150427-android-game-nyanko-425997.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_ほしの島のにゃんこ,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000039.png" alt="ほしの島のにゃんこ">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>ほしの島のにゃんこ</h4>
                            <p class="details">可愛いにゃんこと一緒に育てる農園ゲーム！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20150530-android-game-chkai-432515.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_誓いのキスは突然に,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/49907/4990700000001/h_85155_92622.png" alt="誓いのキスは突然に">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>誓いのキスは突然に</h4>
                            <p class="details">ウソから始まった恋の行く末は…？</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2015%2F05%2Fausp-q-pazzle.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_Q for au,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/74010/7401000000001/m_90187_100674.png" alt="Q for au">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>Q for au</h4>
                            <p class="details">簡単でしょ？解ける人には解けるパズル</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20150516-android-game-chromwolf-428963.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_RPG鋼鉄幻想記クロムウルフ,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/89438/8943810000030/h_87150_95674.png" alt="RPG鋼鉄幻想記クロムウルフ">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>RPG鋼鉄幻想記クロムウルフ</h4>
                            <p class="details">重厚なストーリーで描かれる正統派RPG！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20150409-android-game-sengoku-x-422499.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_戦国X（センゴククロス）,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/90051/9005100000001/m_88995_98774.png" alt="戦国X（センゴククロス）">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>戦国X（センゴククロス）</h4>
                            <p class="details">戦国時代を舞台に軍勢バトルが開幕！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2015%2F04%2Fausp-koushien.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_ぼくらの甲子園！ポケット for auスマートパス,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4526600000001.png" alt="ぼくらの甲子園！ポケット for auスマートパス">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>ぼくらの甲子園！ポケット for auスマートパス</h4>
                            <p class="details">500万人突破！「ぼくらの甲子園」最新作</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20140716-android-game-whitecat-342323.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_白猫プロジェクト,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/46980/4698010000038/h_87633_96421.png" alt="白猫プロジェクト">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>白猫プロジェクト</h4>
                            <p class="details">ぷにコンにハマる！ワンフィンガーRPG！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2014%2F10%2Fausp-jp.colopl.quizwiz.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_魔法使いと黒猫のウィズ,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/46980/4698010000022/h_84570_91637.png" alt="魔法使いと黒猫のウィズ">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>魔法使いと黒猫のウィズ</h4>
                            <p class="details">クイズとRPGが融合。『魔法使いと黒猫のウィズ』はこんなゲーム</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20141029-android-game-monsterstrike-381161.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_モンスターストライク,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/60020/6002000000001/h_85132_92591.png" alt="モンスターストライク">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>モンスターストライク</h4>
                            <p class="details">人気引ひっぱりアクションがお得に遊べる！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2014%2F11%2Fjp.colopl.sangoku.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_蒼の三国志,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/46980/4698010000030/h_85016_92281.png" alt="蒼の三国志">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>蒼の三国志</h4>
                            <p class="details">軍師を名乗るなら三国志はハズせない</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2015%2F03%2Fausp-lyne.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_LYNE -ライン-,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/10906/1090600000012/m_85860_93760.png" alt="LYNE -ライン-">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>LYNE -ライン-</h4>
                            <p class="details">ライン違い？頭を鍛えるパズルLYNE</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20150315-android-game-worldofgoo-416337.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_ワールド･オブ･グー,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/86622/8662210000011/h_78903_82074.png" alt="ワールド･オブ･グー">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>ワールド･オブ･グー</h4>
                            <p class="details">バランス感覚を磨いてゴールを目指せ！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20150214-android-game-issho-miracle-408815.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_「おかあさんといっしょ」のパズルであそぼ！ ミラクルぽんっ！,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/86127/8612700000001/m_84511_91558.png" alt="「おかあさんといっしょ」のパズルであそぼ！ ミラクルぽんっ！">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>「おかあさんといっしょ」のパズルであそぼ！ ミラクルぽんっ！</h4>
                            <p class="details">親子で遊べるパズルゲーム！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20150215-android-game-jubeat-plus-408810.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_jubeat (ユビート),event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/32938/3293810000003/h_81471_86938.png" alt="jubeat (ユビート)">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>jubeat (ユビート)</h4>
                            <p class="details">ゲーセンで大人気の音ゲー登場！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2015%2F02%2Fausp-townsv.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_財閥タウンズＶ,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/86876/8687610000008/m_85359_93025.png" alt="財閥タウンズＶ">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>財閥タウンズＶ</h4>
                            <p class="details">セレブ街をつくろう！ 町育てゲーム</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20150204-android-game-sushi-kaido-406286.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_海鮮!!すし街道,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/86876/8687600000021/h_82168_88093.png" alt="海鮮!!すし街道">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>海鮮!!すし街道</h4>
                            <p class="details">らっしゃい！日本一の寿司屋さんを作ろう！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2015%2F02%2Fausp-attack25.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_パネルクイズ アタック２５,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/18616/1861610000001/h_85494_93197.png" alt="パネルクイズ アタック２５">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>パネルクイズ アタック２５</h4>
                            <p class="details">アタック！ 人気クイズ＋陣取りゲーム</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20150127-android-game-bridge-constructor-playground-403709.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_Bridge Constructor Playground,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/82566/8256610000001/m_78984_82241.png" alt="Bridge Constructor Playground">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>Bridge Constructor Playground</h4>
                            <p class="details">バランスよく組み立てないと…崩れちゃう！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20150112-android-game-revolt-399918.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_RE-VOLT Classic,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/24175/2417510000009/h_79569_83556.png" alt="RE-VOLT Classic">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>RE-VOLT Classic</h4>
                            <p class="details">独特のコース満載！ラジコンレースアプリ！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2015%2F01%2Fjp.co.artnw.kaerugoogleplay.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_FROG MINUTES,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/35687/3568700000001/h_72045_69840.png" alt="FROG MINUTES">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>FROG MINUTES</h4>
                            <p class="details">春が待ち遠しい。かわいいカエル集めゲーム</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2014%2F12%2Fcom.galapagossoft.trial.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_Trial Xtreme,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/24175/2417510000005/m_70838_67322.png" alt="Trial Xtreme">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>Trial Xtreme</h4>
                            <p class="details">100万回事故ってもめげないバイクゲーム</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2014%2F12%2Fjp.co.taito.am.moba.siInfinityGene.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_スペースインベーダー インフィニティジーン,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/68956/6895600000003/h_54413_40013.png" alt="スペースインベーダー インフィニティジーン">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>スペースインベーダー インフィニティジーン</h4>
                            <p class="details">ただのインベーダーゲーム...じゃない！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2014%2F12%2Fjp.co.ponos.prism.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_パズルプリズム,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/24300/2430000000001/m_76883_78572.png" alt="パズルプリズム">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>パズルプリズム</h4>
                            <p class="details">サルになるほどハマった「テトリス」3D版</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20120701-android-app-shougi-wars-145511.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_将棋ウォーズ,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/92660/9266000000002/h_78363_81116.png" alt="将棋ウォーズ">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>将棋ウォーズ</h4>
                            <p class="details">日本将棋連盟公認のオンライン将棋アプリ！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2014%2F11%2Fspikechunsoft.android428google.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_428-封鎖された渋谷で-,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/21529/2152910000001/m_79766_83986.png" alt="428-封鎖された渋谷で-">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>428-封鎖された渋谷で-</h4>
                            <p class="details">スマホ×ノベルゲームの相性はもはや麻薬</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20141109-android-game-cytus-385116.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_Cytus,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/24175/2417510000001/m_82573_88670.png" alt="Cytus">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>Cytus</h4>
                            <p class="details">美しいグラフィックが音楽を彩る！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2014%2F11%2Fam.ate.android.olmahjong.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_麻雀 雷神 -Rising-,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/84863/8486300000097/h_85296_92886.png" alt="麻雀 雷神 -Rising-">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>麻雀 雷神 -Rising-</h4>
                            <p class="details">本格派麻雀に仮面ライダー参戦</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20140929-android-game-happystreet-370339.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_ハッピーストリート,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/19373/1937300000006/m_84495_91542.png" alt="ハッピーストリート">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>ハッピーストリート</h4>
                            <p class="details">可愛い街を作ろう！癒やし系箱庭ゲーム！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2014%2F10%2Fausp-Power-Smash.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_Power Smash CHALLENGE,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/66543/6654310000006/h_77183_78975.png" alt="Power Smash CHALLENGE">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>Power Smash CHALLENGE</h4>
                            <p class="details">もっと熱くなれよ！ 本気のテニスゲーム！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20140927-android-game-crazy-taxi-370154.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_CRAZY TAXI,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/66543/6654310000009/h_78566_81496.png" alt="CRAZY TAXI">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>CRAZY TAXI</h4>
                            <p class="details">往年の名作がAndroidアプリに！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20140923-android-game-chariso-dx-370780.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_チャリ走DX,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/16408/1640800000026/m_75567_76150.png" alt="チャリ走DX">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>チャリ走DX</h4>
                            <p class="details">ハイテンションなBGMに乗って爆走！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Foctoba.net%2Farchives%2F20140917-android-game-hungry-shark-368065.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_Hungry Shark,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/60503/6050300000016/m_81856_87515.png" alt="Hungry Shark">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>Hungry Shark</h4>
                            <p class="details">腹ぺこサメで食べて食べて食べまくる！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2014%2F09%2Fapp.store.757926910.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_ディグディグ,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/19373/1937310000008/m_83201_89587.png" alt="ディグディグ">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>ディグディグ</h4>
                            <p class="details">つるはし＆ダイナマイトで宝さがし！</p>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="https://pass.auone.jp/app/r?redirect=9&r_url=http%3A%2F%2Fwww.tabroid.jp%2Fnews%2F2014%2F09%2Fid700637744.html" target="_blank" onclick="trEventBe(this,'auゲーム','コラム一覧','コラム_Deemo,event);">
                    <div class="border">
                        <div class="image"><img class="app-icon" src="//img.au-market.com/mapi/pc_icon/24175/2417510000011/m_79176_82570.png" alt="Deemo">
                        </div>
                        <div class="app-text app-list-top">
                            <h4>Deemo</h4>
                            <p class="details">ピアノのように旋律を紡ぐ音楽ゲーム</p>
                        </div>
                    </div>
                </a>
            </li>

        </ul>
    </div><!-- list -->

    <aside class="t-button-right container-min">
        <p class="t-button__top"><a class="pagetop">ページトップへ</a></p>
    </aside>

    </div><!-- ranking -->
    <?php inc("adRect_android"); ?>
    </div>
    <?php inc("footer"); ?>
</div><!--contens-body-->
</div>

    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

  </body>
</html>