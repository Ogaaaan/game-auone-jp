<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
<meta charset="UTF-8">
<title>毎月3日はポイント還元日 - auゲーム</title>
<meta name="keywords" content="ゲーム,auゲーム,auスマートパス,ポイント還元,WALLETポイント" />
<meta name="description" content="毎月3日はポイント還元日！たまったポイントでゲーム内のアイテムと交換しよう！" />

<?php inc("head"); ?>

<link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_pointday.css">

</head>

<body class="all-app">
<div class="js-t-wrapper">
<div class="contens-body">

<?php inc("header"); ?>
<?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->

<div class="lppb-wrapper">

    <!--メインイメージ-->
    <section class="lppb-main-img">
        <img src="<?php echo $PATH['url_game'];?>img/lp/pointday/mainimg.png" alt="毎月3日はポイント還元日！">
    </section>
    <!--/メインイメージ-->
        
        
    <section class="lppb-main-contents">
        <!--上部ボタン-->
        <div class="lppb-main-contents__btn">
            <a href="https://id.auone.jp/point/info/index.html" onclick="trEventBe(this,'auゲーム','ポイント還元日LP','ポイント明細確認',event);">ポイント明細を確認する</a>
        </div>
        <!--/上部ボタン-->


        <!--たまったポイントでアイテム交換しよう！-->
        <section class="lppb-exchenge-item">
            <h3 class="lppb-exchenge-item__title">
                <img src="<?php echo $PATH['url_game'];?>img/lp/pointday/h3_exchenge_item.png" width ="283" alt="たまったポイントでアイテム交換しよう！"> 
            </h3>
            <div class="pd10">
                <p class="pb10">たまったポイントは、ゲーム内のアイテム交換にも使えます。</p>
                <img src="<?php echo $PATH['url_game'];?>img/lp/pointday/img_exchenge_item.png" class="full pb10">
            </div>
        </section>
         <!--/たまったポイントでアイテム交換しよう！-->
    </section>

    <section class="lppb-sub-contents">
        <!--auゲームなら10％ポイント還元！-->
        <section class="lppb-reward-points">
            <h3 class="lppb-reward-points__title">
                <img src="<?php echo $PATH['url_game'];?>img/lp/pointday/h3_reward_points.png" width ="258" alt="auゲームなら10%還元！"> 
            </h3>
            <div class="pd10">
                <p class="pb10">auゲームの対象タイトルであれば、課金した金額の10％分がWALLET ポイントで還元されます。</p>
                <img src="<?php echo $PATH['url_game'];?>img/lp/pointday/img_reward_points.png" class="full">
                <div class="lppb-reward-points__btn">
                    <a href="https://game.auone.jp/list/" onclick="trEventBe(this,'auゲーム','ポイント還元日LP','10％ポイント還元一覧',event);">10％ポイント還元ゲーム一覧</a>
                </div>
            </div>
        </section>
        <!--/auゲームなら10％ポイント還元！-->

        <!--おトクなauゲームに乗りかえよう！-->
        <section class="lppb-transfer">
            <h3 class="lppb-transfer__title">
                <img src="<?php echo $PATH['url_game'];?>img/lp/pointday/h3_transfer.png" width ="263"　alt="おトクなauゲームに乗りかえよう！"> 
            </h3>
            <div class="pd10">
                <p class="pb10">プレイ中のゲームと同じタイトルがauゲーム内にあれば、データの移行が可能。<span class="txt-orange">時間をかけて大事に育てたキャラやアイテム、一生懸命進めたレベルがそのまま維持できます！</span></p>
                <img src="<?php echo $PATH['url_game'];?>img/lp/pointday/img_transfer.png" class="full pb15">
                <h4>
                    <img src="<?php echo $PATH['url_game'];?>img/lp/pointday/h4_simple_method.png" width ="115"　alt="方法はかんたん！"> 
                </h4>
                <p>手順にしたがってデータの引き継ぎを行い、ホームのアプリアイコンに「auロゴ」が表示されていれば手続き完了！</p>
                <div class="lppb-transfer__btn">
                    <a href="https://game.auone.jp/app/transfer/" onclick="trEventBe(this,'auゲーム','ポイント還元日LP','データ移行方法',event);">auゲームへのデータ移行方法</a>
                </div>

                <div  class="lppb-transfer__bnr">
                    <a href="https://game.auone.jp/list/" onclick="trEventBe(this,'auゲーム','ポイント還元日LP','限定アイテム',event);"><img src="<?php echo $PATH['url_game'];?>img/lp/pointday/bnr_transfer.png" class="full"></a>
                </div>
            </div>
        </section>
        <!--/おトクなauゲームに乗りかえよう！-->
         
      <!--下部ボタン-->
        <div class="lppb-sub-contents__btn">
            <a href="https://game.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイント還元日LP','ゲームトップ',event);">auゲームトップ</a>
        </div>
   　 <!--/下部ボタン-->
    </section>
    
    <div class="mg30">
        <?php inc("adRect_android"); ?>
    </div>

</div>
</div>
<?php inc("footer"); ?>

</div><!--contens-body-->


<?php inc("script"); ?>
<?php inc("gtm"); ?>

</body>
</html>
