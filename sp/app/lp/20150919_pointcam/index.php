<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>シルバーウィーク企画! WALLET ポイントプレゼントキャンペーン - auゲーム</title>
    <meta name="keywords" content="ポイントキャンペーン,au WALLET,ポイントプレゼント,ポイントゲーム,アプリ,スマートパス,スマパス,auスマートパス,ぼくとドラゴン,ベストイレブン,BestEleven" />
    <meta name="description" content="auゲームのWALLET ポイントプレゼントキャンペーン。期間内に対象タイトルを新規ダウンロードまたは起動した方1000名様に抽選で200ポイントをプレゼント！" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
    <style>
        .app-title p{font-size: 16px; font-weight: bold; padding: 20px 10px 5px 10px;}
        .app-text h4{font-weight: normal; color: #eb5505;}
        .border{margin-left: 0 !important;}
        .border img{ margin-left: 8px;}
    </style>
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>

            <div class="all-games special-games">
                <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title">
                            <div class="box_period clearfix mt_10">
                                <p>期間限定</p>
                                <p>9/19(土)0:00〜9/27(日)23:59</p>
                            </div>
                            <img src="<?php print($PATH['url_game']);?>img/lp/lp_20150919_main.png" class="img_width-full" alt="シルバーウィーク企画! WALLETポイントプレゼントキャンペーン">
                            <p class="text_left">期間中に対象アプリを1タイトル以上ダウンロード、または起動していただいた方の中から抽選で1,000名にWALLET ポイントをプレゼントいたします。<br><span style="font-size:10px;">※auスマートパス会員の方が対象です。</span></p>
                        </div>
                        <div class="list mb_10">
                            <div class="headline pb_15">
                                <div class="middle">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_flag.png">
                                </div>
                                <h3>キャンペーン対象アプリ</h3>
                            </div>
                            <ul class="apps">
                                <li>
                                    <a href="https://pass.auone.jp/app/detail?app_id=6721200000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ぼくとドラゴン for au',event);">
                                        <div class="border">
                                            <div class="app-title">
                                                <p>ぼくとドラゴン for au</p>
                                            </div>
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6721200000001.png" alt="ぼくとドラゴン">
                                            </div>
                                            <div class="app-text">
                                                <h4>簡単操作!! 新感覚3Dアクション</h4>
                                                <p class="genre">ホーム画面で起動して数秒で簡単操作、スタミナ消費無しで誰でもお手軽にスライム討伐が楽しめる新感覚RPG！</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://pass.auone.jp/app/detail?app_id=6798100000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_BEST☆ELEVEN+',event);">
                                        <div class="border">
                                            <div class="app-title">
                                                <p>BEST☆ELEVEN+</p>
                                            </div>
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6798180002101.png" alt="BEST☆ELEVEN+">
                                            </div>
                                            <div class="app-text">
                                                <h4>大人気サッカーゲーム決定版</h4>
                                                <p class="genre">ヨーロッパ14クラブチームの選手がすべて実名・実写で登場する本格サッカーゲーム！最高のクラブチームを指揮しよう！</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_thumb.png">
                                </div>
                                <h3>すでにプレイ中の方も安心！</h3>
                            </div>

                            <div class="pd_10"><span>auスマートパスアプリ以外ですでにプレイ中の方も、auスマートパスアプリにデータを引き継いでプレイを再開すれば、今のレベルのままポイント還元を受けることができます。<br>
                             ぜひこの機会に、ご利用ください。</span>
                            </div>
                            <div class="operation-btn-box mb_10"><a href="//game.auone.jp/app/transfer/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','データ引き継ぎ方法',event);">データ引き継ぎ方法</a>
                            </div>
                        </div>
                    </div>
                </div><!-- container-min lp_201410--><!-- Bg_orange -->
            </div><!-- all-games special-games-->

            <div class="all-games ranking container-min lp_201410">
                <!-- <div class="list">
                    <div class="special-gift">
                        <div class="headline">プレゼント内容</div>
                        <div class="pd_10"><span>課金金額の20％分のWALLET ポイント</span></div>
                    </div>
                </div>-->

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">キャンペーン期間</div>
                        <div class="pd_10">
                            <span>2015年9月19日(土)0:00から<br>2015年9月27日(日)23:59まで!!</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">応募方法</div>
                        <div class="pd_10">
                        <span>期間中に対象アプリを1つ以上ダウンロード、もしくは対象アプリを起動いただいた方の中から抽選でWALLET ポイントをプレゼントいたします。</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">抽選/WALLET ポイントの付与</div>
                        <div class="pd_10">
                        <span>キャンペーン終了後、厳正なる抽選のうえ当選者を決定いたします。<br>当選者へのWALLET ポイントの付与は、2015年10月上旬ごろを予定しております。</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">au WALLET について</div>
                        <span class="step-text" style="text-align:left;">au WALLET についての詳細は、<a href="//wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">こちら</a>をご覧ください。</span>
                        <div class="img_center pd_10">
                            <a href="//wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">
                            <img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text08.png" alt="au WALLET">
                            </a>
                        </div>
                        <div class="btn-box_half pb_15 clearfix">
                            <div class="operation-btn-box"> <a href="//wallet.auone.jp/contents/sp/guide/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET ご利用ガイド',event);">ご利用ガイド</a></div>
                            <div class="operation-btn-box"> <a href="//wallet.auone.jp/contents/sp/help/index.html" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET よくある質問',event);">よくある質問</a></div>
                        </div>
                    </div>
                </div>
                <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">注意事項</div>
                        <div class="pd_10">
                            <span>
                            ・本キャンペーンは、予告なく変更または中止する場合がございます。<br>
                            ・メンテナンス等の関係で、アプリのダウンロードができない場合がございます。
                            </span>
                        </div>
                    </div>
                </div>
                <?php inc("adRect_android"); ?>
            </div><!--all-games ranking container-min lp_201410-->
        </div><!--contens-body-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>
