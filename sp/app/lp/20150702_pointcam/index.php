<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>7月2日「auスマートパスの日」キャンペーン - auゲーム</title>
    <meta name="keywords" content="ポイントキャンペーン,au WALLET,ポイント還元,15%,プレゼント,ゲーム,アプリ,スマートパス,スマパス,auスマートパスの日" />
    <meta name="description" content="auゲームのポイントキャンペーン。「auスマートパスの日」限定でポイント還元率プラス5％！" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/game-top.css">
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>

            <!-- <div class="top clearfix">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/banner_600_70_01.png" class="img_width-full">
            </div> -->
            <div class="all-games special-games">
                 <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title">
                            <p class="pd_10"><img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/banner_600_70_01.png" class="img_width-full" alt="毎月2日、22日はauスマートパスの日"></p>
                            <div class="box_period clearfix">
                                <p>期間限定</p>
                                <p>7/2(木)10:00から21:59まで</p>
                            </div>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/lp_pointcam_main01.png" class="img_width-full" alt="全員に還元率プラス5％　WALLET ポイントで合計15％戻ってくる！">
                            <p class="text_left">通常、対象アプリで課金いただいた金額の10％がWALLET ポイントで還元されるところを、「auスマートパスの日」は、追加で5％分のポイントをさらにプレゼント。ぜひ、この機会にご利用ください。</p>
                        </div>
                        <div class="list mb_10">
                            <div class="headline pb_15">
                                <div class="middle">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_flag.png">
                                </div>
                                <h3>キャンペーン対象アプリ</h3>
                            </div>
                            <!-- grid -->
                            <div class="t-grid top-grid badge-icon">
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=6002000000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_モンスターストライク',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-favored">人気</div>
                                                    <div class="grid__inner-right"><p>モンスターストライク</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000038&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_白猫プロジェクト',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000038.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-favored">人気</div>
                                                    <div class="grid__inner-right"><p>白猫プロジェクト</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                     <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000022&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_魔法使いと黒猫のウィズ',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000022.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-new">NEW</div>
                                                    <div class="grid__inner-right"><p>魔法使いと黒猫のウィズ</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000039&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ほしの島のにゃんこ',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000039.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-new">NEW</div>
                                                    <div class="grid__inner-right"><p>ほしの島のにゃんこ</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4526600000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ぼくらの甲子園！ポケット for auスマートパス',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4526600000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-new">NEW</div>
                                                    <div class="grid__inner-right"><p>ぼくらの甲子園！ポケット for auスマートパス</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=5235400000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_サウザンドメモリーズ',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_5235400000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-recommend">オススメ</div>
                                                    <div class="grid__inner-right"><p>サウザンドメモリーズ</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=9189400000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ポケコロ',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9189400000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-lady">女子</div>
                                                    <div class="grid__inner-right"><p>ポケコロ</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=1597710000012&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_バーコードフットボーラー',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_1597710000012.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-sports">スポーツ</div>
                                                    <div class="grid__inner-right"><p>バーコードフットボーラー</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000035&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_スリングショットブレイブズ',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000035.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-recommend">オススメ</div>
                                                    <div class="grid__inner-right"><p>スリングショットブレイブズ</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=7132500000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_新章イケメン大奥◆禁じられた恋',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7132500000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-lady">女子</div>
                                                    <div class="grid__inner-right"><p>新章イケメン大奥◆禁じられた恋</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4990700000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_誓いのキスは突然に',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4990700000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-lady">女子</div>
                                                    <div class="grid__inner-right"><p>誓いのキスは突然に</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4585600000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ドラゴンファング',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4585600000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-recommend">オススメ</div>
                                                    <div class="grid__inner-right"><p>ドラゴンファング</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=9005100000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_戦国X（センゴククロス）',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9005100000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-battle">バトル</div>
                                                    <div class="grid__inner-right"><p>戦国X（センゴククロス）</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=1732700000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_どかだん for auスマートパス',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_1732700000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-new">NEW</div>
                                                    <div class="grid__inner-right"><p>どかだん for auスマートパス</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698000000006&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_プロ野球PRIDE',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000006.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-sports">スポーツ</div>
                                                    <div class="grid__inner-right"><p>プロ野球PRIDE</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000030&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_蒼の三国志',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000030.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-battle">バトル</div>
                                                    <div class="grid__inner-right"><p>蒼の三国志</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000018&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ディズニー マジシャン・クロニクル',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000018.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-rpg">RPG</div>
                                                    <div class="grid__inner-right"><p>ディズニー マジシャン・クロニクル</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698000000007&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_恐竜ドミニオン',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000007.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-battle">バトル</div>
                                                    <div class="grid__inner-right"><p>恐竜ドミニオン</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000005&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_秘宝探偵キャリー',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000005.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-battle">バトル</div>
                                                    <div class="grid__inner-right"><p>秘宝探偵キャリー</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=7937300000003&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_sword of phantasia',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7937300000003.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-rpg">RPG</div>
                                                    <div class="grid__inner-right"><p>sword of phantasia</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- grid -->
                        </div>

                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_thumb.png">
                                </div>
                                <h3>すでにプレイ中の方も安心！</h3>
                            </div>

                            <div class="pd_10"><span>auスマートパスアプリ以外ですでにプレイ中の方も、auスマートパスアプリにデータを引き継いでプレイを再開すれば、今のレベルのままポイント還元を受けることができます。<br>
                             ぜひこの機会に、ご利用ください。</span>
                            </div>
                            <div class="operation-btn-box mb_10"><a href="https://game.auone.jp/app/transfer/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','データ引き継ぎ方法',event);">データ引き継ぎ方法</a>
                            </div>
                        </div>
                    </div>
                </div><!-- container-min lp_201410--><!-- Bg_orange -->
            </div><!-- all-games special-games-->

            <div class="all-games ranking container-min lp_201410">
                <div class="list">
                    <div class="special-gift">
                        <div class="headline">キャンペーン期間</div>
                        <div class="pd_10"><span>2015年7月2日(木)10:00から21:59まで</span></div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">対象</div>
                        <div class="pd_10">
                        <span>対象アプリで課金いただいたすべての方</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">WALLET ポイントの付与</div>
                        <div class="pd_10">
                        <span>WALLET ポイントの付与は、2015年8月上旬ごろを予定しております。</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">au WALLET について</div>
                        <span class="step-text" style="text-align:left;">au WALLET についての詳細は、<a href="https://wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">こちら</a>をご覧ください。</span>
                        <div class="img_center pd_10">
                            <a href="https://wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">
                            <img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text08.png">
                            </a>
                        </div>
                        <div class="btn-box_half pb_15 clearfix">
                            <div class="operation-btn-box"> <a href="https://wallet.auone.jp/contents/sp/guide/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET ご利用ガイド',event);">ご利用ガイド</a></div>
                            <div class="operation-btn-box"> <a href="https://wallet.auone.jp/contents/sp/help/index.html" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET よくある質問',event);">よくある質問</a></div>
                        </div>
                    </div>
                </div>

                <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">注意事項</div>
                        <div class="pd_10">
                            <span>・キャンペーン対象期間外に条件を達成した場合は、通常の10％還元となります。<br>
                            ・本キャンペーンは、予告なく変更または中止する場合がございます。<br>
                            ・メンテナンス等の関係で、アプリのダウンロードができない場合がございます。<br>
                            ・事務局より、本キャンペーンに関わるEメールを送付することがありますのでご了承ください。</span>
                        </div>
                    </div>
                </div>
                <div class="banner-medium banner">
                    <a href="http://st.pass.auone.jp/st/tokuten/pass-day/android.html?medid=pass-day&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','戻りリンク用バナー_スマパスの日',event);">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/banner_320_90_01.png" alt="auスマートパスの日　すんごいお得!!詳しくはこちら">
                    </a>
                </div>
                <?php inc("adRect_android"); ?>
            </div><!--all-games ranking container-min lp_201410-->
        </div><!--contens-body-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>
