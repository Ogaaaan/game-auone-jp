<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>auゲーム[バトルガール ハイスクール]グッズプレゼント</title>
    <meta name="keywords" content="バトルガール ハイスクール,グッズプレゼント,auゲーム,au WALLET,ポイント,プレゼント,ゲーム,アプリ,スマートパス,スマパス" />
    <meta name="description" content="バトルガール ハイスクールグッズが当たる！" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
    <style>
    #lp_201504-1 .nav{margin-bottom:15p;}
    #lp_201504-1 .list{margin:19px 16px;}
    #lp_201504-1 .apps img{width:auto;height:auto;float:none;}
    #lp_201504-1 .pb_20{padding-bottom:2rem;}
    #lp_201504-1 .p_20{padding:2rem;}
    #lp_201504-1 .mb_20{margin-bottom:2.5rem;}
    #lp_201504-1 .txt25{font-size:1.8rem;line-height:2.4rem;}
    #lp_201504-1 .pdg1{padding:1rem 1rem 2rem 1rem}
    #lp_201504-1 .apps .btn_green{margin-bottom:2rem;}
    #lp_201504-1 .headline h3{font-size:2.4rem;}
    #lp_201504-1 .headline img{width:20px;padding-right:0.3rem;}
    #lp_201504-1 .apps .btn_green div{width:92%;background:#00643c;border-radius:5px;borx-sizing:border-box;text-align:center;margin:2rem auto 0 auto;padding-bottom:0.4rem;}
    #lp_201504-1 .apps .btn_green div p{background:#16b656;border-radius:5px;box-sizing:border-box;font-size:3.2rem;color:#fff;padding:2.6rem 0 2.4rem 0;font-weight:bold;text-align:right;}
    #lp_201504-1 .apps .btn_green div p img{padding:0 20px;}
    #lp_201504-1 .list_obg{padding:2rem;}
    #lp_201504-1 .list_obg p.txtM{color:#fff;font-size:2.2rem;line-height:2.6rem;}
    #lp_201504-1 .org_bdr{width:80%;border:1px solid #e85505;padding:5px;font-size:2.4rem;margin:0 auto;text-align:center;font-weight;bold;}
    #lp_201504-1 .org_bdr span{color:#e85505;font-size:3rem;}
    #lp_201504-1 a.btn_orange{
    background:#e85505;
    padding:2.2rem 0;
    display:block;
    width:80%;
    margin:0 auto;
    border-radius:0.3rem;
    color:#fff;
    text-align:center;
    font-size:2.4rem;
    }

    #lp_201504-1 .btn_orange2 div{width:92%;background:#784600;border-radius:5px;box-sizing:border-box;text-align:center;margin:2rem auto 2rem auto;padding-bottom:0.4rem;}
    #lp_201504-1 .btn_orange2 div a{background:#e95502;border-radius:5px;box-sizing:border-box;font-size:3.2rem;color:#fff;padding:2.6rem 0 2.4rem 0;font-weight:bold;text-align:right;display:block;width:100%;font-size:3.5rem;display:block;}
    #lp_201504-1 .btn_orange2 div a img{padding:0 20px 0 40px;}
    #lp_201504-1 .special-gift a:link {color:#0000b2;}

    @media screen and (max-width: 460px) {
    #lp_201504-1 .apps .btn_green div p{font-size:2.6rem;padding:0.8em 0 1.0rem 0;}
    .imgbox img{width:93%;}
    #lp_201504-1 .list_obg p.txtM{color:#fff;font-size:1.8rem;line-height:2.2rem;}
    #lp_201504-1 .headline h3 {font-size: 2.2rem;}
    #lp_201504-1 a.btn_orange{font-size:2.2rem;padding:2.0rem 0;}
    #lp_201504-1 .btn_orange2 div a{font-size:2.8rem;}
    }
    @media screen and (max-width: 420px) {
    #lp_201504-1 .btn_orange2 div a img{font-size:2.5rem;padding-left:20px;}
    #lp_201504-1 .apps .btn_green div p{font-size:2.2rem;}
    #lp_201504-1 .headline h3 {font-size: 2.0rem;}
    #lp_201504-1 a.btn_orange{font-size:2.0rem;padding:1.8rem 0;}
    }
    @media screen and (max-width: 360px) {
    #lp_201504-1 .apps .btn_green div p{font-size:2.0rem;}
    #lp_201504-1 .apps .btn_green div p img{padding:0 10px;}
    #lp_201504-1 .btn_orange2 div a{font-size:2.4rem;}
    #lp_201504-1 .btn_orange2 div a img{padding:0 15px;}
    #lp_201504-1 .headline h3 {font-size: 1.8rem;}
    #lp_201504-1 .txt25{font-size:1.6rem;line-height:2.2rem;}
    #lp_201504-1 a.btn_orange{font-size:1.8rem;padding:1.6rem 0;}
    }
    </style>
</head>

<body class="all-app" id="lp_201504-1">
    <div class="js-t-wrapper">
        <div class="contens-body">
            <?php inc("header"); ?>
            <!-- AD -->
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>
            <!-- /AD -->
<!-- Contents -->
            <div class="all-games special-games pb_15">
            <!-- Orange Area -->
                <div class="container-min">
                    <div class="top-title">
<!--- ● -->
                        <img src="<?php print($PATH['url_game']);?>img/lp/battlegirl/lp_battlegirl-hs_main.jpg" class="img_width-full">
<!--- ● -->

                    </div>

                    <div class="list_obg">
                        <p class="txtM">ここが凄い！auスマートパス版は､アイテム購入ごとに10%のWALLETポイントが還元されるのでお得！今やってる方もデータ移行すれば､そのまま利用できるよ！</p>
                    </div>

                    <div class="list mb_10 pb_20">
                        <ul class="apps">
                            <li>
<!-- ● -->
                                <a href="https://pass.auone.jp/app/r?redirect=2&id=4698010000040&rf=augame0815" onclick="trEventBe(this,'auゲーム','バトルガール_プレキャン','ダウンロード',event);">
<!-- ● -->
                                    <div class="btn_green">
                                        <div>
                                            <p>いますぐダウンロード<img src="<?php print($PATH['url_game']);?>img/lp/150416/arrow_gt.png" alt=">"></p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <p class="org_bdr">いつでも<span>10％還元</span></p>
                    </div>

                    <div class="list mb_10 pb_20">
                        <div class="headline pb_15">
                            <div class="middle">
                                <img src="<?php print($PATH['url_game']);?>img/lp/150416/icon_like.png">
                            </div>
                            <h3>すでにプレイ中の方も安心!</h3>
                        </div>

                        <p class="txt25 p_20 mb_20">auスマートパスアプリ以外ですでにプレイ中の方も、auスマートパスアプリにデータを引き継いでプレイを再開すれば、抽選を受けることができます。<br>ぜひこの機会に、ご利用ください。</p>

<!-- ● -->
                        <a href="https://game.auone.jp/app/transfer/4698010000040/?rf=augame0816" class="btn_orange"  onclick="trEventBe(this,'auゲーム','バトルガール_プレキャン','データ移行方法',event);">データ引き継ぎ方法</a>
<!-- ● -->
                    </div>
                </div>
                <!-- /Orange Area -->
            </div>
            <div class="all-games ranking container-min lp_201410">
                <!-- Gray Area -->
                <div class="list">
                    <div class="special-gift">
                        <div class="headline txt25">キャンペーン内容</div>
<!-- ● -->
                        <span class="step-text txt25 pdg1" style="text-align:left;">auスマートパス版「バトルガール ハイスクール」のクリアファイルセットを抽選でプレゼントします。</span>
<!-- ● -->
                    </div>
                </div>
                <div class="list">
                    <div class="special-gift">
                        <div class="headline txt25">プレゼント内容</div>
<!-- ● -->
                        <span class="step-text txt25 pdg1" style="text-align:left;">
                            バトルガールクリアファイルセット<br>
                            20名様<br>
                            <br>
                            ★当選者の発表は賞品の発送をもって代えさせていただきます。<br>
                        </span>
<!-- ● -->
                    </div>
                </div>
                <div class="list">
                    <div class="special-gift">
                        <div class="headline txt25">キャンペーン期間</div>
<!-- ● -->
                        <span class="step-text txt25 pdg1" style="text-align:left;">
                            2015年8月1日(土) 10:00から<br>
                            2015年8月7日(金) 23:59まで
                        </span>
<!-- ● -->
                    </div>
                </div>
                <div class="list">
                    <div class="special-gift">
                        <div class="headline txt25">対象</div>
                        <span class="step-text txt25 pdg1" style="text-align:left;">auスマートパス加入の方ならどなたでも応募できます。</span>
                    </div>
                </div>
                <div class="btn_orange2">
                    <div>
<!-- ● -->
                        <a href="https://pass.auone.jp/gate/?nm=1&ru=https%3a%2f%2fvideopass%2dcc%2eauone%2ejp%2fapply_present%2f" class="btn_orange"  onclick="trEventBe(this,'auゲーム','バトルガール_プレキャン','いますぐ応募',event);">いますぐ応募する<img src="<?php print($PATH['url_game']);?>img/lp/150416/arrow_gt.png" alt="&gt;"></a>
<!-- ● -->
                    </div>
                </div>
            <!-- /Gray Area -->
            </div>
 <!-- /Contents -->

            <!-- AD -->
            <div class="pb_15"><?php inc("adRect_android"); ?></div>
            <!-- /AD -->
            <?php inc("footer"); ?>
        </div><!--contens-body-->
    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>
</body>
</html>
