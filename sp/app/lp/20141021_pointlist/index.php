<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
<meta charset="UTF-8">

<title>オープニングキャンペーン - auゲーム</title>
<meta name="keywords" content="オープニングキャンペーン,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
<meta name="description" content="オープニングキャンペーンページです。遊んでおトクなポイントキャンペーン。" />



<?php inc("head"); ?>

<link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">

</head>

<body class="all-app">
<div class="js-t-wrapper">
<div class="contens-body">

<?php inc("header"); ?>
<?php inc("adSP_android"); ?>

        <!--//////////////////////
      all-games
    //////////////////////  -->
        <div class="all-games" id="#top">

            <!-- operation  -->
            <div class="ranking operation lp_201410 lp_20141021_pointcam_1">
			<strong class="sub_title">auゲーム　オープニングキャンペーン</strong>
                <div class="top-title pd_15"> <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text09.png"> </div>

                <div class="list">
<div class="headline">
              <div class="middle">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/icon_pointcam01.png">
              </div>
              <strong>対象アプリを新規ダウンロードすると…<br>抽選で最大10,000ポイントプレゼント！</strong>
            </div>
                    <div class="special-gift">
                        <div class="headline"> <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png"> 特典 </div>
                        <div class="bg_gray-free"> <span>キャンペーン期間中に対象アプリを新規でダウンロードしていただいた方の中から抽選でau WALLETポイントをプレゼント！</span> </div>
                    </div>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>期間</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"></div>
                    </div>
                    <p class="step-text">2014年10月21日（火）10:00～<br>
                        2014年11月7日（金）23:59</p>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>対象</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"> </div>
                    </div>
                    <p class="step-text">対象アプリを新規でダウンロードされた方</p>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>ポイント</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"> </div>
                    </div>
                    <p class="step-text">抽選で100～10,000ポイント<br>
                        <br>
                        1等：10,000ポイント（100名様）<br>
                        2等：5,000ポイント（300名様）<br>
                        3等：1,000ポイント（600名様）<br>
                        4等：500ポイント（1,000名様）<br>
                        5等：100ポイント（10,000名様） </p>
                    <div class="operation-btn-box"> <a href="../20141021_pointcam_1/" class="operation-button"　onclick="trEventBe(this,'auゲーム','オープニングキャンペーン一覧','第一弾詳細はこちら',event);">キャンペーン　詳細はこちら</a> </div>
                </div>
                <!--/list-->

<div class="list">
<div class="headline">
              <div class="middle">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/icon_pointcam02.png">
              </div>
              <strong>期間中に対象アプリで課金すると...<br>ポイント還元率が2倍の20%還元！</strong>
            </div>
                    <div class="special-gift">
                        <div class="headline"> <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png"> 特典 </div>
                        <div class="bg_gray-free"> <span>通常、対象アプリで課金いただいた金額の10%がポイント還元されるところを、対象期間中に限り2倍の20%を還元！</span> </div>
                    </div>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>期間</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"></div>
                    </div>
                    <p class="step-text">2014年11月1日（土）～<br>2014年11月30日（日）</p>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>対象</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"> </div>
                    </div>
                    <p class="step-text">対象アプリで課金いただいたすべての方</p>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>ポイント</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"> </div>
                    </div>
                    <p class="step-text pb_15">課金金額の20%分</p>
                    <div class="operation-btn-box">
                        <a href="https://game.auone.jp/app/lp/20141101_pointcam_2/" class="operation-button"　onclick="trEventBe(this,'auゲーム','オープニングキャンペーン一覧','第二弾詳細はこちら',event);">キャンペーン　詳細はこちら</a>
                    </div>
                    <!-- <div class="operation-btn-box operation-button no_link">Coming Soon</div> -->
                </div>
                <!--/list-->

<!--キャンペーンが減ったため増えるまでコメントアウト-->
<!--<div class="list">
<div class="headline">
              <div class="middle">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/icon_pointcam03.png">
              </div>
              <strong>期間中に対象アプリを起動すると...<br>最大で3,000ポイントプレゼント！</strong>
            </div>
                    <div class="special-gift">
                        <div class="headline"> <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png"> 特典 </div>
                        <div class="bg_gray-free"> <span>キャンペーン期間中に対象アプリを起動いただいた方の中から抽選でau WALLETポイントをプレゼント！</span> </div>
                    </div>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>期間</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"></div>
                    </div>
                    <p class="step-text">2014年11月中旬予定</p>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>対象</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"> </div>
                    </div>
                    <p class="step-text">対象アプリを起動いただいたすべての方</p>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>ポイント</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"> </div>
                    </div>
                    <p class="step-text pb_15">抽選で100～3,000ポイント</p>
                    <div class="operation-btn-box operation-button no_link">Coming Soon</div>
                </div>-->
                <!--/list-->

<!--<div class="list">
<div class="headline">
              <div class="middle">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/icon_pointcam04.png">
              </div>
              <strong>WebMoney/auWalletカードで課金すると、<br>還元率に5%上乗せ！</strong>
            </div>
                    <div class="special-gift">
                        <div class="headline"> <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png"> 特典 </div>
                        <div class="bg_gray-free"> <span>キャンペーン期間中に対象アプリをWebMoneyかauWalletカードで課金いただくと、還元率を5%上乗せいたします。</span> </div>
                    </div>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>期間</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"></div>
                    </div>
                    <p class="step-text">2014年12月初旬</p>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>対象</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"> </div>
                    </div>
                    <p class="step-text">対象アプリをWebMoneyかauWalletカードで課金いただいたすべての方</p>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>ポイント</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"> </div>
                    </div>
                    <p class="step-text pb_15">還元率を5％上乗せ</p>
                    <div class="operation-btn-box operation-button no_link">Coming Soon</div>
                </div>-->
                <!--/list-->

<!--<div class="list">
<div class="headline">
              <div class="middle">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/icon_pointcam05.png">
              </div>
              <strong>期間中に対象アプリで課金すると...<br>ポイント還元率が2倍の20%還元！</strong>
            </div>
                    <div class="special-gift">
                        <div class="headline"> <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png"> 特典 </div>
                        <div class="bg_gray-free"> <span>通常、対象アプリで課金いただいた金額の10%がポイント還元されるところを、対象期間中に限り2倍の20%を還元！</span> </div>
                    </div>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>期間</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"></div>
                    </div>
                    <p class="step-text">???????</p>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>対象</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"> </div>
                    </div>
                    <p class="step-text">対象アプリで課金いただいたすべての方</p>
                    <div class="step-icon-box">
                        <div class="step-icon">
                            <p>ポイント</p>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png"> </div>
                    </div>
                    <p class="step-text pb_15">課金金額の20%分</p>
                    <div class="operation-btn-box operation-button no_link">Coming Soon</div>
                </div>-->
                <!--/list-->

        	    </div><!--lp_20141021_pointcam_1-->
		      <?php inc("adRect_android"); ?>
		</div>

<?php inc("footer"); ?>

</div> <!--contens-body-->
</div>

<?php inc("script"); ?>
<?php inc("gtm"); ?>

</body>
</html>