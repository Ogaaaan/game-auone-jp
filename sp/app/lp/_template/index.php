<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>キャンペーン名 - auゲーム</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">
            <?php inc("header"); ?>
            <!-- AD -->
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>
            <!-- /AD -->
<!-- Contents -->
            <div class="all-games special-games pb_15">
                <!-- Orange Area -->
                <div class="container-min">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <p style="text-align:center;">Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents</p>
                    <br>
                    <img src="<?php print($PATH['url_game']);?>img/lp/xxxx/dammy.png" class="img_width-full">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>
                <!-- /Orange Area -->
            </div>
            <div class="all-games ranking container-min lp_201410">
                <!-- Gray Area -->
                <br>
                <br>
                <br>
                <br>
                <br>
                <p style="text-align:center;">Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents Contents</p>
                <br>
                <img src="<?php print($PATH['url_game']);?>img/lp/xxxx/dammy.png" class="img_width-full">
                <br>
                <br>
                <br>
                <br>
                <br>
                <!-- /Gray Area -->
            </div>
 <!-- /Contents -->

            <!-- AD -->
            <div class="pb_15"><?php inc("adRect_android"); ?></div>
            <!-- /AD -->
            <?php inc("footer"); ?>
        </div><!--contens-body-->
    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>
</body>
</html>
