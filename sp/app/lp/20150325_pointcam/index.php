<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>15%ポイント還元キャンペーン（3月） - auゲーム</title>
    <meta name="keywords" content="ポイントキャンペーン,au WALLET,ポイントプレゼント,ポイント還元,ゲーム,アプリ,スマートパス,スマパス,auスマートパス" />
    <meta name="description" content="auゲームのポイントキャンペーン。期間限定au WALLETポイントをプレゼント！" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <?php inc("adSP_android"); ?>

            <div class="all-games special-games" id="#top">
                <h3 class="sub_title">【ポイントキャンペーン】</h3>
                <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/lp_20150325_main.png" class="img_width-full">
                            <p class="text_left">通常、対象アプリで課金いただいた金額の10％がWALLETポイントで還元されるところを、キャンペーン期間に限り、追加で5％分のポイントをさらにプレゼント。</p>
                        </div>
                        <div class="list mb_10">
                            <div class="headline pb_15">
                                <div class="middle">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_flag.png">
                                </div>
                                <h3>キャンペーン対象アプリ</h3>
                            </div>
                            <ul class="apps">
                                <li>
                                    <a href="http://pass.auone.jp/app/detail?app_id=6002000000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_モンスターストライク',event);">
                                        <div class="border">
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png" alt="モンスターストライク">
                                            </div>
                                            <div class="app-text">
                                                <h4>モンスターストライク</h4>
                                                <p class="genre">アクション</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://pass.auone.jp/app/detail?app_id=4698010000022&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_魔法使いと黒猫のウィズ',event);">
                                        <div class="border">
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000022.png" alt="魔法使いと黒猫のウィズ">
                                            </div>
                                            <div class="app-text">
                                                <h4>魔法使いと黒猫のウィズ</h4>
                                                <p class="genre">ロールプレイング</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://pass.auone.jp/app/detail?app_id=9189400000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ポケコロ',event);">
                                        <div class="border">
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9189400000001.png" alt="ポケコロ">
                                            </div>
                                            <div class="app-text">
                                                <h4>ポケコロ</h4>
                                                <p class="genre">カジュアル</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://pass.auone.jp/app/detail?app_id=4526600000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ぼくらの甲子園！ポケット for auスマートパス',event);">
                                        <div class="border">
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4526600000001.png" alt="ぼくらの甲子園！ポケット for auスマートパス">
                                            </div>
                                            <div class="app-text">
                                                <h4>ぼくらの甲子園！ポケット for auスマートパス</h4>
                                                <p class="genre">スポーツ</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="list" style="margin-top:10px;">
                          <div class="headline">
                             <div class="middle">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_thumb.png">
                            </div>
                            <h3>すでにプレイ中の方も安心！</h3>
                        </div>

                        <div class="pd_10"><span>auスマートパスアプリ以外ですでにプレイ中の方も、auスマートパスアプリにデータを引き継いでプレイを再開すれば、今のレベルのままポイント還元を受けることができます。<br>
                         ぜひこの機会に、ご利用ください。</span>
                        </div>
                        <div class="operation-btn-box mb_10"><a href="https://game.auone.jp/app/transfer/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','データ引き継ぎ方法',event);">データ引き継ぎ方法</a>
                        </div>
                    </div>
                </div><!-- container-min lp_201410--><!-- Bg_orange -->
            </div><!-- all-games special-games-->
        </div><!--contens-body-->

        <div class="all-games ranking container-min lp_201410">
            <div class="list">
                <div class="special-gift">
                    <div class="headline">ポイント</div>
                    <div class="pd_10"><span>課金金額の15％分</span></div>
                </div>
            </div>

            <div class="list">
                <div class="special-gift">
                    <div class="headline">キャンペーン期間</div>
                    <div class="pd_10">
                        <span>2015年3月25日（水）0:00から<br>2015年3月31日（火）23:59まで</span>
                    </div>
                </div>
            </div>

            <div class="list">
                <div class="special-gift">
                    <div class="headline">対象</div>
                    <div class="pd_10">
                    <span>キャンペーン期間中に対象アプリ内で課金いただいたすべての方</span>
                    </div>
                </div>
            </div>

            <div class="list">
                <div class="special-gift">
                    <div class="headline">WALLETポイントの付与</div>
                    <div class="pd_10">
                    <span>WALLETポイントの付与は、2015年4月上旬ごろを予定しております。<br><br>
                        <b>※注意※</b><br>
                        『魔法使いと黒猫のウィズ』に関しては、ポイント付与が2分割されます。<br>
                        ・１回目：4月上旬に10％<br>
                        ・２回目：4月22日に 残りの5％<br>
                        ※獲得予定には『10％』と表記され、4月上旬には10％分のみの付与となりますが、22日に『auゲーム黒猫のウィズ+5％分』という文言で残りの5％が付与されますのでご安心下さい。その他の3アプリについては、全て獲得予定についても『15％』4月上旬に実際に付与されるポイントも『15％』となるのでご安心ください。
                    </span>
                    </div>
                </div>
            </div>

            <div class="list">
                <div class="special-gift">
                    <div class="headline">au WALLET について</div>
                    <span class="step-text" style="text-align:left;">au WALLET についての詳細は、<a href="https://wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">こちら</a>をご覧ください。</span>
                    <div class="img_center pd_10">
                        <a href="https://wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">
                        <img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text08.png">
                        </a>
                    </div>
                    <div class="btn-box_half pb_15 clearfix">
                        <div class="operation-btn-box"> <a href="https://wallet.auone.jp/contents/sp/guide/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET ご利用ガイド',event);">ご利用ガイド</a></div>
                        <div class="operation-btn-box"> <a href="https://wallet.auone.jp/contents/sp/help/index.html" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET よくある質問',event);">よくある質問</a></div>
                    </div>
                </div>
            </div>
            <div class="list mb_10">
                <div class="special-gift">
                    <div class="headline">注意事項</div>
                        <div class="pd_10">
                            <span>・キャンペーン対象期間外に条件を達成した場合は、通常の10％還元となります。<br>
                            ・本キャンペーンは、予告なく変更または中止する場合がございます。<br>
                            ・メンテナンス等の関係で、アプリのダウンロードができない場合がございます。<br>
                            ・事務局より、本キャンペーンに関わるEメールを送付することがありますのでご了承ください。</span>
                        </div>
                    </div>
                </div>
                <?php inc("adRect_android"); ?>
            </div>
        </div><!--all-games ranking container-min lp_201410-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>