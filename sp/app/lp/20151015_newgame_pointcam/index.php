<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>10月15日「新作ゲーム WALLET ポイント増量還元」キャンペーン - auゲーム</title>
    <meta name="keywords" content="ポイントキャンペーン,au WALLET,ポイントプレゼント,ポイントゲーム,アプリ,スマートパス,スマパス,auスマートパス,勇者と1000の魔王" />
    <meta name="description" content="auゲームのWALLET ポイントプレゼントキャンペーン。期間内に対象タイトルを新規ダウンロードまたは起動した方1,000名様に抽選で200ポイントをプレゼント！" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
    <style>
        .app-title p{font-size: 16px; font-weight: bold; padding: 20px 10px 5px 10px;}
        .app-text h4{font-weight: normal; color: #eb5505;}
        .border{margin-left: 0 !important;}
        .border img{ margin-left: 8px;}
    </style>
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>

            <div class="all-games special-games">
                <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title">
                            <div class="box_period clearfix mt_10">
                                <p>期間限定</p>
                                <p>10/15(木)10:00～10/31(土)23:59</p>
                            </div>
                            <img src="<?php print($PATH['url_game']);?>img/lp/lp_20151015newgame_main.png" class="img_width-full" alt="人気アプリダウンロードキャンペーン">
                            <p class="text_left">期間中に、キャンペーン対象タイトルでアプリ内課金をされた方全員に、課金額の20%分のWALLET ポイントをプレセント！<br><span style="font-size:10px;">※auスマートパス会員の方が対象です。</span></p>
                        </div>

                        <div class="list mb_10">
                            <div class="headline pb_15">
                                <div class="middle">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_flag.png">
                                </div>
                                <h3>キャンペーン対象アプリ</h3>
                            </div>
                            <ul class="apps">
                                <li>
                                    <a href="https://pass.auone.jp/app/detail?app_id=3705000000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_勇者と1000の魔王-覚醒-［ドットRPG］',event);">
                                        <div class="border">
                                            <div class="app-title">
                                                <p>勇者と1000の魔王-覚醒-［ドットRPG］</p>
                                            </div>
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_3705000000001.png" alt="勇者と1000の魔王-覚醒-［ドットRPG］">
                                            </div>
                                            <div class="app-text">
                                                <h4>趣のあるドット絵とBGM！</h4>
                                                <p class="genre">魅力的で多彩なドットキャラクターが君のもとに集結！最強のパーティを編成しよう！</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_thumb.png">
                                </div>
                                <h3>すでにプレイ中の方も安心！</h3>
                            </div>
                            <div class="pd_10"><span>auスマートパスアプリ以外ですでにプレイ中の方も、auスマートパスアプリにデータを引き継いでプレイを再開すれば、今のレベルのままポイント還元を受けることができます。<br>
                             ぜひこの機会に、ご利用ください。</span>
                            </div>
                            <div class="operation-btn-box mb_10"><a href="//game.auone.jp/app/transfer/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','データ引き継ぎ方法',event);">データ引き継ぎ方法</a>
                            </div>
                        </div>

                    </div>
                </div><!-- container-min lp_201410--><!-- Bg_orange -->
            </div><!-- all-games special-games-->

            <div class="all-games ranking container-min lp_201410">
                <div class="list">
                    <div class="special-gift">
                        <div class="headline">プレゼント内容</div>
                        <div class="pd_10"><span>課金金額の20％分のWALLET ポイント</span></div>
                    </div>
                </div>
                <div class="list">
                    <div class="special-gift">
                        <div class="headline">キャンペーン期間</div>
                        <div class="pd_10">
                            <span>2015年10月15日(木)10:00から<br>2015年10月31日(土)23:59まで</span>
                        </div>
                    </div>
                </div>
                <div class="list">
                    <div class="special-gift">
                        <div class="headline">対象</div>
                        <div class="pd_10">
                        <span>auスマートパス会員で、期間中に対象アプリで課金いただいたすべての方</span>
                        </div>
                    </div>
                </div>
                <div class="list">
                    <div class="special-gift">
                        <div class="headline">WALLET ポイントの付与</div>
                        <div class="pd_10">
                        <span>キャンペーン期間中に、対象タイトルでアプリ内課金をされたお客様全員に、課金額の20%分のWALLET ポイントを還元いたします。WALLET ポイントの付与は、2015年11月中旬を予定しております。</span>
                        </div>
                    </div>
                </div>
                <div class="list">
                    <div class="special-gift">
                        <div class="headline">au WALLET について</div>
                        <span class="step-text" style="text-align:left;">au WALLET についての詳細は、<a href="//wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">こちら</a>をご覧ください。</span>
                        <div class="img_center pd_10">
                            <a href="//wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">
                            <img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text08.png" alt="au WALLET">
                            </a>
                        </div>
                        <div class="btn-box_half pb_15 clearfix">
                            <div class="operation-btn-box"> <a href="//wallet.auone.jp/contents/sp/guide/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET ご利用ガイド',event);">ご利用ガイド</a></div>
                            <div class="operation-btn-box"> <a href="//wallet.auone.jp/contents/sp/help/index.html" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET よくある質問',event);">よくある質問</a></div>
                        </div>
                    </div>
                </div>
               <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">注意事項</div>
                        <div class="pd_10">
                            <span>
                            ・キャンペーン対象期間外に条件を達成した場合は抽選の対象外となります。<br>
                            ・本キャンペーンは、予告なく変更または中止する場合がございます。<br>
                            ・メンテナンス等の関係で、アプリのダウンロードができない場合がございます。<br>
                            ・事務局より、本キャンペーンに関わるEメールを送付することがありますのでご了承ください。
                            </span>
                        </div>
                    </div>
                </div>
                <?php inc("adRect_android"); ?>
            </div><!--all-games ranking container-min lp_201410-->
        </div><!--contens-body-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>