<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
<meta charset="UTF-8">
<title>ポイントキャンペーン第一弾 - auゲーム</title>
<meta name="keywords" content="ポイントキャンペーン第一弾,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
<meta name="description" content="ポイントキャンペーン第一弾の詳細ページです。対象アプリを新規ダウンロードすると、抽選で最大10,000ポイントプレゼント！" />

<?php inc("head"); ?>

<link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">

</head>

<body class="all-app">
<div class="js-t-wrapper">
<div class="contens-body">

<?php inc("header"); ?>
<?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games special-games lp_201410" id="#top">
       <h3 class="sub_title">auゲーム　オープニングキャンペーン</h3>

        <!-- operation  -->
       <div class="ranking container-min lp_201410">
       <div class="pb_10">

            <div class="top-title ">

            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text06.png" class="img_width-full" style="padding-top:15px;">
            <p class="text_left">キャンペーン期間中に対象アプリを新規でダウンロードしていただいた方の中から抽選でau WALLETポイントをプレゼント！
</p>
</div>


    <div class="list">
        <div class="headline pb_15">
            <div class="middle">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_flag.png">
            </div>
              <h3>キャンペーン対象アプリ</h3>
        </div>

        <div class="t-grid">
            <ul class="t-grid__3columns">
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=6002000000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_モンスターストライク',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png">
                            <p>モンスターストライク</p>
                        </div>
                    </a>
                </li>
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=4990700000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_誓いのキスは突然に',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4990700000001.png">
                            <p>誓いのキスは突然に</p>
                        </div>
                    </a>
                </li>
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=9189400000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_ポケコロ<',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9189400000001.png">
                            <p>ポケコロ</p>
                        </div>
                    </a>
                </li>
            </ul>
            <ul class="t-grid__3columns">
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=4698010000022&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_魔法使いと黒猫のウィズ',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000022.png">
                            <p>魔法使いと黒猫のウィズ</p>
                        </div>
                    </a>
                </li>
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=5235400000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_サウザンドメモリーズ',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_5235400000001.png">
                            <p>サウザンドメモリーズ</p>
                        </div>
                    </a>
                </li>
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=9005100000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_戦国X（センゴククロス）',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9005100000001.png">
                            <p>戦国X（センゴククロス）</p>
                        </div>
                    </a>
                </li>
           </ul>
            <ul class="t-grid__3columns">
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=4585600000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_ドラゴンファング',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4585600000001.png">
                            <p>ドラゴンファング</p>
                        </div>
                    </a>
                </li>
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=1597710000012&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_バーコードフットボーラー',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_1597710000012.png">
                            <p>バーコードフットボーラー</p>
                        </div>
                    </a>
                </li>
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=7132500000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_新章イケメン大奥◆禁じられた恋',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7132500000001.png">
                            <p>新章イケメン大奥◆禁じられた恋</p>
                        </div>
                    </a>
                </li>
            </ul>
            <ul class="t-grid__3columns">
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=4698010000030&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_蒼の三国志',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000030.png">
                            <p>蒼の三国志</p>
                        </div>
                    </a>
                </li>
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=4698000000006&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_プロ野球PRIDE',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000006.png">
                            <p>プロ野球PRIDE</p>
                        </div>
                    </a>
                </li>
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=4698000000007&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_恐竜ドミニオン',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000007.png">
                            <p>恐竜ドミニオン</p>
                        </div>
                    </a>
                </li>
            </ul>
            <ul class="t-grid__3columns">
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=4698010000005&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_秘宝探偵キャリー',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000005.png">
                            <p>秘宝探偵キャリー</p>
                        </div>
                    </a>
                </li>
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=4698010000018&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_ディズニー マジシャン・クロニクル',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000018.png">
                            <p>ディズニー マジシャン・クロニクル</p>
                        </div>
                    </a>
                </li>
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=7937300000003&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_sword of phantasia',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7937300000003.png">
                            <p>sword of phantasia</p>
                        </div>
                    </a>
                </li>
            </ul>
            <ul class="t-grid__3columns">
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=7937300000002&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_戦国幻想曲',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7937300000002.png">
                            <p>戦国幻想曲</p>
                        </div>
                    </a>
                </li>
                <li class="t-grid__column">
                    <a href="http://pass.auone.jp/app/detail?app_id=4698010000035&rf=gameportal" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','DLボタン_スリングショットブレイブズ',event);">
                        <div class="t-grid__inner">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000035.png">
                            <p>スリングショットブレイブズ</p>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
    </div> <!-- list-->
  </div>   <!-- Bg_orange-->



<div class="all-games container-min lp_201410 pb_15">


            <div class="list">
                <div class="special-gift">
                    <div class="headline">プレゼント内容</div>
                    <div class="img_center pt-pb_15"><img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text05.png"></div>
                </div>
            </div>

            <div class="list">
                <div class="special-gift">
                    <div class="headline">キャンペーン期間</div>
                    <div class="img_center pt-pb_15"><img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text07.png"></div>
                </div>
            </div>


            <div class="list">
                <div class="special-gift">
                    <div class="headline">応募方法</div>
                    <div class="pd_10">
                        <span>対象アプリ一覧からダウンロードページに遷移いただき、ゲームアプリをダウンロードしてください。キャンペーン対象期間中に対象アプリを新規でダウンロードいただいた時点で、自動的に応募完了となります。</span>
                    </div>
                </div>
            </div>

            <div class="list">
                <div class="special-gift">
                    <div class="headline">抽選／WALLETポイントの付与</div>
                    <div class="pd_10">
                        <span>キャンペーン終了後、厳正なる抽選のうえ当選者を決定いたします。<br>
                        当選者へのWALLETポイントの付与は、11月下旬ごろを予定しております。<br>
                        なお、当選者の発表はWALLETポイントの付与をもって、代えさせていただきます。</span>
                    </div>
                </div>
            </div>
            <!-- list -->

            <div class="list">
                <div class="special-gift">
                    <div class="headline">au WALLETについて</div>
                    <span class="step-text" style="text-align:left;">au WALLET についての詳細は、<a href="//wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','au WALLET',event);">こちら</a>をご覧ください。</span>
                    <div class="img_center pd_10">
                        <a href="//wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','au WALLET',event);">
                            <img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text08.png">
                        </a>
                    </div>
                    <div class="btn-box_half pb_15 clearfix">
                        <div class="operation-btn-box"> <a href="//wallet.auone.jp/contents/sp/guide/" class="operation-button" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','au WALLET ご利用ガイド',event);">ご利用ガイド</a> </div>
                        <div class="operation-btn-box"> <a href="//wallet.auone.jp/contents/sp/help/index.html" class="operation-button" onclick="trEventBe(this,'auゲーム','キャンペーン第一弾','au WALLET よくある質問',event);">よくある質問</a> </div>
                    </div>
                </div>
            </div>
            <!-- list -->

            <div class="list mb_10">
                <div class="special-gift">
                    <div class="headline">注意事項</div>
                    <div class="pd_10">
                        <span>・キャンペーン対象期間外に条件を達成した場合やポイント付与までにauスマートパスを解約された場合は、抽選の対象外となります。<br>
                        ・本キャンペーンは、予告なく変更または中止する場合がございます。<br>
                        ・メンテナンス等の関係で、アプリのダウンロードができない場合がございます。</span>
                    </div>
                </div>
            </div>


      <?php inc("adRect_android"); ?>
    </div>
    </div>
<?php inc("footer"); ?>

</div><!--contens-body-->


<?php inc("script"); ?>
<?php inc("gtm"); ?>

</body>
</html>