<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>WALLET ポイントでおトクに遊べる！ - auゲーム</title>
    <meta name="keywords" content="au WALLET,ポイントプレゼント,アプリ,スマートパス,スマパス,auスマートパス,モンスト,モンスターストライク,黒猫のウィズ,白猫,人気ゲーム" />
    <meta name="description" content="人気のゲームアプリでプレイした分だけWALLET ポイントがたまる、お得なゲームサイト。たまったポイントはauゲーム対象アプリ内でアイテム交換に使えます！" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/game-top.css">
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>

            <div class="all-games special-games">
                <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title">
                            <img src="<?php print($PATH['url_game']);?>img/lp/lp_walletpoint_main.png" alt="人気のゲームアプリで遊んでもらえる！たまったポイントはアイテム交換に使える！" class="img_width-full mt_10">
                        </div>
                        <!-- banner -->
                        <div class="top clearfix">
                            <div class="banner-small banner banner-left">
                                <div class="box-left">
                                    <a href="/app/howto/" onclick="trEventBe(this,'auゲーム','トップページ','小バナー_auゲームの使い方_New2',event);">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/new_banner_small_beginner.png" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="banner-small banner banner-left">
                                <div class="box-left">
                                    <a href="/app/android/howto_transfer/" onclick="trEventBe(this,'auゲーム','トップページ','小バナー_引き継ぎ促進_new',event);">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/top/new_banner_small_transfer.png" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- /banner -->
                        <div class="list mb_10">
                            <div class="headline">
                                <div class="middle">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_flag.png">
                                </div>
                                <h3 class="list__crosshead">WALLET ポイントがもらえる！使える！ゲームアプリ</h3>
                            </div>
                            <!-- grid -->
                            <div class="t-grid top-grid badge-icon">
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=6002000000001&rf=augame0861" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_モンスターストライク',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-favored">人気</div>
                                                    <div class="grid__inner-right"><p>モンスターストライク</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000038&rf=augame0878" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_白猫プロジェクト',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000038.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-favored">人気</div>
                                                    <div class="grid__inner-right"><p>白猫プロジェクト</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                     <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000022&rf=augame0864" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_魔法使いと黒猫のウィズ',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000022.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-new">NEW</div>
                                                    <div class="grid__inner-right"><p>魔法使いと黒猫のウィズ</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000040&rf=augame0881" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_バトルガールハイスクール',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000040.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-new">NEW</div>
                                                    <div class="grid__inner-right"><p>バトルガールハイスクール</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000039&rf=augame0880" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ほしの島のにゃんこ',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000039.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-new">NEW</div>
                                                    <div class="grid__inner-right"><p>ほしの島のにゃんこ</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4526600000001&rf=augame0877" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ぼくらの甲子園！ポケット for auスマートパス',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4526600000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-new">NEW</div>
                                                    <div class="grid__inner-right"><p>ぼくらの甲子園！ポケット for auスマートパス</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=5235400000001&rf=augame0862" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_サウザンドメモリーズ',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_5235400000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-recommend">オススメ</div>
                                                    <div class="grid__inner-right"><p>サウザンドメモリーズ</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=9189400000001&rf=augame0863" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ポケコロ',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9189400000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-lady">女子</div>
                                                    <div class="grid__inner-right"><p>ポケコロ</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=1597710000012&rf=augame0869" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_バーコードフットボーラー',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_1597710000012.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-sports">スポーツ</div>
                                                    <div class="grid__inner-right"><p>バーコードフットボーラー</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000035&rf=augame0876" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_スリングショットブレイブズ',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000035.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-recommend">オススメ</div>
                                                    <div class="grid__inner-right"><p>スリングショットブレイブズ</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=7132500000001&rf=augame0865" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_新章イケメン大奥◆禁じられた恋',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7132500000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-lady">女子</div>
                                                    <div class="grid__inner-right"><p>新章イケメン大奥◆禁じられた恋</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4990700000001&rf=augame0866" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_誓いのキスは突然に',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4990700000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-lady">女子</div>
                                                    <div class="grid__inner-right"><p>誓いのキスは突然に</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4585600000001&rf=augame0868" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ドラゴンファング',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4585600000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-recommend">オススメ</div>
                                                    <div class="grid__inner-right"><p>ドラゴンファング</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=9005100000001&rf=augame0867" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_戦国X（センゴククロス）',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9005100000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-battle">バトル</div>
                                                    <div class="grid__inner-right"><p>戦国X（センゴククロス）</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=1732700000001&rf=augame0879" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_どかだん for auスマートパス',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_1732700000001.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-new">NEW</div>
                                                    <div class="grid__inner-right"><p>どかだん for auスマートパス</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698000000006&rf=augame0871" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_プロ野球PRIDE',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000006.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-sports">スポーツ</div>
                                                    <div class="grid__inner-right"><p>プロ野球PRIDE</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000030&rf=augame0870" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_蒼の三国志',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000030.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-battle">バトル</div>
                                                    <div class="grid__inner-right"><p>蒼の三国志</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000018&rf=augame0874" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ディズニー マジシャン・クロニクル',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000018.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-rpg">RPG</div>
                                                    <div class="grid__inner-right"><p>ディズニー マジシャン・クロニクル</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="t-grid__3columns top-grid">
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698000000007&rf=augame0872" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_恐竜ドミニオン',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000007.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-battle">バトル</div>
                                                    <div class="grid__inner-right"><p>恐竜ドミニオン</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=4698010000005&rf=augame0873" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_秘宝探偵キャリー',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000005.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-battle">バトル</div>
                                                    <div class="grid__inner-right"><p>秘宝探偵キャリー</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="t-grid__column">
                                        <a href="https://pass.auone.jp/app/detail?app_id=7937300000003&rf=augame0875" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_sword of phantasia',event);">
                                            <div class="t-grid__inner">
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7937300000003.png" alt="">
                                                <div class="grid__inner-wrap">
                                                    <div class="grid__inner-left badge-rpg">RPG</div>
                                                    <div class="grid__inner-right"><p>sword of phantasia</p></div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- grid -->
                        </div>
                    </div>
                </div><!-- container-min lp_201410-->
            </div><!-- all-games special-games--><!-- Bg_orange -->

            <div class="all-games">
                <?php inc("adRect_android"); ?>
            </div>
        </div><!--contens-body-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>
