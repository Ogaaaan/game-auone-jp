<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>『ぼくとドラゴン』ダウンロード - auゲーム</title>
    <meta name="keywords" content="au WALLET,ポイントプレゼント,ポイントゲーム,アプリ,スマートパス,スマパス,auスマートパス,ぼくとドラゴン,ぼくドラ" />
    <meta name="description" content="auゲーム「ぼくとドラゴン」のダウンロードページ。キャンペーン中に新規ダウンロードまたは起動した方に抽選で200ポイントをプレゼント！" />
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
    <style>
    .mb_0 {
        margin-bottom: 0;
    }
    .download_btn_wrapper {
        padding: 10px 10px 20px 10px;
        margin: 10px;
        background: #fff;
        border-radius: 10px;
    }
    .download_btn {
        position: relative;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        margin: 10px;
    }
    .download_btn a {
        display: box;
        display: -webkit-box;
        box-align: center;
        -webkit-box-align: center;
        border-radius: 10px;
        background: #16b656;
        padding: 10px;
        box-shadow: 0 10px 0 0 #045e28;
    }
    .download_btn img:first-child {
        display: block;
        width: 50px;
        padding: 5px 0;
    }
    .download_btn p {
        display: block;
        font-size: 15px;
        color: #fff;
        font-weight: bold;
        line-height: 1.2em;
        margin: 0 10px;
    }
    .download_btn img:last-child {
        display: block;
        position:absolute;
        right: 10px;
        top: 32px;
        width: 15px;
        height: 15px;

    }
    .title_txt p {
        text-align: center;
        padding: 0 10px;
        font-size: 14px;
        font-weight: bold;
        line-height: 1.5;
        color: #666;
        margin-top: 16px;
    }
    .txt_gray {
        color: #666;
    }
    </style>
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>

            <div class="all-games special-games mb_0">
                 <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title">
                            <img src="<?php print($PATH['url_game']);?>img/lp/dlcam/lp_bokudora_main.png" class="img_width-full" alt="全員に還元率プラス5％　WALLET ポイントで合計15％戻ってくる！">
                        </div>
                        <div class="download_btn_wrapper">
                            <!-- DLボタン-->
                            <div class="download_btn">
                                <a href="https://pass.auone.jp/app/r?id=6721200000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ぼくドラDLページ','DLボタン_ぼくとドラゴン',event);">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6721200000001.png">
                                    <p>いますぐダウンロード</p>
                                    <img src="//cdn-img.auone.jp/pass/sp/game/img/lp/150416/arrow_gt.png" alt=">">
                                </a>
                            </div>
                            <!-- /DLボタン-->
                        </div>

                     </div>
                </div><!-- container-min lp_201410--><!-- Bg_orange -->
            </div><!-- all-games special-games-->

            <div class="title_txt">
                <p>新作リリース記念！ <br>au WALLET ポイントプレゼントキャンペーン</p>
            </div>

            <div class="all-games ranking container-min lp_201410">
                <div class="list">
                    <div class="special-gift">
                        <div class="headline">概要</div>
                        <div class="pd_10"><span class="txt_gray">
                            auゲーム対象アプリ「ぼくとドラゴン」をダウンロードしていただいた方の中から抽選で1,000名にWALLET ポイントを200ポイントプレゼントいたします。<br>※auスマートパス会員の方が対象です。
                        </span></div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">期間</div>
                        <div class="pd_10">
                        <span class="txt_gray">
                            2015年9月19日(土)0:00から<br>
                            2015年9月27日(日)23:59まで</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">発表</div>
                        <div class="pd_10">
                        <span class="txt_gray">
                        キャンペーン終了後、厳正なる抽選のうえ当選者を決定いたします。当選された方へは、ポイントの付与をもってかえさせていただきます。
                        </span>
                        </div>
                    </div>
                </div>

                <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">WALLET ポイント付与</div>
                        <div class="pd_10">
                            <span class="txt_gray">
                                2015年10月を予定しております。
                            </span>
                        </div>
                    </div>
                </div>

            </div>

            <div class="title_txt">
                <p>WALLET ポイント増量キャンペーン</p>
            </div>

            <div class="all-games ranking container-min lp_201410">

                <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">概要</div>
                        <div class="pd_10">
                            <span class="txt_gray">
                                スマートパス版「ぼくとドラゴン【仲間とわいわいリアルタイムバトル】」でアプリ内課金をされた方全員に、通常は課金額の10%ポイント還元のところ、20%ポイント還元します！
                            </span>
                        </div>
                    </div>
                </div>

                <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">期間</div>
                        <div class="pd_10">
                            <span class="txt_gray">
                                2015年9月1日(火) 0時00分から<br>
                                2015年9月30日(水) 23時59分まで
                            </span>
                        </div>
                    </div>
                </div>

                <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">WALLET ポイント付与</div>
                        <div class="pd_10">
                            <span class="txt_gray">
                                2015年10月を予定しております。
                            </span>
                        </div>
                    </div>
                </div>

               <?php inc("adRect_android"); ?>
            </div><!--all-games ranking container-min lp_201410-->
        </div><!--contens-body-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>
