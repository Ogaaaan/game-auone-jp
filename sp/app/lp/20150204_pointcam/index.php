<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
	<meta charset="UTF-8">
	<title>ポイントキャンペーン - auゲーム</title>
	<meta name="keywords" content="ポイントキャンペーン,au WALLET,ポイント,プレゼント,ゲーム,アプリ,スマートパス,スマパス" />
	<meta name="description" content="auゲームのポイントキャンペーン。" />

	<?php inc("head"); ?>

<link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
</head>

<body class="all-app">
	<div class="js-t-wrapper">
		<div class="contens-body">

			<?php inc("header"); ?>
			<?php inc("adSP_android"); ?>

      	<div class="all-games special-games" id="#top">
      	<h3 class="sub_title">【ポイントキャンペーン】</h3>

      	<!-- operation  -->
      	<div class="container-min lp_201410">

      		<div class="pb_15"><!-- Bg_orange-->
      			<div class="top-title ">
      				<img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/lp_20150204_main.png" class="img_width-full">
      				<p class="text_left">通常、対象アプリで課金いただいた金額の10%がWALLETポイントで還元されるところを、キャンペーン期間に限り、追加で5％分のポイントをさらにプレゼント。</p>
      			</div>
                <div class="list mb_10">
                      <div class="headline pb_15">
                       <div class="middle">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_flag.png">
                    </div>
                    <h3>キャンペーン対象アプリ</h3>
                </div>

                <ul class="apps">
                                            <li>
                            <a href="http://pass.auone.jp/app/detail?app_id=4698010000038&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_白猫プロジェクト',event);">
                                <div class="border">
                                    <div class="image">
                                        <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000038.png" alt="白猫プロジェクト">                                    </div>
                                    <div class="app-text">
                                        <h4>白猫プロジェクト</h4>
                                        <p class="genre">ロールプレイング</p>
                                        <p>アイテム100円購入ごとに10ポイント</p>
                                        <p class="gift-text">限定アイテム付！</p>
                                    </div>
                                </div>
                            </a>
                        </li>


                                            <li>
                            <a href="http://pass.auone.jp/app/detail?app_id=5235400000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_サウザンドメモリーズ',event);">
                                <div class="border">
                                    <div class="image">
                                        <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_5235400000001.png" alt="サウザンドメモリーズ">                                    </div>
                                    <div class="app-text">
                                        <h4>サウザンドメモリーズ</h4>
                                        <p class="genre">ロールプレイング</p>
                                        <p>アイテム100円購入ごとに10ポイント</p>
                                        <p class="gift-text">限定アイテム付！</p>
                                    </div>
                                </div>
                            </a>
                        </li>


                                            <li>
                            <a href="http://pass.auone.jp/app/detail?app_id=9005100000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_戦国X（センゴククロス）',event);">
                                <div class="border">
                                    <div class="image">
                                        <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9005100000001.png" alt="戦国X（センゴククロス）">                                    </div>
                                    <div class="app-text">
                                        <h4>戦国X（センゴククロス）</h4>
                                        <p class="genre">シミュレーション・ストラテジー</p>
                                        <p>アイテム100円購入ごとに10ポイント</p>
                                        <p class="gift-text">限定アイテム付！</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                         <li>
                            <a href="http://pass.auone.jp/app/detail?app_id=4585600000001&amp;rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ドラゴンファング',event);">
                                <div class="border">
                                    <div class="image">
                                        <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4585600000001.png" alt="ドラゴンファング">                                    </div>
                                    <div class="app-text">
                                        <h4>ドラゴンファング</h4>
                                        <p class="genre">ロールプレイング</p>
                                        <p>アイテム100円購入ごとに10ポイント</p>
                                        <p class="gift-text"></p>
                                    </div>
                                </div>
                            </a>
                        </li>
                </ul>
            </div>

    	<div class="list" style="margin-top:10px;">
    		<div class="headline">
    			<div class="middle">
    				<img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_thumb.png">
    			</div>
    			<h3>すでにプレイ中の方も安心！</h3>
    		</div>

    		<div class="pd_10"><span>auスマートパスアプリ以外ですでにプレイ中の方も、auスマートパスアプリにデータを引き継いでプレイを再開すれば、今のレベルのままポイント還元を受けることができます。<br>
    			ぜひこの機会に、ご利用ください。</span></div>
    			<div class="operation-btn-box mb_10"> <a href="https://game.auone.jp/app/transfer/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','データ引き継ぎ方法',event);">データ引き継ぎ方法</a> </div>
    		</div> <!-- list-->
    	</div> <!-- Bg_orange-->
    </div>
</div>


 <div class="all-games ranking container-min lp_201410">
    	<div class="list">
    		<div class="special-gift">
    			<div class="headline">ポイント</div>
    			<div class="pd_10">
    				<span>課金金額の15％分</span>
    			</div>
    		</div>
    	</div>

    	<div class="list">
    		<div class="special-gift">
    			<div class="headline">キャンペーン期間</div>
    			<div class="pd_10">
    			<span>2015年2月7日（土）0:00から<br>2015年2月15日（日）23:59 まで</span>
    			</div>
    		</div>
    	</div>

    	<div class="list">
    		<div class="special-gift">
    			<div class="headline">対象</div>
    			<div class="pd_10">
    				<span>対象アプリ内で課金いただいたすべての方</span>
    			</div>
    		</div>
    	</div>

    	<div class="list">
    		<div class="special-gift">
    			<div class="headline">WALLETポイントの付与</div>
    			<div class="pd_10">
    				<span>WALLETポイントの付与は、2015年3月上旬ごろを予定しております。
白猫プロジェクトのみ、「ポイント獲得・利用履歴」には獲得予定に、10%還元の付与ポイントが表示されますが3月上旬に別途追加で5%分が還元されますのでご安心ください。その他タイトルについては、獲得予定時点で15%還元の付与ポイントが表示されます。</span>
    				</div>
    			</div>
    		</div>

    		<div class="list">
    			<div class="special-gift">
    				<div class="headline">au WALLETについて</div>
    				<span class="step-text" style="text-align:left;">au WALLET についての詳細は、<a href="https://wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">こちら</a>をご覧ください。</span>
    				<div class="img_center pd_10">
    					<a href="https://wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">
    						<img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text08.png">
    					</a>
    				</div>
    				<div class="btn-box_half pb_15 clearfix">
    					<div class="operation-btn-box"> <a href="https://wallet.auone.jp/contents/sp/guide/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET ご利用ガイド',event);">ご利用ガイド</a> </div>
    					<div class="operation-btn-box"> <a href="https://wallet.auone.jp/contents/sp/help/index.html" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET よくある質問',event);">よくある質問</a> </div>
    				</div>
    			</div>
    		</div>

    		<div class="list mb_10">
    			<div class="special-gift">
    				<div class="headline">注意事項</div>
    				<div class="pd_10">
    					<span>・キャンペーン対象期間外に条件を達成した場合は、通常の10％還元となります。<br>
    						・本キャンペーンは、予告なく変更または中止する場合がございます。<br>
    						・メンテナンス等の関係で、アプリのダウンロードができない場合がございます。<br>
                            ・事務局より、本キャンペーンに関わるEメールを送付することがありますのでご了承ください。</span>
    					</div>
    				</div>
    			</div>
    			<?php inc("adRect_android"); ?>
    		</div>
    	</div>

    		<?php inc("footer"); ?>

    	</div><!--contens-body-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>