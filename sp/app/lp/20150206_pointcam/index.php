<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
	<meta charset="UTF-8">
	<title>ポイントキャンペーン - auゲーム</title>
	<meta name="keywords" content="ポイントキャンペーン,au WALLET,10000ポイント,プレゼント,ゲーム,アプリ,スマートパス,スマパス" />
	<meta name="description" content="auゲームのポイントキャンペーン。au WALLETポイントを抽選で20名様に10,000ポイントプレゼント！" />

<link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
<link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">

<?php inc("head"); ?>
<link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
</head>

<body class="all-app">
	<div class="js-t-wrapper">
		<div class="contens-body">

			<?php inc("header"); ?>
			<?php inc("adSP_android"); ?>

         <div class="all-games special-games" id="#top">
             <h3 class="sub_title">【ポイントキャンペーン】</h3>

             <!-- operation  -->
            <div class="container-min lp_201410">
                <div class="pb_15">
                <div class="top-title">
                      <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/lp_20150206_main.png" class="img_width-full">
                      <p class="text_left">期間中に対象アプリを新規で1つ以上ダウンロードいただくか、もしくは対象アプリで1,000円以上課金いただいた方の中から抽選でau WALLETポイントをプレゼント！</p>
                  </div>
                      <div class="list mb_10">
                      <div class="headline pb_15">
                         <div class="middle">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_flag.png">
                        </div>
                        <h3>キャンペーン対象アプリ</h3>
                    </div>

                <div class="t-grid top-grid">
                        <ul class="t-grid__3columns top-grid">
                        <li class="t-grid__column">
                            <a href="http://pass.auone.jp/app/detail?app_id=4698010000038&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_白猫プロジェクト',event);">
                                <div class="t-grid__inner">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000038.png" alt="">
                                    <p>白猫プロジェクト</p>
                                </div>
                            </a>
                        </li>
                        <li class="t-grid__column">
                            <a href="http://pass.auone.jp/app/detail?app_id=6002000000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_モンスターストライク',event);">
                                <div class="t-grid__inner">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png" alt="">                                    <p>モンスターストライク</p>
                                </div>
                            </a>
                        </li>
                        <li class="t-grid__column">
                            <a href="http://pass.auone.jp/app/detail?app_id=4698010000022&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_魔法使いと黒猫のウィズ',event);">
                                <div class="t-grid__inner">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000022.png" alt="">                                    <p>魔法使いと黒猫のウィズ</p>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <ul class="t-grid__3columns top-grid">
                        <li class="t-grid__column">
                            <a href="http://pass.auone.jp/app/detail?app_id=4526600000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ぼくらの甲子園！ポケット for auスマートパス',event);">
                                <div class="t-grid__inner">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4526600000001.png" alt="">                                    <p>ぼくらの甲子園！ポケット for auスマートパス</p>
                                </div>
                            </a>
                        </li>
                        <li class="t-grid__column">
                        <a href="http://pass.auone.jp/app/detail?app_id=1597710000012&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_バーコードフットボーラー',event);">
                            <div class="t-grid__inner">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_1597710000012.png" alt="">                                    <p>バーコードフットボーラー</p>
                            </div>
                        </a>
                    </li>


                    <li class="t-grid__column">
                        <a href="http://pass.auone.jp/app/detail?app_id=4698010000030&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_蒼の三国志',event);">
                            <div class="t-grid__inner">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000030.png" alt="">                                    <p>蒼の三国志</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <ul class="t-grid__3columns top-grid">
                    <li class="t-grid__column">
                        <a href="http://pass.auone.jp/app/detail?app_id=4698010000035&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_スリングショットブレイブズ',event);">
                            <div class="t-grid__inner">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000035.png" alt="">                                    <p>スリングショットブレイブズ</p>
                            </div>
                        </a>
                    </li>
                    <li class="t-grid__column">
                <a href="http://pass.auone.jp/app/detail?app_id=4990700000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_誓いのキスは突然に',event);">
                    <div class="t-grid__inner">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4990700000001.png" alt="">                                    <p>誓いのキスは突然に</p>
                    </div>
                </a>
            </li>
            <li class="t-grid__column">
                <a href="http://pass.auone.jp/app/detail?app_id=9189400000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ポケコロ',event);">
                    <div class="t-grid__inner">
                        <img src="https://cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9189400000001.png" alt="">                                    <p>ポケコロ</p>
                    </div>
                </a>
            </li>
        </ul>
        <ul class="t-grid__3columns top-grid">
            <li class="t-grid__column">
                <a href="http://pass.auone.jp/app/detail?app_id=7132500000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_新章イケメン大奥◆禁じられた恋',event);">
                    <div class="t-grid__inner">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7132500000001.png" alt="">                                    <p>新章イケメン大奥◆禁じられた恋</p>
                    </div>
                </a>
            </li>
            <li class="t-grid__column">
                <a href="http://pass.auone.jp/app/detail?app_id=4698000000006&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_プロ野球PRIDE',event);">
                    <div class="t-grid__inner">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000006.png" alt="">                                    <p>プロ野球PRIDE</p>
                    </div>
                </a>
            </li>
            <li class="t-grid__column">
                <a href="http://pass.auone.jp/app/detail?app_id=4698010000018&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ディズニー マジシャン・クロニクル',event);">
                    <div class="t-grid__inner">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000018.png" alt="">                                    <p>ディズニー マジシャン・クロニクル</p>
                    </div>
                </a>
            </li>
        </ul>
        <ul class="t-grid__3columns top-grid">
            <li class="t-grid__column">
                <a href="http://pass.auone.jp/app/detail?app_id=4698000000007&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_恐竜ドミニオン',event);">
                    <div class="t-grid__inner">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000007.png" alt="">                                    <p>恐竜ドミニオン</p>
                    </div>
                </a>
                </li>
                <li class="t-grid__column">
                <a href="http://pass.auone.jp/app/detail?app_id=7937300000003&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_sword of phantasia',event);">
                    <div class="t-grid__inner">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7937300000003.png" alt="">                                    <p>sword of phantasia</p>
                    </div>
                </a>
            </li>
            <li class="t-grid__column">
                <a href="http://pass.auone.jp/app/detail?app_id=4698010000005&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_秘宝探偵キャリー',event);">
                    <div class="t-grid__inner">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000005.png" alt="">                                    <p>秘宝探偵キャリー</p>
                    </div>
                </a>
            </li>
        </ul>
        <ul class="t-grid__3columns top-grid">
            <li class="t-grid__column">
                <a href="http://pass.auone.jp/app/detail?app_id=9005100000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_戦国X（センゴククロス）',event);">
                    <div class="t-grid__inner">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9005100000001.png" alt="">                                    <p>戦国X（センゴククロス）</p>
                    </div>
                </a>
            </li>
        <li class="t-grid__column">
            <a href="http://pass.auone.jp/app/detail?app_id=4585600000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ドラゴンファング',event);">
                <div class="t-grid__inner">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4585600000001.png" alt="">                                    <p>ドラゴンファング</p>
                </div>
            </a>
        </li>
        <li class="t-grid__column">
            <a href="http://pass.auone.jp/app/detail?app_id=5235400000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_サウザンドメモリーズ',event);">
                <div class="t-grid__inner">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_5235400000001.png" alt="">                                    <p>サウザンドメモリーズ</p>
                </div>
            </a>
        </li>
    </ul>
</div>

                </div>

                <div class="list" style="margin-top:10px;">
                  <div class="headline">
                     <div class="middle">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_thumb.png">
                    </div>
                    <h3>すでにプレイ中の方も安心！</h3>
                </div>

                <div class="pd_10"><span>auスマートパスアプリ以外ですでにプレイ中の方も、auスマートパスアプリにデータを引き継いでプレイを再開すれば、今のレベルのままポイント還元を受けることができます。<br>
                 ぜひこの機会に、ご利用ください。</span></div>
                 <div class="operation-btn-box mb_10"> <a href="https://game.auone.jp/app/transfer/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','データ引き継ぎ方法',event);">データ引き継ぎ方法</a> </div>
             </div> <!-- list-->
         </div> <!-- Bg_orange-->
     </div>
 </div>

 <div class="all-games ranking container-min lp_201410">
   <div class="list">
      <div class="special-gift">
         <div class="headline">プレゼント内容</div>
         <div class="pd_10">
            <span>10,000ポイント　｜　20名様</span>
        </div>
    </div>
</div>

<div class="list">
  <div class="special-gift">
     <div class="headline">キャンペーン期間</div>
     <div class="pd_10">
         <span>2015年2月7日（土）0:00から<br>2015年2月15日（日）23:59まで</span>
     </div>
 </div>
</div>

<div class="list">
  <div class="special-gift">
     <div class="headline">応募方法</div>
     <div class="pd_10">
        <span>キャンペーン期間中に以下の条件のうち、いずれかを満たしたお客さまの中から抽選で20名様へ10,000ポイントプレゼント。<br>
①対象アプリを新規で1つ以上ダウンロード<br>
②対象アプリで、1,000円以上の課金
</span>
    </div>
</div>
</div>

<div class="list">
  <div class="special-gift">
     <div class="headline">抽選／WALLETポイントの付与</div>
     <div class="pd_10">
        <span>キャンペーン終了後、厳正なる抽選のうえ当選者を決定いたします。<br>当選者へのWALLETポイントの付与は、2015年2月下旬ごろを予定しております。<br>なお、当選者の発表はWALLETポイントの付与をもって、代えさせていただきます。
</span>
    </div>
</div>
</div>

<div class="list">
 <div class="special-gift">
    <div class="headline">au WALLETについて</div>
    <span class="step-text" style="text-align:left;">au WALLET についての詳細は、<a href="https://wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">こちら</a>をご覧ください。</span>
    <div class="img_center pd_10">
       <a href="https://wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">
          <img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text08.png">
      </a>
  </div>
  <div class="btn-box_half pb_15 clearfix">
   <div class="operation-btn-box"> <a href="https://wallet.auone.jp/contents/sp/guide/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET ご利用ガイド',event);">ご利用ガイド</a> </div>
   <div class="operation-btn-box"> <a href="https://wallet.auone.jp/contents/sp/help/index.html" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET よくある質問',event);">よくある質問</a> </div>
</div>
</div>
</div>

<div class="list mb_10">
 <div class="special-gift">
    <div class="headline">注意事項</div>
    <div class="pd_10">
       <span>・キャンペーン対象期間外に条件を達成した場合は、通常の10％還元となります。<br>
          ・本キャンペーンは、予告なく変更または中止する場合がございます。<br>
          ・メンテナンス等の関係で、アプリのダウンロードができない場合がございます。<br>
          ・事務局より、本キャンペーンに関わるEメールを送付することがありますのでご了承ください。</span>
      </div>
  </div>
</div>
<?php inc("adRect_android"); ?>
</div>
</div>

<?php inc("footer"); ?>

</div><!--contens-body-->
<?php inc("script"); ?>
<?php inc("gtm"); ?>

</body>
</html>