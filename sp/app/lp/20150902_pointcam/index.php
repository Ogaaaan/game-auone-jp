<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>9月2日「auスマートパスの日」キャンペーン - auゲーム</title>
    <meta name="keywords" content="ポイントキャンペーン,au WALLET,ポイント還元,15%,プレゼント,ゲーム,アプリ,スマートパス,スマパス,auスマートパスの日" />
    <meta name="description" content="auゲームのポイントキャンペーン。「auスマートパスの日」限定でポイント還元率プラス5％！" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/game-top.css">
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>

            <!-- <div class="top clearfix">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/banner_600_70_01.png" class="img_width-full">
            </div> -->
            <div class="all-games special-games">
                 <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title">
                            <p class="pd_10"><img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/bnr_header_640_180.png" class="img_width-full" alt="毎月2日、22日はauスマートパスの日"></p>
                            <div class="box_period clearfix">
                                <p>期間限定</p>
                                <p>9/2(水)10:00から21:59まで</p>
                            </div>
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/lp_pointcam_main01.png" class="img_width-full" alt="全員に還元率プラス5％　WALLET ポイントで合計15％戻ってくる！">
                            <p class="text_left">通常、対象アプリで課金いただいた金額の10％がWALLET ポイントで還元されるところを、「auスマートパスの日」は、追加で5％分のポイントをさらにプレゼント。ぜひ、この機会にご利用ください。</p>
                        </div>
                        <div class="list mb_10">
                            <div class="headline pb_15">
                                <div class="middle">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_flag.png">
                                </div>
                                <h3>キャンペーン対象アプリ</h3>
                            </div>
                            <!-- grid -->
                            <?php inc("app_list"); ?>
                            <!-- grid -->
                        </div>

                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_thumb.png">
                                </div>
                                <h3>すでにプレイ中の方も安心！</h3>
                            </div>

                            <div class="pd_10"><span>auスマートパスアプリ以外ですでにプレイ中の方も、auスマートパスアプリにデータを引き継いでプレイを再開すれば、今のレベルのままポイント還元を受けることができます。<br>
                             ぜひこの機会に、ご利用ください。</span>
                            </div>
                            <div class="operation-btn-box mb_10"><a href="https://game.auone.jp/app/transfer/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','データ引き継ぎ方法',event);">データ引き継ぎ方法</a>
                            </div>
                        </div>
                    </div>
                </div><!-- container-min lp_201410--><!-- Bg_orange -->
            </div><!-- all-games special-games-->

            <div class="all-games ranking container-min lp_201410">
                <div class="list">
                    <div class="special-gift">
                        <div class="headline">キャンペーン期間</div>
                        <div class="pd_10"><span>2015年9月2日(水)10:00から21:59まで</span></div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">対象</div>
                        <div class="pd_10">
                        <span>対象アプリで課金いただいたすべての方</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">WALLET ポイントの付与</div>
                        <div class="pd_10">
                        <span>WALLET ポイントの付与は、2015年10月上旬ごろを予定しております。</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">au WALLET について</div>
                        <span class="step-text" style="text-align:left;">au WALLET についての詳細は、<a href="https://wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">こちら</a>をご覧ください。</span>
                        <div class="img_center pd_10">
                            <a href="https://wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">
                            <img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text08.png">
                            </a>
                        </div>
                        <div class="btn-box_half pb_15 clearfix">
                            <div class="operation-btn-box"> <a href="https://wallet.auone.jp/contents/sp/guide/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET ご利用ガイド',event);">ご利用ガイド</a></div>
                            <div class="operation-btn-box"> <a href="https://wallet.auone.jp/contents/sp/help/index.html" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET よくある質問',event);">よくある質問</a></div>
                        </div>
                    </div>
                </div>

                <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">注意事項</div>
                        <div class="pd_10">
                            <span>・キャンペーン対象期間外に条件を達成した場合は、通常の10％還元となります。<br>
                            ・本キャンペーンは、予告なく変更または中止する場合がございます。<br>
                            ・メンテナンス等の関係で、アプリのダウンロードができない場合がございます。<br>
                            ・事務局より、本キャンペーンに関わるEメールを送付することがありますのでご了承ください。</span>
                        </div>
                    </div>
                </div>
                <div class="banner-medium banner">
                    <a href="http://st.pass.auone.jp/st/tokuten/pass-day/android.html?medid=pass-day&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','戻りリンク用バナー_スマパスの日',event);">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/bnr_back-link_600_70.png" alt="auスマートパスの日　すんごいお得!!詳しくはこちら">
                    </a>
                </div>
                <?php inc("adRect_android"); ?>
            </div><!--all-games ranking container-min lp_201410-->
        </div><!--contens-body-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>
