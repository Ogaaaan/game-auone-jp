<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>限定ギフトキャンペーン - auゲーム</title>
    <meta name="keywords" content="限定ギフトキャンペーン,au WALLET,ポイントプレゼント,ポイントゲーム,アプリ,スマートパス,スマパス,auスマートパス,限定アイテム" />
    <meta name="description" content="auゲームの限定ギフトキャンペーン。人気ゲームの限定アイテムを無料でプレゼント！" />
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
    <style>
    .all-games {
        margin-bottom: 8px;
    }
    .mb_0 {
        margin-bottom: 0;
    }
    .pt_10 {
        padding-top: 10px;
    }
    .banner_spass {
        max-width: 480px;
        margin: 0 auto;
        text-align: center;
    }
    .download_btn_wrapper {
        background: #fff;
        padding: 10px 10px 15px;
        border-radius: 2px;
        margin: 10px 18px 0;
        box-shadow: 0 1px 3px rgba(0,0,0,0.24);
    }
    .download_btn {
        position: relative;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        margin: 0;
    }
    .download_btn a {
        display: box;
        display: -webkit-box;
        box-align: center;
        -webkit-box-align: center;
        border-radius: 10px;
        background: #16b656;
        padding: 2px 10px;
        box-shadow: 0 6px 0 0 #045e28;
    }
    .download_btn img:first-child {
        display: block;
        width: 50px;
        padding: 5px 0;
    }
    .download_btn p {
        display: block;
        font-size: 15px;
        color: #fff;
        font-weight: bold;
        line-height: 1.2em;
        margin: 0 10px;
    }
    .download_btn img:last-child {
        display: block;
        position:absolute;
        right: 10px;
        top: 24px;
        width: 15px;
        height: 15px;
    }
    .title_img {
        margin: 0;
    }
    .title_txt p {
        text-align: center;
        padding: 0 10px;
        font-size: 14px;
        font-weight: bold;
        line-height: 1.5;
        color: #666;
        margin-top: 16px;
    }
    .txt_gray {
        color: #666;
    }
    .text_caption {
        text-align: left;
        color: #fff;
        padding: 10px 10px 0;
        margin: 0 10px;
        line-height: 1.4;
    }
    .text_caption>.headline {
        margin: 0;
        padding: 8px;
        color: #eb5505;
        font-weight: bold;
        background: #fff;
        border: none;
    }
    .text_caption ul {
        border: 1px solid #fff;
        margin-bottom: 20px;
    }
    .text_caption ul li {
        color: #fff;
        margin: 5px 0 5px 5px;
    }
    .app_medialist {
        background: #fff;
        border-radius: 2px;
        margin: 0 8px;
        padding-top: 10px;
        overflow: hidden;
        box-shadow: 0 1px 3px rgba(0,0,0,0.24);
    }
    .app_medialist p {
        font-size: 14px;
        font-weight: bold;
    }
    .app_medialist>p {
        margin-left: 10px;
    }
    .app_medialist>p img {
        width: 15px;
    }
    .app_medialist ul li {
        border-top: 1px solid #dfdfdf;
    }
    .app_medialist ul li a {
        position: relative;
        display: box;
        display: -webkit-box;
        margin: 10px;
    }
    .app_medialist ul li a>img {
        display: block;
        width: 60px;
        height: 60px;
        margin-right: 10px;
    }
    .app_medialist ul li a>div p {
        display: block;
        font-size: 14px;
        font-weight: bold;
        padding-right: 65px;
    }
    .app_medialist ul li a>div div {
        position: absolute;
        bottom: 10px;
        font-size: 12px;
    }
    .all-games{
        margin-bottom: 0;
    }
    </style>
</head>
<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>

            <!-- <div class="top clearfix">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/banner_600_70_01.png" class="img_width-full">
            </div> -->
            <div class="all-games special-games">
                 <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title pt_10">
                            <!--<p style="padding:0 10px 10px 10px;"><img src="<?php print($PATH['url_game']);?>img/lp/pass_day_bnr_600x70.png" class="img_width-full" alt="毎月2日、22日はauスマートパスの日"></p>-->
                            <div class="box_period clearfix">
                                <p>プレゼント期間</p>
                                <p>12/31(木)16:00まで</p>
                            </div>
                            <p class="title_img"><img src="<?php print($PATH['url_game']);?>img/lp/lp_20150829_main.png" class="img_width-full" alt="欲しかったアイテムを無料で手に入れるチャンス！"></p>
                            <p class="text_caption">スマートパス会員の方全員に、対象ゲームの限定ギフトを無料でプレゼントします。<br>まずは、GooglePlayで『ゲームギフト』アプリをダウンロードしよう！</p>
                        </div>
                        <div class="download_btn_wrapper">
                            <!-- DLボタン-->
                            <div class="download_btn">
                                <a href="market://details?id=jp.gamegift" onclick="trEventBe(this,'auゲーム','限定ギフトCP','DLボタン_ゲームギフト',event);">
                                    <img src="<?php print($PATH['url_game']);?>img/lp/gentei/gift_title.png">
                                    <p>いますぐダウンロード</p>
                                    <img src="//cdn-img.auone.jp/pass/sp/game/img/lp/150416/arrow_gt.png" alt=">">
                                </a>
                            </div>
                            <!-- /DLボタン-->
                        </div>

                        <p class="text_caption">ダウンロードができましたら、欲しいアイテムを選択して、シリアルをゲットしてください。</p>

                        <div class="text_caption">
                            <div class="headline">注意事項</div>
                            <ul>
                                <li>※ゲームギフトは最新版にしてください。</li>
                                <li>※ブラウザは標準ブラウザ推奨。</li>
                                <li>※対象は、Androidユーザのみ。</li>
                            </ul>
                        </div>

                        <div class="app_medialist">
                            <p><img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png"> 対象ゲーム/アイテム一覧</p>
                            <ul>
                                <!-- 白猫プロジェクト -->
                                <li>
                                    <a href="http://gamegift.jp/lp/au/wcat/wiz_01_01.html" onclick="trEventBe(this,'auゲーム','限定ギフトCP','DLボタン_白猫プロジェクト',event);">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000038.png" alt="白猫プロジェクト">
                                        <div>
                                            <p>白猫プロジェクト</p>
                                            <div>
                                                <img src="<?php print($PATH['url_game']);?>img/lp/gentei/wcat_5.jpg" alt="ジュエル" width="20">
                                                ジュエルx25個
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- /白猫プロジェクト -->
                                <!-- 魔法使いと黒猫のウィズ -->
                                <li>
                                    <a href="http://gamegift.jp/lp/au2/wiz_01_01.html" onclick="trEventBe(this,'auゲーム','限定ギフトCP','DLボタン_魔法使いと黒猫のウィズ',event);">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000022.png" alt="魔法使いと黒猫のウィズ">
                                        <div>
                                            <p>魔法使いと黒猫のウィズ</p>
                                            <div>
                                                <img src="<?php print($PATH['url_game']);?>img/lp/gentei/bcat_4971.png" alt="クリスタル" width="20">
                                                クリスタル×5個
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- /魔法使いと黒猫のウィズ -->
                                <!-- バトルガールハイスクール -->
                                <li>
                                    <a href="http://gamegift.jp/lp/au/bgirl/wiz_01_01.html" onclick="trEventBe(this,'auゲーム','限定ギフトCP','DLボタン_バトルガールハイスクール',event);">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000040.png" alt="バトルガールハイスクール">
                                        <div>
                                            <p>バトルガールハイスクール</p>
                                            <div>
                                                <img src="<?php print($PATH['url_game']);?>img/lp/gentei/bat_item.gif" alt="星のかけら" width="20">
                                                星のかけら×25個
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- /バトルガールハイスクール -->
                                <!-- ほしの島のにゃんこ -->
                                <li>
                                    <a href="http://gamegift.jp/lp/au/hoshinyan/wiz_01_01.html" onclick="trEventBe(this,'auゲーム','限定ギフトCP','DLボタン_ほしの島のにゃんこ',event);">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000039.png" alt="ほしの島のにゃんこ">
                                        <div>
                                            <p>ほしの島のにゃんこ</p>
                                            <div>
                                                <img src="<?php print($PATH['url_game']);?>img/lp/gentei/hoshi_item.png" alt="ルビー×100個" width="20">
                                                ルビー×100個
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- /ほしの島のにゃんこ -->
                                <!-- 蒼の三国志 -->
                                <li>
                                    <a href="http://gamegift.jp/lp/au/aonosangokushi/wiz_01_01.html" onclick="trEventBe(this,'auゲーム','限定ギフトCP','DLボタン_蒼の三国志',event);">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000030.png" alt="蒼の三国志">
                                        <div>
                                            <p>蒼の三国志</p>
                                            <div>
                                                <img src="<?php print($PATH['url_game']);?>img/lp/gentei/ao_item.png" alt="龍玉" width="24">
                                                龍玉×25個
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- /蒼の三国志 -->
                                <!-- ポケコロ -->
                                <li>
                                    <a href="http://gamegift.jp/lp/au/pocketcolony/wiz_01_01.html" onclick="trEventBe(this,'auゲーム','限定ギフトCP','DLボタン_ポケコロ',event);">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9189400000001.png" alt="ポケコロ">
                                        <div>
                                            <p>ポケコロ</p>
                                            <div>
                                                <img src="<?php print($PATH['url_game']);?>img/lp/gentei/poke_item.png" alt="アバターガチャ券" height="20">
                                                アバターガチャ券
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- /ポケコロ -->
                                <!-- 新章イケメン大奥◆禁じられた恋 -->
                                <li>
                                    <a href="http://gamegift.jp/lp/au/ikemenooku/wiz_01_01.html" onclick="trEventBe(this,'auゲーム','限定ギフトCP','DLボタン_新章イケメン大奥◆禁じられた恋',event);">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7132500000001.png" alt="新章イケメン大奥◆禁じられた恋">
                                        <div>
                                            <p>新章イケメン大奥◆禁じられた恋</p>
                                            <div>
                                                <img src="<?php print($PATH['url_game']);?>img/lp/gentei/ike_item.png" alt="菜の花の小袖" width="20">
                                                菜の花の小袖一式
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- /新章イケメン大奥◆禁じられた恋 -->
                                <!-- 誓いのキスは突然に -->
                                <li>
                                    <a href="http://gamegift.jp/lp/au/chikainokiss/wiz_01_01.html" onclick="trEventBe(this,'auゲーム','限定ギフトCP','DLボタン_誓いのキスは突然に',event);">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4990700000001.png" alt="誓いのキスは突然に">
                                        <div>
                                            <p>誓いのキスは突然に</p>
                                            <div>
                                                <img src="<?php print($PATH['url_game']);?>img/lp/gentei/tika_item.png" alt="オレンジ/マカロン/ウエーブ" width="20">
                                                オレンジ/マカロン/ウエーブ
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- /誓いのキスは突然に-->
                                <!-- 戦国X（センゴククロス）-->
                                <li>
                                    <a href="http://gamegift.jp/lp/au/sengokux/wiz_01_01.html" onclick="trEventBe(this,'auゲーム','限定ギフトCP','DLボタン_戦国X（センゴククロス）',event);">
                                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9005100000001.png" alt="戦国X（センゴククロス）">
                                        <div>
                                            <p>戦国X（センゴククロス）</p>
                                            <div>
                                                <img src="<?php print($PATH['url_game']);?>img/lp/gentei/x_item.png" alt="極レア★★★★ /織田信長" width="24">
                                                極レア★★★★ /織田信長
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- /戦国X（センゴククロス）-->
                            </ul>
                        </div>

                    </div>
                </div><!-- container-min lp_201410--><!-- Bg_orange -->
            </div><!-- all-games special-games-->

            <div class="all-games ranking container-min lp_201410">
                <!--<div class="banner-medium banner">
                <a href="http://st.pass.auone.jp/st/tokuten/pass-day/android.html?medid=pass-day&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','戻りリンク用バナー_スマパスの日',event);">
                <img src="<?php print($PATH['url_game']);?>img/lp/pass_day_bnr_640x180.png" alt="auスマートパスの日　すんごいお得!!詳しくはこちら">
                </a>
            </div>-->
                <?php inc("adRect_android"); ?>
            </div><!--all-games ranking container-min lp_201410-->
        </div><!--contens-body-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>