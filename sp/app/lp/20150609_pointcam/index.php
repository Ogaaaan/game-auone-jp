<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>WALLET ポイント20％プレゼントキャンペーン（6月） - auゲーム</title>
    <meta name="keywords" content="ポイントキャンペーン,au WALLET,ポイントプレゼント,ポイント還元,ゲーム,アプリ,スマートパス,スマパス,auスマートパス" />
    <meta name="description" content="auゲームのポイントキャンペーン。期間限定で対象アプリへの課金額20％分のau WALLET ポイントをプレゼント！" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>

            <div class="all-games special-games">
                <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/lp_20150609_main.png" class="img_width-full" alt="期間限定 2015年6月9日（火）0:00から2015年6月11日（木）23:59まで!! いつもの2倍 WALLET ポイントプレゼント！ 通常10％還元のところ、3日間限定でいつもの2倍に増量！ 20%還元">
                            <p class="text_left">通常、対象アプリで課金いただいた金額の10％がWALLET ポイントで還元されるところを、キャンペーン期間内に対象アプリで課金した方全員に、いつもの2倍の20％をプレゼント！</br>ぜひ、この機会にご利用ください。</p>
                        </div>
                        <div class="list mb_10">
                            <div class="headline pb_15">
                                <div class="middle">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_flag.png">
                                </div>
                                <h3>キャンペーン対象アプリ</h3>
                            </div>
                            <ul class="apps">
                                <li>
                                    <a href="//pass.auone.jp/app/detail?app_id=5235400000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_サウザンドメモリーズ',event);">
                                        <div class="border">
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_5235400000001.png" alt="サウザンドメモリーズ">
                                            </div>
                                            <div class="app-text">
                                                <h4>サウザンドメモリーズ</h4>
                                                <p class="genre">ロールプレイング</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="//pass.auone.jp/app/detail?app_id=4585600000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_ドラゴンファング',event);">
                                        <div class="border">
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4585600000001.png" alt="ドラゴンファング">
                                            </div>
                                            <div class="app-text">
                                                <h4>ドラゴンファング</h4>
                                                <p class="genre">ロールプレイング</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="//pass.auone.jp/app/detail?app_id=9005100000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_戦国X（センゴククロス）',event);">
                                        <div class="border">
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9005100000001.png" alt="戦国X（センゴククロス）">
                                            </div>
                                            <div class="app-text">
                                                <h4>戦国X（センゴククロス）</h4>
                                                <p class="genre">シミュレーション・ストラテジー</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_thumb.png">
                                </div>
                                <h3>すでにプレイ中の方も安心！</h3>
                            </div>

                            <div class="pd_10"><span>auスマートパスアプリ以外ですでにプレイ中の方も、auスマートパスアプリにデータを引き継いでプレイを再開すれば、今のレベルのままポイント還元を受けることができます。<br>
                             ぜひこの機会に、ご利用ください。</span>
                            </div>
                            <div class="operation-btn-box mb_10"><a href="//game.auone.jp/app/transfer/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','データ引き継ぎ方法',event);">データ引き継ぎ方法</a>
                            </div>
                        </div>
                    </div>
                </div><!-- container-min lp_201410--><!-- Bg_orange -->
            </div><!-- all-games special-games-->

            <div class="all-games ranking container-min lp_201410">
                <div class="list">
                    <div class="special-gift">
                        <div class="headline">プレゼント内容</div>
                        <div class="pd_10"><span>課金金額の20％分のWALLET ポイント</span></div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">キャンペーン期間</div>
                        <div class="pd_10">
                            <span>2015年6月9日（火）0:00から<br>2015年6月11日（木）23:59まで!!</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">対象</div>
                        <div class="pd_10">
                        <span>キャンペーン期間中に対象アプリ内で課金いただいたすべての方</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">WALLET ポイントの付与</div>
                        <div class="pd_10">
                        <span>WALLET ポイントの付与は、2015年7月上旬ごろを予定しております。</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">au WALLET について</div>
                        <span class="step-text" style="text-align:left;">au WALLET についての詳細は、<a href="//wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">こちら</a>をご覧ください。</span>
                        <div class="img_center pd_10">
                            <a href="//wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">
                            <img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text08.png" alt="au WALLET">
                            </a>
                        </div>
                        <div class="btn-box_half pb_15 clearfix">
                            <div class="operation-btn-box"> <a href="//wallet.auone.jp/contents/sp/guide/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET ご利用ガイド',event);">ご利用ガイド</a></div>
                            <div class="operation-btn-box"> <a href="//wallet.auone.jp/contents/sp/help/index.html" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET よくある質問',event);">よくある質問</a></div>
                        </div>
                    </div>
                </div>
                <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">注意事項</div>
                        <div class="pd_10">
                            <span>・キャンペーン対象期間外に条件を達成した場合は、通常の10％還元となります。<br>
                            ・本キャンペーンは、予告なく変更または中止する場合がございます。<br>
                            ・メンテナンス等の関係で、アプリのダウンロードができない場合がございます。<br>
                            ・事務局より、本キャンペーンに関わるEメールを送付することがありますのでご了承ください。</span>
                        </div>
                    </div>
                </div>
                <?php inc("adRect_android"); ?>
            </div><!--all-games ranking container-min lp_201410-->
        </div><!--contens-body-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>