<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>「au SHINJUKU」グッズプレゼントキャンペーン - auゲーム</title>
    <meta name="keywords" content="サンプルキャンペーン,au SHINJUKU,au新宿,白猫プロジェクト,グッズプレゼント,ポイントゲーム,スマパス,auスマートパス" />
    <meta name="description" content="2015年8月21日～23日の3日間、「au SHINJUKU」にご来場いただいた方を対象に白猫プロジェクトのグッズをプレゼント！" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
<style>
#lp_201504-1 .apps img{width:auto;height:auto;float:none;}
#lp_201504-1 .pb_20{padding-bottom:2rem;}
#lp_201504-1 .mb_20{margin-bottom:2.5rem;}
#lp_201504-1 .txt25{font-size:1.8rem;line-height:2.4rem;}
#lp_201504-1 .list_obg{padding:2rem;}
#lp_201504-1 .list_obg p.txtM{color:#fff;font-size:2.2rem;line-height:2.6rem;}
#lp_201504-1 .apps .btn_green{margin-bottom:2rem;}
#lp_201504-1 .apps .btn_green div{width:92%;background:#00643c;border-radius:5px;borx-sizing:border-box;text-align:center;margin:2rem auto 0 auto;padding-bottom:0.4rem;}
#lp_201504-1 .apps .btn_green div p{background:#16b656;border-radius:5px;box-sizing:border-box;font-size:3.2rem;color:#fff;padding:2.6rem 0 2.4rem 0;font-weight:bold;text-align:right;}
#lp_201504-1 .apps .btn_green div p img{padding:0 20px;}
#lp_201504-1 .org_bdr{width:80%;border:1px solid #e85505;/*padding:5px;*/font-size:2.4rem;margin:0 auto;text-align:center;/*font-weight:bold;*/}
#lp_201504-1 .org_bdr span{color:#e85505;font-size:3rem;display:inline!important;}

@media screen and (max-width: 460px) {
#lp_201504-1 .apps .btn_green div p{font-size:2.6rem;padding:0.8em 0 1.0rem 0;}
.imgbox img{width:93%;}
#lp_201504-1 .list_obg p.txtM{color:#fff;font-size:1.8rem;line-height:2.2rem;}
}
@media screen and (max-width: 420px) {
#lp_201504-1 .btn_orange2 div a img{font-size:2.5rem;padding-left:20px;}
#lp_201504-1 .apps .btn_green div p{font-size:2.2rem;}
}
@media screen and (max-width: 360px) {
/*#lp_201504-1 .apps .btn_green div p{font-size:2.0rem;}*/
#lp_201504-1 .apps .btn_green div p img{padding:0 10px}
#lp_201504-1 .txt25{font-size:1.6rem;line-height:2.2rem;}
}
</style>
</head>

<body class="all-app" id="lp_201504-1">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>

            <div class="all-games special-games">
                <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/lp_20150821_main_sironeko.png" class="img_width-full">
                        </div>

                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png">
                                </div>
                                <h3 style="text-indent:5px;">キャンペーン期間</h3>
                            </div>
                            <div class="pd_10"><span>
                                8/21(金)から8/23(日)の各日、下記の時間帯にて開催いたします。<br>
                                ・8/21(金) 12:00～19:00<br>
                                ・8/22(土) 10:00～19:00<br>
                                ・8/23(日) 10:00～19:00<br>
                            </span>
                            </div>
                        </div>
                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png">
                                </div>
                                <h3 style="text-indent:5px;">受け取り条件</h3>
                            </div>
                            <div class="pd_10"><span>
                                キャンペーン期間中、「au SHINJUKU」にご来店された全てのお客様にプレゼントいたします。
                            </span>
                            </div>
                        </div>
                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png">
                                </div>
                                <h3 style="text-indent:5px;">開催場所</h3>
                            </div>
                            <div class="pd_10"><span>「au SHINJUKU」店</span>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3240.371992754929!2d139.70205789999997!3d35.6924625!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5573d9090da77769!2sau+SHINJUKU!5e0!3m2!1sja!2sjp!4v1439367356268" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen class="mt_10"></iframe>
                            </div>
                        </div>

                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png">
                                </div>
                                <h3 style="text-indent:5px;">グッズのご案内</h3>
                            </div>
                            <div class="pd_10">
                                <span class="gift-title">白猫プロジェクトポストカード<br>（2枚1セット）</span>
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/lp_shinjukucam_sironeko_card.png" style="width: 100%; height:auto;">
                                <span>※プレゼントはお一人様1つまでとさせて頂きます。</span>
                            </div>
                        </div>

                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png">
                                </div>
                                <h3 style="text-indent:5px;">グッズの配布状況について</h3>
                            </div>
                            <div class="pd_10"><span>
                                「au SHINJUKU」公式twitterにて、ご確認いただけます。<br>
                                <div class="t-link">
                                    <a href="https://twitter.com/auSHINJUKU">こちらから</a>
                                </div>
                            </span>
                            </div>
                        </div>

                        <div class="list_obg">
                            <p class="txtM">auスマートパス版の白猫プロジェクトは､ジュエル購入ごとに10%のWALLET ポイントが還元されるのでお得！今やってる方もデータ移行すれば､そのまま利用できるよ。</p>
                        </div>
                        <div class="list mb_10 pb_20">
                            <ul class="apps">
                                <li>
                                    <a href="https://pass.auone.jp/app/r?redirect=2&id=4698010000038&rf=augame0906" onclick="trEventBe(this,'auゲーム','新宿イベント_shironeko','ダウンロード',event);">
                                        <div class="btn_green">
                                            <div>
                                                <p><span style="text-align:center;font-size:18px;">auスマートパス会員の方</span>いますぐダウンロード<img src="//cdn-img.auone.jp/pass/sp/game/img/lp/150416/arrow_gt.png" alt=">"></p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <p class="org_bdr">いつでも<span>10％還元</span></p>
                        </div>
                        <div class="list" style="margin-top:10px;">
                            <div class="headline" style="padding:15px 0 15px 12px;">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_thumb.png">
                                </div>
                                <h3 style="margin-top:-3px;text-indent:5px;">すでにプレイ中の方も安心！</h3>
                            </div>
                            <div class="pd_10"><span>auスマートパスアプリ以外ですでにプレイ中の方も、auスマートパスアプリにデータを引き継いでプレイを再開すれば、今のレベルのままポイント還元を受けることができます。<br>ぜひこの機会に、ご利用ください。</span>
                            </div>
                            <div class="operation-btn-box mb_10"><a href="https://game.auone.jp/app/transfer/4698010000038/" class="operation-button" onclick="trEventBe(this,'auゲーム','新宿イベント_shironeko','データ移行方法',event);">データ引き継ぎ方法</a>
                            </div>
                        </div>
                    </div>
                </div><!-- container-min lp_201410--><!-- Bg_orange -->
            </div><!-- all-games special-games-->

            <div class="all-games ranking container-min lp_201410">
                <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">注意事項</div>
                        <div class="pd_10">
                            <span>
                                ・数量に限りがございますため、ご了承の程お願いいたします。
                            </span>
                        </div>
                    </div>
                </div>
                <?php inc("adRect_android"); ?>
            </div><!--all-games ranking container-min lp_201410-->
        </div><!--contens-body-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>
