<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>WALLET ポイントキャンペーン（7月） - auゲーム</title>
    <meta name="keywords" content="ポイントキャンペーン,au WALLET,ポイントプレゼント,ポイントゲーム,アプリ,スマートパス,スマパス,auスマートパス,モンスト,バトルガールハイスクール,千メモ" />
    <meta name="description" content="auゲームのWALLET ポイントキャンペーン。期間内に対象アプリをダウンロードした方には抽選で10名様に10,000ポイントをプレゼント！" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <div class="t-ad--sp"><?php inc("adSP_android"); ?></div>

            <div class="all-games special-games">
                <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title">
                            <div class="box_period clearfix mt_10">
                                <p>期間限定</p>
                                <p>7/27(月)0:00<span>から</span>7/31(金)23:59<span>まで</span></p>
                            </div>
                            <img src="../../../../src/img/lp/lp_20150727_main.png" class="img_width-full">
                            <p class="text_left">期間中に、キャンペーン対象アプリのいずれかを新規ダウンロードしていただいた方に、抽選で10名様にWALLET ポイントを10,000ポイントプレゼント！<br>
                            <span style="font-size:10px;">※auスマートパス会員の方が対象です。</span></p>
                        </div>
                        <div class="list mb_10">
                            <div class="headline pb_15">
                                <div class="middle">
                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_flag.png">
                                </div>
                                <h3>キャンペーン対象アプリ</h3>
                            </div>
                            <ul class="apps">
                                <li>
                                    <a href="https://pass.auone.jp/app/detail?app_id=6002000000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_モンスターストライク',event);">
                                        <div class="border">
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png" alt="モンスターストライク">
                                            </div>
                                            <div class="app-text">
                                                <h4>モンスターストライク</h4>
                                                <p class="genre">アクション</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://pass.auone.jp/app/detail?app_id=4698010000040&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_バトルガールハイスクール',event);">
                                        <div class="border">
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000040.png" alt="バトルガールハイスクール">
                                            </div>
                                            <div class="app-text">
                                                <h4>バトルガールハイスクール</h4>
                                                <p class="genre">ロールプレイング</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://pass.auone.jp/app/detail?app_id=5235400000001&rf=gameportal" onclick="trEventBe(this,'auゲーム','ポイントCP','DLボタン_サウザンドメモリーズ',event);">
                                        <div class="border">
                                            <div class="image">
                                                <img class="app-icon" src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_5235400000001.png" alt="サウザンドメモリーズ">
                                            </div>
                                            <div class="app-text">
                                                <h4>サウザンドメモリーズ</h4>
                                                <p class="genre">ロールプレイング</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_thumb.png">
                                </div>
                                <h3>すでにプレイ中の方も安心！</h3>
                            </div>
                            <div class="pd_10"><span>auスマートパスアプリ以外ですでにプレイ中の方も、auスマートパスアプリにデータを引き継いでプレイを再開すれば、今のレベルのままポイント還元を受けることができます。<br>
                             ぜひこの機会に、ご利用ください。</span>
                            </div>
                            <div class="operation-btn-box mb_10"><a href="//game.auone.jp/app/transfer/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','データ引き継ぎ方法',event);">データ引き継ぎ方法</a>
                            </div>
                        </div>

                    </div>
                </div><!-- container-min lp_201410--><!-- Bg_orange -->
            </div><!-- all-games special-games-->

            <div class="all-games ranking container-min lp_201410">
                <div class="list">
                    <div class="special-gift">
                        <div class="headline">プレゼント内容</div>
                        <div class="pd_10"><span>抽選で10名様にWALLET ポイントを10,000ポイントプレゼント</span></div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">キャンペーン期間</div>
                        <div class="pd_10">
                            <span>2015年7月27日（月）0:00から<br>2015年7月31日（金）23:59まで</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">応募方法</div>
                        <div class="pd_10">
                        <span>期間中に対象アプリを1つ以上ダウンロード頂いた方の中から抽選でWALLET ポイントをプレゼントいたします。</span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">WALLET ポイントの付与</div>
                        <div class="pd_10">
                            <span>対象アプリを1タイトル以上ダウンロードされたお客様を1エントリーとし、キャンペーン終了後に厳正なる抽選のうえ当選者を決定いたします。<br>
                                当選者へのWALLET ポイントの付与は、2015年8月中旬を予定しております。<br>
                                なお、当選者の発表はWALLET ポイントの付与をもって、代えさせていただきます。<br>
                                ※当選確率はダウンロードしたアプリの数に依存いたしません。
                            </span>
                        </div>
                    </div>
                </div>

                <div class="list">
                    <div class="special-gift">
                        <div class="headline">au WALLET について</div>
                        <span class="step-text" style="text-align:left;">au WALLET についての詳細は、<a href="//wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">こちら</a>をご覧ください。</span>
                        <div class="img_center pd_10">
                            <a href="//wallet.auone.jp/" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET',event);">
                            <img class="img_width-full" src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp_201410/img_text08.png" alt="au WALLET">
                            </a>
                        </div>
                        <div class="btn-box_half pb_15 clearfix">
                            <div class="operation-btn-box"> <a href="//wallet.auone.jp/contents/sp/guide/" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET ご利用ガイド',event);">ご利用ガイド</a></div>
                            <div class="operation-btn-box"> <a href="//wallet.auone.jp/contents/sp/help/index.html" class="operation-button" onclick="trEventBe(this,'auゲーム','ポイントCP','au WALLET よくある質問',event);">よくある質問</a></div>
                        </div>
                    </div>
                </div>
                <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">注意事項</div>
                        <div class="pd_10">
                            <span>
                                ・対象アプリを1タイトル以上ダウンロードされたお客様を1エントリーとして抽選を行います。当選確率はダウンロードしたアプリの数に依存いたしません。<br>
                                ・キャンペーン対象期間外に条件を達成した場合は抽選の対象外となります。<br>
                                ・本キャンペーンは、予告なく変更または中止する場合がございます。<br>
                                ・メンテナンス等の関係で、アプリのダウンロードができない場合がございます。<br>
                                ・事務局より、本キャンペーンに関わるEメールを送付することがありますのでご了承ください。
                            </span>
                        </div>
                    </div>
                </div>
                <?php inc("adRect_android"); ?>
            </div><!--all-games ranking container-min lp_201410-->
        </div><!--contens-body-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>
