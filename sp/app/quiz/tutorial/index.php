<?php include("../../inc/set_quiz.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
<?php inc("quiz_head");?>
<link rel="stylesheet" href="<?php echo $PATH['url_game'];?>css/slick.css">
<link rel="stylesheet" href="<?php echo $PATH['url_game'];?>css/slick-theme.css">
</head>
<body class="index gq-tutorial">
    <!-- js-t-wrapper -->
    <div class="js-t-wrapper">

        <!-- header -->
        <?php inc("header");?>
        <!-- /header -->

        <!-- ad -->
        <div class="t-ad--sp">
        <?php inc("adSP_android");?>
        </div>
        <!-- /ad -->

        <!-- contens -->
        <div class="gq-contens">

            <!-- main contents (with bg)-->
            <div class="gq-contents-body gq-contents-tutorial">

                <!-- main body -->
                <section class="gq-tutorial-main">

                    <div class="gq-tutorial__carousel">
                        <div class="gq-tutorial__carousel-slide gq-tutorial__carousel--01">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/tutorial/01_base.png" width="300" alt="" class="gq-tutorial__carousel--back">
                        </div>
                        <div class="gq-tutorial__carousel-slide gq-tutorial__carousel--02">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/tutorial/02_base.png" width="300" alt="" class="gq-tutorial__carousel--back">
                        </div>
                        <div class="gq-tutorial__carousel-slide gq-tutorial__carousel--03">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/tutorial/03_base.png" width="300" alt="" class="gq-tutorial__carousel--back">
                        </div>
                        <div class="gq-tutorial__carousel-slide gq-tutorial__carousel--04">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/tutorial/04_base.png" width="300" alt="" class="gq-tutorial__carousel--back">
                        </div>
                        <div class="gq-tutorial__carousel-slide gq-tutorial__carousel--05">
                            <img src="<?php echo $PATH['url_game'];?>img/quiz/tutorial/05_base.png" width="300" alt="" class="gq-tutorial__carousel--back">
                        </div>
                    </div>
                    <div class="gq-tutorial__carousel-arrow--next" style="display: none;">
                        <img src="<?php echo $PATH['url_game'];?>img/quiz/tutorial/btn_next.png" alt="次へ" width="50">
                    </div>

                    <div class="gq-btn__challenge-prod3">

                        <a href="/quiz/" class="gq-btn__challenge-item" title="このクイズに挑戦!" onclick="trEventBe(this,'auゲーム','クイズチュートリアル','このクイズに挑戦',event);">
                            <div class="gq-btn__challenge-wrapper">
                                <div class="gq-btn__challenge-left"></div>
                                <div class="gq-btn__challenge-center"></div>
                                <div class="gq-btn__challenge-right"></div>
                            </div>
                            <img class="gq-btn__challenge-text" src="<?php echo $PATH['url_game'];?>img/quiz/btn_challenge03.png" width="94">
                        </a>

                    </div>

                </section>
                <!-- /main body -->
            </div>
            <!-- /main contents (with bg)-->

        </div>
        <!-- /contens -->

        <!-- ad -->
        <div class="gq-ad--rect">
            <?php inc("adRect_android");?>
        </div>
        <!-- /ad -->

        <!-- footer -->
        <?php inc("footer");?>
        <!-- /footer -->

    </div>
    <!-- /js-t-wrapper -->

    <?php inc("quiz_script");?>
    <script src="<?php print($PATH['url_game']);?>js/slick.min.js"></script>
    <script src="<?php print($PATH['url_game']);?>js/jquery.a3d.min.js"></script>
    <script src="<?php print($PATH['url_game']);?>js/game_quiz_anim_tuto.min.js"></script>
    <?php inc("gtm");?>

</body>
</html>
