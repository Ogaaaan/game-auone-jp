<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
<meta charset="UTF-8">
<title>データ引き継ぎ方法 - auゲーム</title>
<meta name="keywords" content="データ引き継ぎ方法,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
<meta name="description" content="ゲームアプリのデータ引き継ぎ方法ページです。すでにお使いのゲームでも、レベルはそのままでお得に遊べます。" />
<?php inc("head"); ?>
<style>
.game_t-scroll-tabs__cover{
    padding: 20px 0 0;
    border-bottom: solid 2px #EB5505;
}
.game_t-scroll-tabs__cover ul{
    display: table;
    width: 100%;
    margin: auto;
}
.game_t-scroll-tabs__cover ul li{
    display: table-cell;
    width: 50%;
}
.game_t-scroll-tabs__cover ul li a{
    background: #f3f3f3;
    display: block;
    padding: 5px 2px;
    text-align: center;
    border-radius: 3px 3px 0 0;
    margin: 0px 7px;
    color:#333;
    font-size: 13px;
}
.game_t-scroll-tabs__nav.t-scroll-tabs__nav--active{
    background: #eb5505;
    color:#fff;
}
</style>

</head>

  <body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">
<?php inc("header"); ?>
<?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games">

      <!-- operation  -->
      <div class="ranking operation">
        <div class="operation-top-title">
          データ引継ぎ方法
        </div>
        <div class="list">
          <div class="headline">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000038.png">
            <div class="title">
              <h3>白猫プロジェクト</h3>
              <p>ロールプレイング</p>
            </div>
          </div><!-- headline -->


          <!-- 特典 -->
          <div class="special-gift">
            <div class="headline">
              <!-- 特典枠タイトル -->
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png">
              特典
            </div>
            <p>
              <!-- 特典枠文言 -->
              ジュエル100円購入ごとに10ポイント
            </p>
          </div>

          <!-- リンクボタン -->
          <div class="operation-btn-box">
            <a href="http://pass.auone.jp/app/detail?app_id=4698010000038&rf=gameportal" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','DLボタン_白猫プロジェクト',event);">アプリダウンロードページを見る</a>
          </div>

        <!-- game-tabs -->
            <div class="game_t-scroll-tabs__cover">
                <ul class="js-t-scroll-tabs__nav">
                    <li><a class="game_t-scroll-tabs__nav t-scroll-tabs__nav--active" href="#">Googleアカウントで<br>引継</a></li>
                    <li><a class="game_t-scroll-tabs__nav" href="#">メールアドレスで<br>引継</a></li>
                </ul>
            </div>

            <div class="js-t-scroll-tabs__items t-scroll-tabs__items" style="display:block;">
                                  <!-- ステップアイコン -->
                  <div class="step-icon-box">
                    <div class="step-icon">
                      <p>ステップ１</p>
                      <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
                      <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
                    </div>
                  </div>

                  <!-- ステップタイトル -->
                  <div class="step-title">
                    【ダウンロード】ボタンを押下して、au MarketからアプリをDLする。
                  </div>

                  <!-- 画像 -->
                  <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/1.png">
                  </div>

                  <!-- ステップアイコン -->
                  <div class="step-icon-box">
                    <div class="step-icon">
                      <p>ステップ２</p>
                      <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
                      <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
                    </div>
                  </div>

                  <!-- ステップタイトル -->
                  <div class="step-title">
                    ホーム画面にアプリが２つあることを確認する。
                  </div>

                  <!-- 画像 -->
                  <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/2.png">
                  </div>

                  <!-- 機種変更補足文 -->
                  <div class="step-bg-gray">
                    <p>
                      <span class="title">機種変更の場合</span><br>
                      ホーム画面には、一つのアプリのみが出現します。
                      </p>
                  </div>


                  <!-- ステップアイコン -->
                  <div class="step-icon-box">
                    <div class="step-icon">
                      <p>ステップ３</p>
                      <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
                      <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
                    </div>
                  </div>

                  <!-- ステップタイトル -->
                  <ol class="list-num">
                    <li>
                      白猫プロジェクトを起動してホーム画面を表示します。<br>画面上部の「MENU」をタップし、「オプション」を選択してください。
                      <div class="step-img">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_01.png">
                      </div>
                    </li>
                    <li>
                      「アカウント設定」を選択する。
                      <div class="step-img">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_02.png">
                      </div>
                    </li>
                    <li>
                      「googleアカウントで登録」を選択してください。
                      <div class="step-img">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_03_google.png">
                      </div>
                    </li>
                    <li>
                      登録したいアカウントを選択してください。<br><br>
                      6文字以上8文字以下のパスワードを設定してください。<br>
                      <span class="red">※設定したパスワードは忘れないようにメモをしておいてください。</span><br><br>
                      「OK」ボタンを押下してください。<br>

                      <div class="step-img">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_04_google.png">
                      </div>
                    </li>
                    <li>
                      登録成功の画面が表示されれば、バックアップの登録は完了です。
                      <div class="step-img">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_05.png">
                      </div>
                    </li>
                  </ol>
                <!-- ステップアイコン -->
                  <div class="step-icon-box">
                    <div class="step-icon">
                      <p>ステップ４</p>
                      <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
                      <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
                    </div>
                  </div>

                  <!-- ステップタイトル -->
                  <ol class="list-num">
                    <li>
                      ホーム画面より、auロゴのついたアイコンを押下し、「機種変更はこちら」をタップする。
                      <div class="step-img">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_06.png">
                      </div>
                    </li>
                    <li>
                      「googleアカウントでログイン」を選択してください。
                      <div class="step-img">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_07_google.png">
                      </div>
                    </li>
                    <li>
                      STEP3-4 で設定したアカウントを設定し、パスワードを入力してください。<br>最後に「OK」ボタンをタップしてください。
                      <div class="step-img">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_08_google.png">
                      </div>
                    </li>
                    <li>
                      ログイン成功画面で、引継ぎ完了となります。
                      <div class="step-img">
                        <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_09.png">
                      </div>
                    </li>
                  </ol>

                  <div class="step-icon-box">
                    <div class="step-icon">
                      <p>ステップ５</p>
                      <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
                      <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
                    </div>
                  </div>

                  <ol class="list-num">
                    <li>
                      「auロゴ」のついたアプリを起動してデータ移行されていることを確認
                    </li>
                    <li>
                      旧アプリをホーム画面から削除する
                    </li>
                    <li>
                      <span class="red">データ引継ぎ完了！</span>
                    </li>
                  </ol>
            </div>
            <div class="js-t-scroll-tabs__items t-scroll-tabs__items">

              <!-- ステップアイコン -->
              <div class="step-icon-box">
                <div class="step-icon">
                  <p>ステップ１</p>
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
                  <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
                </div>
              </div>

              <!-- ステップタイトル -->
              <div class="step-title">
                【ダウンロード】ボタンを押下して、au MarketからアプリをDLする。
              </div>

              <!-- 画像 -->
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/1.png">
              </div>

              <!-- ステップアイコン -->
              <div class="step-icon-box">
                <div class="step-icon">
                  <p>ステップ２</p>
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
                  <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
                </div>
              </div>

              <!-- ステップタイトル -->
              <div class="step-title">
                ホーム画面にアプリが２つあることを確認する。
              </div>

              <!-- 画像 -->
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/2.png">
              </div>

              <!-- 機種変更補足文 -->
              <div class="step-bg-gray">
                <p>
                  <span class="title">機種変更の場合</span><br>
                  ホーム画面には、一つのアプリのみが出現します。
                  </p>
              </div>

              <!-- ステップアイコン -->
              <div class="step-icon-box">
                <div class="step-icon">
                  <p>ステップ３</p>
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
                  <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
                </div>
              </div>

              <!-- ステップタイトル -->
              <ol class="list-num">
                <li>
                  白猫プロジェクトを起動してホーム画面を表示します。<br>画面上部の「MENU」をタップし、「オプション」を選択してください。
                  <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_01.png">
                  </div>
                </li>
                <li>
                  「アカウント設定」を選択する。
                  <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_02.png">
                  </div>
                </li>
                <li>
                  「メールアドレスで登録」を選択してください。
                  <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_03.png">
                  </div>
                </li>
                <li>
                  登録したいメールアドレスをフィールドに入力してください。<br><br>
                  6文字以上8文字以下のパスワードを設定してください。<br>
                  <span class="red">※設定したパスワードは忘れないようにメモをしておいてください。</span><br><br>
                  秘密の質問を選び、質問の回答を登録してください。<br>
                  <span class="red">※設定した質問と回答は忘れないようにメモをしておいてください。</span><br><br>
                  「OK」ボタンを押下してください。
                  <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_04.png">
                  </div>
                </li>
                <li>
                  登録成功の画面が表示されれば、バックアップの登録は完了です。
                  <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_05.png">
                  </div>
                </li>
              </ol>
            <!-- ステップアイコン -->
              <div class="step-icon-box">
                <div class="step-icon">
                  <p>ステップ４</p>
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
                  <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
                </div>
              </div>

              <!-- ステップタイトル -->
              <ol class="list-num">
                <li>
                  ホーム画面より、auロゴのついたアイコンを押下し、「機種変更はこちら」をタップする。
                  <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_06.png">
                  </div>
                </li>
                <li>
                  「メールアドレスでログイン」を選択してください。
                  <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_07.png">
                  </div>
                </li>
                <li>
                  STEP3-4 で設定したメールアドレスとパスワードを入力し、「OK」ボタンをタップしてください。
                  <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_08.png">
                  </div>
                </li>
                <li>
                  ログイン成功画面で、引継ぎ完了となります。
                  <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000038_09.png">
                  </div>
                </li>
              </ol>

              <div class="step-icon-box">
                <div class="step-icon">
                  <p>ステップ５</p>
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
                  <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
                </div>
              </div>

              <ol class="list-num">
                <li>
                  「auロゴ」のついたアプリを起動してデータ移行されていることを確認
                </li>
                <li>
                  旧アプリをホーム画面から削除する
                </li>
                <li>
                  <span class="red">データ引継ぎ完了！</span>
                </li>
              </ol>
            </div>
        <!-- /game-tabs -->
          <div class="special-gift">
            <div class="headline">
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_gift.png">
              ギフト
            </div>
            <p>
              限定アイテム付！
            </p>
          </div>

          <div class="operation-btn-box">
            <a href="http://gamegift.jp/lp/au/wcat/" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','ギフトボタン_白猫プロジェクト',event);">ギフトをゲットする</a>
          </div>

          </div><!-- list -->
          <div class="step-bg-gray">
            <div>
                ※当ページ（データ引き継ぐには）は、KDDIが独自に調べ作成したもので、アプリ会社とは関係ございません。アプリのバージョンにより、正常に移行できない可能性がございますので、最新のバージョンでお試しください。
            </div>
          </div>
         </div><!-- manual -->
      <?php inc("adRect_android"); ?>
   </div>

<?php inc("footer"); ?>

</div><!--contens-body-->
</div>

<?php inc("script"); ?>
<script src="<?php print($PATH['url_tentomushi']);?>js/t.js"></script>
<?php inc("gtm"); ?>


  </body>
</html>