<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
<meta charset="UTF-8">

    <title>データ引き継ぎ方法 - auゲーム</title>
    <meta name="keywords" content="データ引き継ぎ方法,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />

    <meta name="description" content="ゲームアプリのデータ引き継ぎ方法ページです。すでにお使いのゲームでも、レベルはそのままでお得に遊べます。" />

    <?php inc("head"); ?>
  </head>

  <body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

    <!--//////////////////////
      nav
    //////////////////////  -->
        <?php inc("header"); ?>
        <?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games" id="#top">

      <!-- operation  -->
      <div class="ranking operation">
        <div class="operation-top-title">
          データ引継ぎ方法
        </div>
        <div><p class="red" style="margin:0 10px 20px;">※2015年8月31日をもちまして、本アプリの提供は終了いたしました。</p></div>
        <div class="list">
          <div class="headline">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_1597710000012.png">
            <div class="title">
              <h3>バーコードフットボーラー</h3>
              <p>スポーツ</p>
            </div>
          </div><!-- headline -->


          <!-- 特典 -->
          <div class="special-gift">
            <div class="headline">
              <!-- 特典枠タイトル -->
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png">
              特典
            </div>
            <p>
              <!-- 特典枠文言 -->
              ポイント100円購入ごとに10ポイント
            </p>
          </div>

          <!-- リンクボタン -->
          <div class="operation-btn-box">
            <a href="http://pass.auone.jp/app/detail?app_id=1597710000012&rf=gameportal" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','DLボタン_バーコードフットボーラー',event);">アプリダウンロードページを見る</a>
          </div>

          <ul class="list-text">
            <li>
              「auロゴ」のついていないアプリをアンインストールした後、auMarketからアプリをダウンロードしてください。
            自動的にデータが引き継がれている状態でアプリをご利用いただけます。
            </li>
          </ul>

          <div class="special-gift">
            <div class="headline">
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_gift.png">
              ギフト
            </div>
            <p>
              限定アイテム付！
            </p>
          </div>


          <div class="operation-btn-box">
            <a href="http://gamegift.jp/lp/au/barcodefootballer2/" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','ギフトボタン_バーコードフットボーラー',event);">ギフトをゲットする</a>
          </div>
        </div><!-- list -->
          <div class="step-bg-gray">
            <div>
                ※当ページ（データ引き継ぐには）は、KDDIが独自に調べ作成したもので、アプリ会社とは関係ございません。アプリのバージョンにより、正常に移行できない可能性がございますので、最新のバージョンでお試しください。
            </div>
          </div>
      </div><!-- manual -->
      <?php inc("adRect_android"); ?>

    </div>
    <?php inc("footer"); ?>
</div><!--contens-body-->
</div>
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

  </body>
</html>