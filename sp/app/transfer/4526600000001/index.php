<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
<meta charset="UTF-8">
<title>データ引き継ぎ方法 - auゲーム</title>
<meta name="keywords" content="データ引き継ぎ方法,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
<meta name="description" content="ゲームアプリのデータ引き継ぎ方法ページです。すでにお使いのゲームでも、レベルはそのままでお得に遊べます。" />


<?php inc("head"); ?>
</head>

<body class="all-app">
<div class="js-t-wrapper">
<div class="contens-body">

<?php inc("header"); ?>
<?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games" id="#top">

      <!-- operation  -->
      <div class="ranking operation">
        <div class="operation-top-title">
          データ引継ぎ方法
        </div>
        <div class="list">
          <div class="headline">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4526600000001.png">
            <div class="title">
              <h3>ぼくらの甲子園！ポケット for auスマートパス</h3>
              <p>スポーツ</p>
            </div>
          </div><!-- headline -->


          <!-- 特典 -->
          <div class="special-gift">
            <div class="headline">
              <!-- 特典枠タイトル -->
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png">
              特典
            </div>
            <p>
              <!-- 特典枠文言 -->
              アイテム100円購入ごとに10ポイント
            </p>
          </div>

          <!-- リンクボタン -->
          <div class="operation-btn-box">
            <a href="http://pass.auone.jp/app/detail?app_id=4526600000001&rf=gameportal" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','DLボタン_ぼくらの甲子園！ポケット for auスマートパス',event);"
>アプリダウンロードページを見る</a>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ１</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            【ダウンロード】ボタンを押下して、au MarketからアプリをDLする。
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/1.png">
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ２</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            ホーム画面にアプリが２つあることを確認する。
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/2.png">
          </div>

          <!-- 機種変更補足文 -->
          <div class="step-bg-gray">
            <p>
              <span class="title">機種変更の場合</span><br>
              ホーム画面には、一つのアプリのみが出現します。
              </p>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ３</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>
          <ol class="list-num">
            <li>
              「auロゴ」のないアプリを起動後、「街」＞「設定」（画面内右上のアイコン） ＞ 「その他」内「データ引継ぎ」に進む
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4526600000001_01.png">
              </div>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4526600000001_02.png">
              </div>            </li>
            <li>
              データ引継ぎ注意事項の内容を確認し、「同意する」ボタンを押し、データ引継ぎ情報を発行
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4526600000001_03.png">
              </div>
            </li>
            <li>
              発行された「プレイヤーID」「引継ぎコード」の2つをメモしておく。<br><br>
※「同意する」ボタンを押す度に、新しい「引継ぎコード」が発行されます。その場合、古い引継ぎコード」は使用できなくなります。<br><br>
※発行画面の「メールで送信」ボタンを押すと、メーラーが起動し、じぶんのメールアドレスにデータ引継ぎ情報を送ることができます。<br><br>
※「引き継ぎコード」の有効期間は、発行から3ヶ月間です。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4526600000001_04.png">
              </div>
            </li>
          </ol>

          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ４</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
             ステップ1でダウンロードしたアプリを起動し、タイトル画面右下にある、「データ引継ぎ」ボタンを押す。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4526600000001_05.png">
              </div>
            </li>
            <li>
              IDの入力画面が表示されるので、古い端末で発行した「プレイヤーID」「引継ぎコード」の2つを入力して「引き継ぐ」ボタンを押す。<br>
アプリ再起動後、データ引継ぎが完了していることを必ず確認してください。<br>
<br>
※1度使用した「引き継ぎコード」は、使用できなくなります。<br>
※データ引き継ぎ後、30日間、新たな「引き継ぎコード」を発行できなくなります。<br>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4526600000001_06.png">
              </div>
            </li>
          </ol>


          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ５</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              「auロゴ」のついたアプリを起動してデータ移行されていることを確認
            </li>
            <li>
              旧アプリをホーム画面から削除する
            </li>
            <li>
              <span class="red">データ引継ぎ完了！</span>
            </li>
          </ol>

          </div><!-- list -->
          <div class="step-bg-gray">
            <div>
                ※当ページ（データ引き継ぐには）は、KDDIが独自に調べ作成したもので、アプリ会社とは関係ございません。アプリのバージョンにより、正常に移行できない可能性がございますので、最新のバージョンでお試しください。
            </div>
          </div>
        </div><!-- manual -->
      <?php inc("adRect_android"); ?>
    </div>

<?php inc("footer"); ?>

</div><!--contens-body-->
</div>

<?php inc("script"); ?>
<?php inc("gtm"); ?>

</body>
</html>