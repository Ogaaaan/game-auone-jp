<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
<meta charset="UTF-8">
<title>データ引き継ぎ方法 - auゲーム</title>
<meta name="keywords" content="データ引き継ぎ方法,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
<meta name="description" content="ゲームアプリのデータ引き継ぎ方法ページです。すでにお使いのゲームでも、レベルはそのままでお得に遊べます。" />
<?php inc("head"); ?>

</head>

<body class="all-app">
<div class="js-t-wrapper">
<div class="contens-body">

<?php inc("header"); ?>
<?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games" id="#top">

      <!-- operation  -->
      <div class="ranking operation">
        <div class="operation-top-title">
          データ引継ぎ方法
        </div>
        <div class="list">
          <div class="headline">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png">
            <div class="title">
              <h3>モンスターストライク</h3>
              <p>アクション</p>
            </div>
          </div><!-- headline -->


          <!-- 特典 -->
          <div class="special-gift">
            <div class="headline">
              <!-- 特典枠タイトル -->
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png">
              特典
            </div>
            <p>
              <!-- 特典枠文言 -->
              オーブ100円購入ごとに10ポイント
            </p>
          </div>

          <!-- リンクボタン -->
          <div class="operation-btn-box">
            <a href="http://pass.auone.jp/app/detail?app_id=6002000000001&rf=gameportal" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','DLボタン_モンスターストライク',event);"
>アプリダウンロードページを見る</a>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ１</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            【ダウンロード】ボタンを押下して、au MarketからアプリをDLする。
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/1.png">
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ２</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            ホーム画面にアプリが２つあることを確認する。
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/2.png">
          </div>

          <!-- 機種変更補足文 -->
          <div class="step-bg-gray">
            <p>
              <span class="title">機種変更の場合</span><br>
              ホーム画面には、一つのアプリのみが出現します。
              </p>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ３</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- 説明文 -->

          <ol class="list-num">
            <li>
              「Google+」の登録が必要です 。<br>
              iOS をご利用の場合には、アプリ（Google+）をスマートフォンにインストールし、　ご登録をいただくとお手続きが簡単です。<br>
              <p class="step-text-small">※注意※<br>
              ・Google+ にご登録されますと、プロフィール情報がインターネットに公開されますので、あらかじめご了承ください。 <br>
              ・電波状況が良い場所で操作をしてください。<br>
              ・車や電車など、高速で移動している場合には、バックアップしたデータが壊れてしまう可能性がございますので、お止めください。</p>
            </li>
            <li>
              モンストを起動してホーム画面を表示します。<br>
              画面下部の「その他」をタップし、「ヘルプ/その他」ページを表示します。<br>
              「データのバックアップ」ボタンをタップし、プレイデータのバックアップをするために「バックアップ」ボタンをタップしてください。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/6002000000001_01.png">
              </div>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/6002000000001_02.png">
              </div>
            </li>
            <li>
              プレイデータをバックアップする Google アカウントにログインして、モンスターストライクからのリクエストの確認画面において、「OK」ボタンをタップしてください。　<br>
              <p class="step-text-small">※注意 <br>
              ・自分で管理している Google アカウントにログインしてください。 <br>
              ・他人の Google アカウントでバックアップを行うと、プレイデータが引き継げない場合や乗っ取られる危険がありますので、お止めください。 </p>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/6002000000001_03.png">
              </div>
            </li>
            <li>
              バックアップの完了画面に、プレイデータを引き継ぐ際に必要な情報が表示されます。<br>
              控えた情報は、自分以外には知られないように大切に保管してください。<br>
              <span class="red">※Google アカウントのログイン情報を、自分以外の方に教えないでください。</span>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/6002000000001_04.png">
              </div>
            </li>
          </ol>

          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ４</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              STEP1でダウンロードしたモンスターストライクを起動し、ニックネームの入力画面を表示し、「プレイデータの引き継ぎ」ボタンをタップします。<br>
              <span class="red">※「au」ロゴの入っている方を起動してください！<br>
              ※誤って新規でプレイではじめてしまった場合には、アプリを再インストールしてからやり直してください。 </span>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/6002000000001_05.png">
              </div>
              「プレイデータの引継ぎ」⇒「はい」を選択
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/6002000000001_06.png">
              </div>
            </li>
            <li>
                プレイデータをバックアップした Google アカウントにログインします。<br>
                モンスターストライクからのリクエストの確認画面において、「OK」ボタンをタップしてください。<br>
                ※プレイデータをバックアップしているアカウント以外にログインをされても、引き継ぐ事はできません。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/6002000000001_07.png">
              </div>
            </li>
            <li>
                STEP3-4でメモしたプレイデータの「あなたのID」を入力し OK をタップしてくさい。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/6002000000001_08.png">
              </div>
            </li>
            <li>
                表示されたIDとニックネームを確認し、 問題なければ「はい」⇒「OK」を押下
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/6002000000001_09.png">
              </div>
            </li>
            <li>
                バックアップしてあるプレイデータが表示されますので、OK をタップしてください。<br>
                データの更新が完了すると、以前の続きからご利用ができます。
            </li>
          </ol>


          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ５</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              「auロゴ」のついたアプリを起動してデータ移行されていることを確認
            </li>
            <li>
              旧アプリをホーム画面から削除する
            </li>
            <li>
              <span class="red">データ引継ぎ完了！</span>
            </li>
          </ol>

          <!-- <div class="special-gift">
            <div class="headline">
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_gift.png">
              ギフト
            </div>
            <p>

            </p>
          </div>


          <div class="operation-btn-box">
            <a href="http://pass.auone.jp/app/detail?app_id=6002000000001&rf=gameportal" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','ギフトボタン_',event);">ギフトをゲットする</a>
          </div> -->

		      </div><!-- list -->
          <div class="step-bg-gray">
            <div>
                ※当ページ（データ引き継ぐには）は、KDDIが独自に調べ作成したもので、アプリ会社とは関係ございません。アプリのバージョンにより、正常に移行できない可能性がございますので、最新のバージョンでお試しください。
            </div>
          </div>
	      </div><!-- manual -->
      <?php inc("adRect_android"); ?>
    </div>

<?php inc("footer"); ?>

</div><!--contens-body-->
</div>

<?php inc("script"); ?>
<?php inc("gtm"); ?>

</body>
</html>