<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
<meta charset="UTF-8">
    <title>データ引き継ぎ方法 - auゲーム</title>
    <meta name="keywords" content="データ引き継ぎ方法,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
    <meta name="description" content="ゲームアプリのデータ引き継ぎ方法ページです。すでにお使いのゲームでも、レベルはそのままでお得に遊べます。" />

    <?php inc("head"); ?>
  </head>

  <body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

    <!--//////////////////////
      nav
    //////////////////////  -->
        <?php inc("header"); ?>
        <?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games" id="#top">

      <!-- operation  -->
      <div class="ranking operation">
        <div class="operation-top-title">
          データ引継ぎ方法
        </div>
        <div class="list">
          <div class="headline">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000022.png">
            <div class="title">
              <h3>魔法使いと黒猫のウィズ</h3>
              <p>ロールプレイング</p>
            </div>
          </div><!-- headline -->


          <!-- 特典 -->
          <div class="special-gift">
            <div class="headline">
              <!-- 特典枠タイトル -->
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png">
              特典
            </div>
            <p>
              <!-- 特典枠文言 -->
              クリスタル100円購入ごとに10ポイント
            </p>
          </div>

          <!-- リンクボタン -->
          <div class="operation-btn-box">
            <a href="http://pass.auone.jp/app/detail?app_id=4698010000022&rf=gameportal" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','DLボタン_魔法使いと黒猫のウィズ',event);"
>アプリダウンロードページを見る</a>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ１</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              [GooglePlay版]のアプリを起動してmenuをタップします。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000022_01.png">
              </div>
            </li>
            <li>
              「オプション」ボタンをタップします。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000022_02.png">
              </div>
            </li>
            <li>
              「アカウント設定」ボタンをタップします。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000022_03.png">
              </div>
            </li>
            <li>
              設定するアカウントを選択してボタンをタップします。<br>
              （画像はＧoogleアカウントで登録した場合のイメージです。）
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000022_04.png">
              </div>
            </li>
            <li>
              登録するアカウントでよろしければ「ＯＫ」ボタンをタップし、アカウント登録完了です。<br>
              （画像はGoogleアカウントで登録した場合のイメージです。）
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000022_05.png">
              </div>
            </li>
          </ol>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ２</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              [GooglePlay版]のアプリをアンインストールします。<br>
              <span class="red">※STEP1の手続き（アドバンストユーザ登録）をせずにアンインストールをしてしまいますと、データが引き継ぐことができません。</span>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000022_10.png">
              </div>
            </li>
          </ol>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ３</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <ol class="list-num">
            <li>
              【ダウンロード】ボタンを押下して、au MarketからアプリをDLする。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/1.png">
              </div>
            </li>
          </ol>

        <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ４</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              STEP3でダウンロードしたアプリを起動し、「機種変更の方はこちら」をタップします。<br>
              <span class="red">※「au」ロゴの入っている方を起動してください！</span>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000022_06.png">
              </div>
            </li>
            <li>
              STEP1で登録したアカウントの「ボタン」をタップします。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000022_07.png">
              </div>
            </li>
            <li>
              該当項目（登録したメールアドレス、パスワード）を入力して「ＯＫ」ボタンをタップします。<br>
              （画像はクイズアカウントでログインする場合のイメージです。）
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000022_08.png">
              </div>
            </li>
            <li>
              「ＯＫ」ボタンをタップします。<br>
              以上でアカウント移行は完了です。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000022_09.png">
              </div>
            </li>
          </ol>


          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ５</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              「auロゴ」のついたアプリを起動してデータ移行されていることを確認
            </li>
            <li>
              旧アプリをホーム画面から削除する
            </li>
            <li>
              <span class="red">データ引継ぎ完了！</span>
            </li>
          </ol>

          <div class="special-gift">
            <div class="headline">
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_gift.png">
              ギフト
            </div>
            <p>
              クリスタル５個プレゼント！
            </p>
          </div>


          <div class="operation-btn-box">
            <a href="http://gamegift.jp/lp/au2/" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','ギフトボタン_魔法使いと黒猫のウィズ',event);">ギフトをゲットする</a>
          </div>

        </div><!-- list -->
          <div class="step-bg-gray">
            <div>
                ※当ページ（データ引き継ぐには）は、KDDIが独自に調べ作成したもので、アプリ会社とは関係ございません。アプリのバージョンにより、正常に移行できない可能性がございますので、最新のバージョンでお試しください。
            </div>
          </div>
      </div><!-- manual -->
      <?php inc("adRect_android"); ?>
    </div>

        <?php inc("footer"); ?>
</div><!--contens-body-->
</div>

    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

  </body>
</html>