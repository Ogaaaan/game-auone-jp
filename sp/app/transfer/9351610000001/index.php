<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
<meta charset="UTF-8">
    <title>データ引き継ぎ方法 - auゲーム</title>
    <meta name="keywords" content="データ引き継ぎ方法,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
    <meta name="description" content="ゲームアプリのデータ引き継ぎ方法ページです。すでにお使いのゲームでも、レベルはそのままでお得に遊べます。" />

    <?php inc("head"); ?>
  </head>

  <body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

    <!--//////////////////////
      nav
    //////////////////////  -->
        <?php inc("header"); ?>
        <?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games">

      <!-- operation  -->
      <div class="ranking operation">
        <div class="operation-top-title">
          データ引継ぎ方法
        </div>
        <div class="list">
          <div class="headline">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9351610000001.png">
            <div class="title">
              <h3>パズル&ドラゴン</h3>
              <p>パズル</p>
            </div>
          </div><!-- headline -->

          <!-- リンクボタン -->
          <div class="operation-btn-box">
            <a href="https://pass.auone.jp/app/detail?app_id=9351610000001&rf=gameportal" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','DLボタン_恐竜ドミニオン',event);"
>アプリダウンロードページを見る</a>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ1</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
            </div>
          </div>

          <ol class="list-num">
            <li>
              [GooglePlay版]のアプリを起動してその他をタップします。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/9351610000001_01.png">
              </div>
            </li>
            <li>
              「機種変更」ボタンをタップします。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/9351610000001_02.png">
              </div>
            </li>
            <li>
              「機種変コード発行」ボタンをタップします。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/9351610000001_03.png">
              </div>
            </li>
            <li>
              注意事項をよくご確認いただき、<br>「発行する」をタップします。

              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/9351610000001_04.png">
              </div>
            </li>
            <li>
              フレンドをタップし、ゲーム内メールから機種変コード発行のメールを開いてください。<br>
あなたのIDと機種変コードをメモやスクリーンショットでお控えください。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/9351610000001_05.png">
              </div>
            </li>
          </ol>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ2</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            [GooglePlay版]アプリを一度アンインストールします。<br>
            <span class="red">※ステップ1の手続きをせずにアンインストールをしてしまいますと、データを引き継ぐことができません。</span>
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/9351610000001_06.png">
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ3</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            【ダウンロード】ボタンを押下して、au MarketからアプリをDLする。
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/1.png">
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ4</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              ステップ3でダウンロードしたアプリを起動し、利用規約に同意後、名前入力画面で「機種変コードを使ったゲームデータ移行はコチラ」をタップします。<br>
              <span class="red">※「au」ロゴが入っています！</span>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/9351610000001_07.png">
              </div>
            </li>
            <li>
               ステップ1でメモしたあなたのIDを入力します。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/9351610000001_08.png">
              </div>
            </li>
            <li>
              ステップ1で発行した機種変コードを入力します。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/9351610000001_09.png">
              </div>
            </li>
            <li>
                「はい」ボタンをタップします。<br>
                以上でアカウント移行は完了です。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/9351610000001_10.png">
              </div>
            </li>
          </ol>

          <!-- <div class="special-gift">
            <div class="headline">
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_gift.png">
              ギフト
            </div>
            <p>
              龍玉２５個プレゼント！
            </p>
          </div>
          <div class="operation-btn-box">
            <a href="http://pass.auone.jp/app/detail?app_id=4698000000007&rf=gameportal" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','ギフトボタン_恐竜ドミニオン',event);">ギフトをゲットする</a>
          </div> -->

        </div><!-- list -->
          <div class="step-bg-gray">
            <div>
                ※当ページ（データ引き継ぐには）は、KDDIが独自に調べ作成したもので、アプリ会社とは関係ございません。アプリのバージョンにより、正常に移行できない可能性がございますので、最新のバージョンでお試しください。
            </div>
          </div>
      </div><!-- manual -->
      <?php inc("adRect_android"); ?>
    </div>


        <?php inc("footer"); ?>

</div><!--contens-body-->
</div>

    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

  </body>
</html>