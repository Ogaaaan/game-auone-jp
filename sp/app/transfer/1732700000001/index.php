<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
<meta charset="UTF-8">
<title>データ引き継ぎ方法 - auゲーム</title>
<meta name="keywords" content="データ引き継ぎ方法,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
<meta name="description" content="?" />

<?php inc("head"); ?>

</head>

<body class="all-app">
<div class="js-t-wrapper">
<div class="contens-body">

<?php inc("header"); ?>
<?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games" id="#top">

      <!-- operation  -->
      <div class="ranking operation">
        <div class="operation-top-title">
          データ引継ぎ方法
        </div>
        <div class="list">
          <div class="headline">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_1732700000001.png">
            <div class="title">
              <h3>どかだん for auスマートパス</h3>
              <p>カジュアル</p>
            </div>
          </div><!-- headline -->


          <!-- 特典 -->
          <div class="special-gift">
            <div class="headline">
              <!-- 特典枠タイトル -->
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png">
              特典
            </div>
            <p>
              <!-- 特典枠文言 -->
              虹色ダイヤ100円購入ごとに10ポイント
            </p>
          </div>

          <!-- リンクボタン -->
          <div class="operation-btn-box">
            <a href="http://pass.auone.jp/app/detail?app_id=1732700000001&rf=gameportal" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','DLボタン_どかだん for auスマートパス',event);"
>アプリダウンロードページを見る</a>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ１</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            【ダウンロード】ボタンを押下して、au MarketからアプリをDLする。
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/1.png">
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ２</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            ホーム画面にアプリが２つあることを確認する。
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/2.png">
          </div>

          <!-- 機種変更補足文 -->
          <div class="step-bg-gray">
            <p>
              <span class="title">機種変更の場合</span><br>
              ホーム画面には、一つのアプリのみが出現します。
              </p>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ３</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>

              どっかーん海賊団を起動してタイトル画面を表示します。<br><br>
              画面左下の「引越し」をタップしてください。

              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/1732700000001_01.png">
              </div>

            </li>
            <li>
              「IDの発行」をタップしてください。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/1732700000001_02.png">
              </div>
            </li>
            <li>
              引継ぎIDが発行されますので忘れないように引継ぎIDをメモをしておいてください。<br><br>

              「実行」ボタンを押下してください。<br>
              <span class="red">※発行した引継ぎIDの有効期限は24時間ですので、期限内に引継ぎを完了させてください。</span>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/1732700000001_03.png">
              </div>
            </li>
            <li>
              データ引継ぎ処理実行中が表示されれば、バックアップの登録は完了です。<br>
              <span class="red">※取り消しボタンを押すと引継ぎが解除されますのでご注意ください。</span><br><br>

              どっかーん海賊団を終了させてください。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/1732700000001_04.png">
              </div>
            </li>
          </ol>

          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ４</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              ホーム画面より、auロゴのついたアイコンを押下し、どっかーん海賊団を起動してタイトル画面を表示します。<br><br>

              画面左下の「引越し」をタップしてください。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/1732700000001_01.png">
              </div>
            </li>
            <li>
              引継ぎIDの入力欄に、STEP3-3 で発行した引継ぎIDを入力してください。<br><br>
              引継ぎIDの入力が完了したら「引き継ぐ」をタップしてください。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/1732700000001_05.png">
              </div>
            </li>
            <li>
              データの引継ぎ完了の表示で、引継ぎ完了となります。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/1732700000001_06.png">
              </div>
            </li>
          </ol>


          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ５</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              「auロゴ」のついたアプリを起動してデータ移行されていることを確認
            </li>
            <li>
              旧アプリをホーム画面から削除する
            </li>
            <li>
              <span class="red">データ引継ぎ完了！</span>
            </li>
          </ol>

          </div><!-- list -->
          <div class="step-bg-gray">
            <div>
                ※当ページ（データ引き継ぐには）は、KDDIが独自に調べ作成したもので、アプリ会社とは関係ございません。アプリのバージョンにより、正常に移行できない可能性がございますので、最新のバージョンでお試しください。
            </div>
          </div>
        </div><!-- manual -->
      <?php inc("adRect_android"); ?>
    </div>

<?php inc("footer"); ?>

</div><!--contens-body-->
</div>

<?php inc("script"); ?>
<?php inc("gtm"); ?>

</body>
</html>