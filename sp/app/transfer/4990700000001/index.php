<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
<meta charset="UTF-8">
<title>データ引き継ぎ方法 - auゲーム</title>
<meta name="keywords" content="データ引き継ぎ方法,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
<meta name="description" content="ゲームアプリのデータ引き継ぎ方法ページです。すでにお使いのゲームでも、レベルはそのままでお得に遊べます。" />

<?php inc("head"); ?>

</head>

  <body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">
<?php inc("header"); ?>
<?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games" id="#top">

      <!-- operation  -->
      <div class="ranking operation">
        <div class="operation-top-title">
          データ引継ぎ方法
        </div>
        <div class="list">
          <div class="headline">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4990700000001.png">
            <div class="title">
              <h3>誓いのキスは突然に</h3>
              <p>恋愛ゲーム</p>
            </div>
          </div><!-- headline -->


          <!-- 特典 -->
          <div class="special-gift">
            <div class="headline">
              <!-- 特典枠タイトル -->
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/star.png">
              特典
            </div>
            <p>
              <!-- 特典枠文言 -->
              コイン100円購入ごとに10ポイント
            </p>
          </div>

          <!-- リンクボタン -->
          <div class="operation-btn-box">
            <a href="http://pass.auone.jp/app/detail?app_id=4990700000001&rf=gameportal" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','DLボタン_誓いのキスは突然に',event);"
>アプリダウンロードページを見る</a>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ１</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            【ダウンロード】ボタンを押下して、au MarketからアプリをDLする。
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/1.png">
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ２</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            ホーム画面にアプリが２つあることを確認する。
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/2.png">
          </div>

          <!-- 機種変更補足文 -->
          <div class="step-bg-gray">
            <p>
              <span class="title">機種変更の場合</span><br>
              ホーム画面には、一つのアプリのみが出現します。
              </p>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ３</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              誓いのキスは突然にを起動してホーム画面を表示します。<br>
              画面下部右の「サポート」をタップし、続いて「データ引継」をタップしてください。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4990700000001_01.png">
              </div>
            </li>
            <li>
              データ引継登録画面でパスワードを入力し、「登録」ボタンをタップしてください。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4990700000001_02.png">
              </div>
            </li>
            <li>
              以上でバックアップは完了です。引継IDとパスワードは忘れないようにメモして控えておきましょう。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4990700000001_03.png">
              </div>
            </li>
          </ol>

          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ４</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              STEP1でダウンロードした誓いのキスは突然にを起動し、「データを引き継ぎ」ボタンをタップします。<br>
<span class="red">※「au」ロゴの入っている方を起動してください！</span>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4990700000001_04.png">
              </div>
            </li>
            <li>
              STEP3の3でメモした引継IDとパスワードを入力して「送信」ボタンをタップします。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4990700000001_05.png">
              </div>
            </li>
            <li>
              認証成功の画面が表示されます。「OK」ボタンをタップしてデータ引継ぎ完了です。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4990700000001_06.png">
              </div>
            </li>
          </ol>


          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ５</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              「auロゴ」のついたアプリを起動してデータ移行されていることを確認
            </li>
            <li>
              旧アプリをホーム画面から削除する
            </li>
            <li>
              <span class="red">データ引継ぎ完了！</span>
            </li>
          </ol>

          <div class="special-gift">
            <div class="headline">
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/common/icon_gift.png">
              ギフト
            </div>
            <p>
              限定アイテム付！
            </p>
          </div>


          <div class="operation-btn-box">
            <a href="http://gamegift.jp/lp/au/chikainokiss/" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','ギフトボタン_誓いのキスは突然に',event);">ギフトをゲットする</a>
          </div>

		      </div><!-- list -->
          <div class="step-bg-gray">
            <div>
                ※当ページ（データ引き継ぐには）は、KDDIが独自に調べ作成したもので、アプリ会社とは関係ございません。アプリのバージョンにより、正常に移行できない可能性がございますので、最新のバージョンでお試しください。
            </div>
          </div>
 	     </div><!-- manual -->
      <?php inc("adRect_android"); ?>
   </div>

<?php inc("footer"); ?>

</div><!--contens-body-->
</div>

<?php inc("script"); ?>
<?php inc("gtm"); ?>

  </body>
</html>