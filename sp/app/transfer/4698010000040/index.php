<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="jp_JP">
<head>
<meta charset="UTF-8">
<title>データ引き継ぎ方法 - auゲーム</title>
<meta name="keywords" content="データ引き継ぎ方法,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
<meta name="description" content="ゲームアプリのデータ引き継ぎ方法ページです。すでにお使いのゲームでも、レベルはそのままでお得に遊べます。" />
<?php inc("head"); ?>

</head>

<body class="all-app">
<div class="js-t-wrapper">
<div class="contens-body">

<?php inc("header"); ?>
<?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games">

      <!-- operation  -->
      <div class="ranking operation">
        <div class="operation-top-title">
          データ引継ぎ方法
        </div>
        <div class="list">
          <div class="headline">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000040.png">
            <div class="title">
              <h3>バトルガールハイスクール</h3>
              <p>ロールプレイング</p>
            </div>
          </div><!-- headline -->

          <!-- リンクボタン -->
          <div class="operation-btn-box">
            <a href="https://pass.auone.jp/app/detail?app_id=4698010000040&rf=gameportal" class="operation-button" onclick="trEventBe(this,'auゲーム','引き継ぎ方法','DLボタン_バトルガールハイスクール',event);">アプリダウンロードページを見る</a>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ１</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            【ダウンロード】ボタンを押下して、au MarketからアプリをDLする。
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/1.png">
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ２</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <!-- ステップタイトル -->
          <div class="step-title">
            ホーム画面にアプリが２つあることを確認する。
          </div>

          <!-- 画像 -->
          <div class="step-img">
            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/2.png">
          </div>

          <!-- 機種変更補足文 -->
          <div class="step-bg-gray">
            <p>
              <span class="title">機種変更の場合</span><br>
              ホーム画面には、一つのアプリのみが出現します。
              </p>
          </div>

          <!-- ステップアイコン -->
          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ３</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
                バトルガールを起動してホーム画面を表示します。<br>
                画面上部の「MENU」ボタンをタップすると、MENU一覧が表示されますので、「オプション」を選択してください。
                <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000040_01.png">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000040_02.png">
                </div>
            </li>
            <li>
                「アカウント設定」を選択する。
                <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000040_03.png">
                </div>
            </li>
            <li>
                「Googleアカウントで登録」を選択してください。
                <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000040_04.png">
                </div>
            </li>
            <li>
              登録したいメールアドレスをフィールドに入力し「次へ」ボタンを押下してください。<br><br>
              6文字以上のパスワードを設定してください。<br>
              ※設定したパスワードは忘れないようにメモをしておいてください。<br><br>
              「次へ」ボタンを押下してください。
                <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000040_05.png">
                </div>
            </li>
            <li>
                登録成功の画面が表示されれば、アカウントの登録は完了です。
                <div class="step-img">
                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000040_06.png">
                </div>
            </li>
          </ol>

          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ４</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              ホーム画面より、auロゴのついたアイコンを押下し、「機種変更はこちら」をタップする。<br>
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000040_07.png">
              </div>
            </li>
            <li>
              「Googleアカウント」でログインしてください。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000040_08.png">
              </div>
            </li>
            <li>
              STEP3-4で設定したメールアドレスとパスワードを入力し、「OK」ボタンをタップしてください。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000040_09.png">
              </div>
            </li>
            <li>
              ログイン成功画面で、引継ぎ完了となります。
              <div class="step-img">
                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/4698010000040_06.png">
              </div>
            </li>
          </ol>


          <div class="step-icon-box">
            <div class="step-icon">
              <p>ステップ５</p>
              <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/triangle.png">
              <a class="pagetop" href="javascript:void(0)">ページトップに戻る</a>
            </div>
          </div>

          <ol class="list-num">
            <li>
              「auロゴ」のついたアプリを起動してデータ移行されていることを確認
            </li>
            <li>
              旧アプリをホーム画面から削除する
            </li>
            <li>
              <span class="red">データ引継ぎ完了！</span>
            </li>
          </ol>
        </div><!-- list -->

        <div class="step-bg-gray">
          <div>※当ページ（データ引き継ぐには）は、KDDIが独自に調べ作成したもので、アプリ会社とは関係ございません。アプリのバージョンにより、正常に移行できない可能性がございますので、最新のバージョンでお試しください。</div>
        </div>
        </div><!-- manual -->
      <?php inc("adRect_android"); ?>
    </div>

<?php inc("footer"); ?>

</div><!--contens-body-->
</div>

<?php inc("script"); ?>
<?php inc("gtm"); ?>

</body>
</html>