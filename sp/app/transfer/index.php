<?php include("../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
<meta charset="UTF-8">
    <title>データ引き継ぎ方法一覧 - auゲーム</title>
    <meta name="keywords" content="データ引き継ぎ方法,ゲーム,アプリ,スマートパス,スマパス,ポイント,取り放題" />
    <meta name="description" content="ゲームアプリのデータ引き継ぎ方法一覧ページです。ゲーム内で課金した金額の10％がWALLETポイントで還元される、お得なゲームアプリをご紹介。すでにお使いのゲームでも、レベルはそのままでお得に遊べます。" />

    <?php inc("head"); ?>
  </head>

  <body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">
    <!--//////////////////////
      nav
    //////////////////////  -->
        <?php inc("header"); ?>
        <?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games">

      <!-- ranking -->
      <div class="ranking manual-all">
        <div class="text-title">
          <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/manual-all-text.png">
          <h2>データ引継ぎ方法一覧</h2>
        </div>

        <div class="list first">
          <div class="headline">
            <h3>「ア行」からはじまるゲーム一覧</h3>
          </div><!-- headline -->
          <ul class="apps">
            <li>
              <a href="4698010000030/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','蒼の三国志',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000030.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>蒼の三国志</h4>
                    <p class="details">シミュレーション・ストラテジー</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div><!-- list -->

        <div class="list">
          <div class="headline">
            <h3>「カ行」からはじまるゲーム一覧</h3>
          </div><!-- headline -->
          <ul class="apps">
            <li>
              <a href="4698010000041/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','カジノプロジェクト',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000041.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>カジノプロジェクト</h4>
                    <p class="details">シミュレーション・ストラテジー</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="4698000000007/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','恐竜ドミニオン',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000007.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>恐竜ドミニオン</h4>
                    <p class="details">シミュレーション・ストラテジー</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div><!-- list -->

        <div class="list">
          <div class="headline">
            <h3>「サ行」からはじまるゲーム一覧</h3>
          </div><!-- headline -->
          <ul class="apps">
            <li>
              <a href="5235400000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','サウザンドメモリーズ',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_5235400000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>サウザンドメモリーズ</h4>
                    <p class="details">ロールプレイング</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="4698010000038/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','白猫プロジェクト',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000038.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>白猫プロジェクト</h4>
                    <p class="details">ロールプレイング</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="7132500000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','新章イケメン大奥◆禁じられた恋',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7132500000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>新章イケメン大奥◆禁じられた恋</h4>
                    <p class="details">恋愛ゲーム</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="4698010000035/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','スリングショットブレイブズ',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000035.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>スリングショットブレイブズ</h4>
                    <p class="details">ロールプレイング</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="9005100000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','戦国X（センゴククロス）',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9005100000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>戦国X（センゴククロス）</h4>
                    <p class="details">シミュレーション・ストラテジー</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div><!-- list -->
        <div class="list">
          <div class="headline">
            <h3>「タ行」からはじまるゲーム一覧</h3>
          </div><!-- headline -->
          <ul class="apps">
            <li>
              <a href="4990700000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','誓いのキスは突然に',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4990700000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>誓いのキスは突然に</h4>
                    <p class="details">恋愛ゲーム</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="4698010000018/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','ディズニー マジシャン・クロニクル',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000018.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>ディズニー マジシャン・クロニクル</h4>
                    <p class="details">シミュレーション・ストラテジー</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="1732700000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','どかだん for auスマートパス',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_1732700000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>どかだん for auスマートパス</h4>
                    <p class="details">カジュアル</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="4585600000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','ドラゴンファング',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4585600000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>ドラゴンファング</h4>
                    <p class="details">ロールプレイング</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div><!-- list -->

        <div class="list">
          <div class="headline">
            <h3>「ハ行」からはじまるゲーム一覧</h3>
          </div><!-- headline -->
          <ul class="apps">
            <li>
              <a href="9351610000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','パズル&ドラゴン',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9351610000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>パズル&ドラゴン</h4>
                    <p class="details">パズル</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="4698010000040/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','バトルガールハイスクール',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000040.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>バトルガールハイスクール</h4>
                    <p class="details">ロールプレイング</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="4698010000005/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','秘宝探偵キャリー',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000005.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>秘宝探偵キャリー</h4>
                    <p class="details">シミュレーション・ストラテジー</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="4698000000006/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','プロ野球PRIDE',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698000000006.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>プロ野球PRIDE</h4>
                    <p class="details">スポーツ</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="6721200000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','ぼくとドラゴン',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6721200000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>ぼくとドラゴン</h4>
                    <p class="details">ロールプレイング</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="4526600000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','バーコードフットボーラー',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4526600000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>ぼくらの甲子園！ポケット for auスマートパス</h4>
                    <p class="details">スポーツ</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="9189400000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','ポケコロ',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_9189400000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>ポケコロ</h4>
                    <p class="details">カジュアル</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="4698010000039/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','ほしの島のにゃんこ [島づくりシミュレーションゲームアプリ] for auスマートパス',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000039.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>ほしの島のにゃんこ [島づくりシミュレーションゲームアプリ] for auスマートパス</h4>
                    <p class="details">シミュレーション・ストラテジー</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div><!-- list -->

        <div class="list">
          <div class="headline">
            <h3>「マ行」からはじまるゲーム一覧</h3>
          </div><!-- headline -->
          <ul class="apps">
            <li>
              <a href="4698010000022/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','魔法使いと黒猫のウィズ',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_4698010000022.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>魔法使いと黒猫のウィズ</h4>
                    <p class="details">ロールプレイング</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="6002000000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','モンスターストライク',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6002000000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>モンスターストライク</h4>
                    <p class="details">アクション</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div><!-- list -->

        <div class="list">
          <div class="headline">
            <h3>「ヤ行」からはじまるゲーム一覧</h3>
          </div><!-- headline -->
          <ul class="apps">
            <li>
              <a href="3705000000001/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','勇者と1000の魔王-覚醒-',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_3705000000001.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>勇者と1000の魔王-覚醒-</h4>
                    <p class="details">ロールプレイング</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div><!-- list -->

        <div class="list">
          <div class="headline">
            <h3>「a-z」からはじまるゲーム一覧</h3>
          </div><!-- headline -->
          <ul class="apps">
            <li>
              <a href="6798180002101/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','BEST☆ELEVEN+',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_6798180002101.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>BEST☆ELEVEN+</h4>
                    <p class="details">スポーツ</p>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href="7937300000003/" onclick="trEventBe(this,'auゲーム','引き継ぎ方法一覧','sword of phantasia',event);">
                <div class="border">
                  <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/transfer/1410/icon_7937300000003.png" class="app-icon">
                  <div class="app-text app-list-top">
                    <h4>sword of phantasia</h4>
                    <p class="details">ロールプレイング</p>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div><!-- list -->
      </div><!-- ranking -->
          <?php inc("adRect_android"); ?>
    </div>

    <?php inc("footer"); ?>

</div><!--contens-body-->
</div>

    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

  </body>
</html>