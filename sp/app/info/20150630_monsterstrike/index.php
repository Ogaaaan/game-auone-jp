<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja-JP">
<head>
<meta charset="UTF-8">

    <title>重要なお知らせ - auゲーム</title>
    <meta name="keywords" content="モンスターストライク,auゲーム,ゲーム,アプリ,スマートパス,スマパス,auスマートパス" />
    <meta name="description" content="auゲーム「モンスターストライクをご利用のお客様へのお知らせ」" />

    <?php inc("head"); ?>

    <style>
    .t-head__text{display:block; padding-top:10px;}
    .t-text__middle p{margin-bottom:15px;}
    </style>
  </head>

  <body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

    <!--//////////////////////
      nav
    //////////////////////  -->
        <?php inc("header"); ?>
        <?php inc("adSP_android"); ?>

    <!--//////////////////////
      all-games
    //////////////////////  -->
    <div class="all-games">
      <!-- operation  -->
      <div class="ranking operation">
        <div class="list">

        <!-- head -->
        <div class="t-head">
            <h2 class="t-head__text">【重要なお知らせ】モンスターストライクをご利用のお客様へ</h2>
            <div class="t-text">
                <div class="t-text__small">(Ver.4.3.1の誤配布についてのお詫びと再ダウンロードのお願い)</div>
            </div>
        </div>
        <!-- /head -->

        <div class="t-text">
            <div class="t-text__middle">
              <p>モンスターストライク運営事務局です。<br>いつも、モンスターストライクをお楽しみ頂き、誠にありがとうございます。</p>
              <p>本日(6/30)AM5:00からAM10:30までの間、auスマートパスにて配布されていたVer.4.3.1につきまして、リリース前に不具合が確認された為、本来リリースを見送る予定のバージョンでしたが、誤って本日(6/30)AM5:00頃より配布されておりました。<br>
                その為、現在、Ver.4.3.0に戻す為、Ver.4.3.0のアプリの配布を実施しております。<br>
                auスマートパス版のアプリをご利用の皆様へはご迷惑をおかけし大変申し訳ございません。</p>
            </div>
        </div>

        <!-- t-column-head -->
        <div class="t-column-head">
            <h4 class="t-column-head__text">6/30 AM5:00以前にauスマートパス版のアプリをダウンロードされている方へ</h4>
        </div>
        <!-- /t-column-head -->
        <div class="t-text">
            <div class="t-text__middle">
              6/30 AM5:00以前にauスマートパス版のアプリをダウンロードされている方で、既にVer.4.3.1にアップデートされている方は、Ver.4.3.0へ戻す為、再度アップデートを実行していただけますようお願い申し上げます。ご迷惑をおかけし大変申し訳ございません。
            </div>
        </div>

        <!-- t-column-head -->
        <div class="t-column-head">
            <h4 class="t-column-head__text">6/30 AM5:00頃からAM10:30までの間に新規でauスマートパス版のアプリをダウンロードされた方へ</h4>
        </div>
        <!-- /t-column-head -->
        <div class="t-text">
            <div class="t-text__middle">
              <p>本日(6/30)AM5:00頃からAM10:30までの間に新規で、auスマートパスよりアプリをダウンロードされた皆様は、大変心苦しいのですが、Ver.4.3.1のアプリを削除いただき、再度Ver.4.3.0のアプリを新規でダウンロードしていただく必要がございます。なお、アプリを削除頂き、新規でダウンロードした場合、それまでのデータは消去されてしまうことになります。ご迷惑をおかけし大変申し訳ございません。ご不明な点等ございましたら、サポートまでお問い合わせ頂けますようお願い申し上げます。</p>
              <p>Ver.4.3.1は、開発中に不具合が確認されていた為、リリースを見送る予定のバージョンでした。そのようなバージョンを誤って配布してしまい、ご迷惑をおかけし大変申し訳ございません。なお、Ver.4.3.1は本来、軽微な不具合修正の為に配布を予定していたバージョンで、新機能などのリリースの為に予定されていたバージョンではございません。</p>
              <p>ご利用されている皆様が安心してお楽しみいただけるサービスとなるように、引き続き運営に努めてまいります。<br>
              今後ともモンスターストライクをよろしくお願いいたします。</p>
              <p>モンスターストライク運営事務局</p>
            </div>
        </div>

        </div><!-- list -->

      </div><!-- manual -->
      <?php inc("adRect_android"); ?>

    </div>
    <?php inc("footer"); ?>
</div><!--contens-body-->
</div>
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

  </body>
</html>