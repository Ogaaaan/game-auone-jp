# auゲームトップページの前後テスト（5/17公開予定分）
# 確認URL

stgアップ
- https://stg.game.auone.jp/app/index.html

制作内テストアップ
- http://mediba.jpn.org/game.auone.jp/sp/index.html

Redmine
- http://redmine.mediba.local/issues/11042
***
# 仕様
 - 既存の「オススメコラム」と同様のajaxで「おすすめアプリ枠」を別ファイル管理
 - 「おすすめアプリ枠」は常時6枠（最大10枠）
 - カテゴリーごとの表題右、「もっと見る」リンクは任意で表示非表示
 - １枠内は3つのアプリ（固定）
 - カテゴリー内の「王冠」「ワッペン」「メダル（丸い形）」は各３種類あり、html内に記述。

 ***
## index.html
 既存との変更箇所
 - 16:　game_top.cssをgame_block.cssに差し替え
 - 50:「.hyperlink-button_group」内のリンク先をページ内リンクからサービス内リンクへ
 - 54: 文言「カテゴリ」→「カテゴリー」へ
 - 81: 文言「ランキング」→「人気ランキング」へ
 - 71~123: 既存ランキング枠。イベント「trEventBe」の第3引数変更
 - 171: js-veiw__load-contents要素内に「おすすめアプリ枠」コンテンツの表示

手順
 - 要素削除
  - コメント内「special-games」を親にまるっと削除
  - コメント内「ranking」を親にまるっと削除
 - 要素追加
   - 外部ファイル化しているjs-game_category-list.htmlの要素を挿入。
 - 上記「変更箇所」参照お願いします（何かあればすぐお呼びください）
***

## js-game_category-list.html
既存との変更箇所
 - iconの出し分けclass　
    - .recommend-icon__a(丸いメダル型img)
        - .recommend-icon__a01(３つ星)
        - .recommend-icon__a02(2つ星)
        - .recommend-icon__a03(1つ星)
    - .recommend-icon__a(王冠型img)
        - .recommend-icon__b01(３つ星)
        - .recommend-icon__a01(2つ星)
        - .recommend-icon__a01(1つ星)
    - .recommend-icon__a(５角形ワッペンimg)
        - .recommend-icon__c01(３つ星)
        - .recommend-icon__c01(2つ星)
        - .recommend-icon__c01(1つ星)

 - 「もっと見る」リンクは「t-crosshead__ex」のブロックです。
 - こちらの更新ファイルの置き場所はコラム欄ファイルと同様にapp直下に置いてあります。そのファイルの指定はtop.js内でされています。
 stg上では
 https://stg.game.auone.jp/app/js-game_category-list.html です。
 dev上では
 http://dev.game.auone.jp/app/js-game_category-list.html です。

 - 各リンク。イベント「trEventBe」の第3引数変更

***

## 実機確認
制作棚
 - SOL23 4.3.3 (chrome/標準)
 - C01   2.3.5 (標準)
 - IS05  2.3.4 (標準)
 - SOL21　4.1.2 (標準/chrome)

## 制作者
- 森田賢二
