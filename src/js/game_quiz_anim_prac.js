/**
 * 練習問題用アニメーション
 *
 * @author    Seiji Ogawa [s-ogawa@mediba.jp]
 * @copyright Mediba.inc 2015
 */

$(function() {

    /**
     * クイズ結果判定
     */
    if(resultData.ans) {
        // 正解の処理
        anim.practiceCorrect();
    } else {
        // 不正解の処理
        anim.practiceIncorrect();
    }

 });

/**
 * アニメーションステージ内を背景と前景で分ける
 * 前景は背景に対して子要素
 * 前景は常に背景のセンター揃え
 *
 * gq-quiz__result-anim : 前景
 * gq-quiz__result-frame: 背景
 */
 $('<div>')
     .addClass('gq-quiz__result-anim')
     .appendTo('.gq-quiz__result-frame');

/**
 * アニメーションオブジェクト
 *
 * jquery.a3d.jsに依存
 *
 * 各アニメーションエレメントのz-index
 * <ol>
 * <li>背景
 * <li>＜未使用＞
 * <li>後光
 * <li>＜未使用＞
 * <li>抽選のシャッフル画像、あたり画像
 * <li>＜未使用＞
 * <li>＜未使用＞
 * <li>スポットライト、コインフォール、ショックライン
 * <li>＜未使用＞
 * <li>センセイ、カナリアちゃん
 * </ol>
 *
 * @type {Object}
 */
var anim = {
    $background: $('.gq-quiz__result-frame'),
    $foreground: $('.gq-quiz__result-anim'),
    /**
     * 以下環境によって差し替える
     * リポジトリ側へは商用を有効にした状態にしておくこと
     */
    imageUrl: '//cdn-img.auone.jp/game/img/quiz', // 商用
    //imageUrl: '//cdn-img.mdev.auone.jp/game/img/quiz', // mdev
    //imageUrl: '//stg.cdn-img.auone.jp/game/img/quiz', // STG
    //imageUrl: '../../src/img/quiz', // ローカル

    /**
     * 練習問題正解
     * @return void
     */
    practiceCorrect: function() {

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            animBack: anim.imageUrl + '/anim_back_correct.png',
            animSensei: anim.imageUrl + '/anim_sensei_correct.png',
            animCanaria: anim.imageUrl + '/anim_canaria_correct.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // 背景
            $(anim.$background)
            .css({
                backgroundImage: 'url(' + imgs.animBack + ')'
            });

            // センセイ
            $('<img>').attr({
                id: 'animSensei',
                src: imgs.animSensei
            })
            .css({
                left: -58,
                bottom: -45,
                zIndex: 10,
                transformOrigin: '50% 200%'
            })
            .appendTo(anim.$foreground);

            // カナリアちゃん
            $('<img>').attr({
                id: 'animCanaria',
                src: imgs.animCanaria
            })
            .css({
                right: -36,
                bottom: -60,
                zIndex: 10,
                transformOrigin: '50% 200%'
            })
            .appendTo(anim.$foreground);

            /**
             * モーションデータ
             */
            var motions = {
                animSensei: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '-90deg'
                            }
                        },
                        '10%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '30%': {
                            trans: {
                                y: 0
                            }
                        },
                        '35%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '40%': {
                            trans: {
                                y: 0
                            }
                        },
                        '45%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '50%': {
                            trans: {
                                y: 0
                            }
                        },
                        '55%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '60%': {
                            trans: {
                                y: 0
                            }
                        }
                    },
                    config: {
                        duration: '2.5s',
                        easing: 'easeOutBack'
                    }
                },
                animCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '90deg'
                            }
                        },
                        '10%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '32%': {
                            trans: {
                                y: 0
                            }
                        },
                        '37%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '42%': {
                            trans: {
                                y: 0
                            }
                        },
                        '47%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '52%': {
                            trans: {
                                y: 0
                            }
                        },
                        '57%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '62%': {
                            trans: {
                                y: 0
                            }
                        }
                    },
                    config: {
                        duration: '2.5s',
                        easing: 'easeOutBack'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            $('#animSensei').a3d(motions.animSensei);
            $('#animCanaria').a3d(motions.animCanaria);

        });

    },

    /**
     * 練習問題不正解
     *
     */
    practiceIncorrect: function () {

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            animBack: anim.imageUrl + '/anim_back_incorrect.png',
            animSensei: anim.imageUrl + '/anim_sensei_incorrect.png',
            animCanaria: anim.imageUrl + '/anim_canaria_incorrect.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // 背景
            $(anim.$background)
            .css({
                backgroundImage: 'url(' + imgs.animBack + ')',
                backgroundSize: '440px 150px',
                backgroundPosition: 'center',
                zIndex: 1
            });

            // センセイ
            $('<img>').attr({
                id: 'animSensei',
                src: imgs.animSensei
            })
            .css({
                left: -58,
                bottom: -45,
                transformOrigin: '50% 200%',
                zIndex: 10
            })
            .appendTo(anim.$foreground);

            // カナリアちゃん
            $('<img>').attr({
                id: 'animCanaria',
                src: imgs.animCanaria
            })
            .css({
                right: -36,
                bottom: -62,
                transformOrigin: '50% 200%',
                zIndex: 10
            })
            .appendTo(anim.$foreground);

            /**
             * モーションデータ
             */
            var motions = {
                animSensei: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '-90deg'
                            }
                        },
                        '100%': {
                            trans: {
                                rotate: '0deg'
                            }
                        }
                    },
                    config: {
                        duration: '0.8s',
                        easing: 'easeOutBack'
                    }
                },
                animCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '90deg'
                            }
                        },
                        '100%': {
                            trans: {
                                rotate: '0deg'
                            }
                        }
                    },
                    config: {
                        duration: '0.8s',
                        easing: 'easeOutBack'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            $('#animSensei').a3d(motions.animSensei);
            $('#animCanaria').a3d(motions.animCanaria);

        });

    },

    /**
     * 以降、プライベートメソッド扱い
     * ※getter、setterを使用する
     */

    /**
     * 画像先読み
     * @param  {list}    images 画像パスを配列で指定
     * @return {promise}        promiseオブジェクトを返却
     */
    _preloadImages: function(imgs) {

        var promises = [];
        var returnDef = $.Deferred();

        // 画像リストをイテレート開始
        $.each(imgs, function() {
            var img = new Image();
            var defer = $.Deferred();
            // 画像ファイル読み込みイベント
            img.addEventListener('load', function () {
                defer.resolve();
                defer = null;
            });
            // 画像ファイル読み込み失敗イベント
            img.addEventListener('error', function () {
                defer.reject();
                defer = null;
            });
            // 実際の読み込み処理(thisは画像パス)
            img.src = this;
            // 各画像のプロミスオブジェクトを配列にスタック
            promises.push(defer.promise());
        });

        // 結果の評価
        $.when.apply(null, promises)
        .done(function () {
            returnDef.resolve();
        })
        .fail(function () {
            returnDef.reject();
        });

        // スタックしたプロミスオブジェクトを返却
        return returnDef.promise();
    },

    /**
     * 待機
     * @param  {Function} callback 待機後に実行する関数
     * @param  {Integer}   time     待機するミリ秒
     * @return void
     */
    _wait: function (callback, time) {
        setTimeout(callback, time);
    },

    /**
     * フォルトトレランス用
     * @return null
     */
    _faultTolerance: function () {
        console.log('404 image not found.');
        console.log(arguments);
    }

};
