/**
 * クイズチュートリアルアニメーション
 *
 * @author    Seiji Ogawa [s-ogawa@mediba.jp]
 * @copyright Mediba.inc 2015
 */

var coinFall = false;

$(function() {

    // 閲覧フラグ用Cookie設定
    $.cookie('tutorial', 1, { expires: 365, path: '/', secure: true });

    // カルーセル設定
    var $arrowNext = $('.gq-tutorial__carousel-arrow--next');
    var slick = $('.gq-tutorial__carousel').slick({
        autoplay: false,
        arrows: false,
        dots: true,
        swipe: true,
        infinite: false,
    }).on('init', function(slick) {

        $('.gq-tutorial-main').show();

    }).on('beforeChange', function (event, slick, current, next) {

        // ページャ処理
        $arrowNext.fadeOut(100);

    }).on('afterChange', function (event, slick, current, next) {

        // ページャ処理
        var last = slick.slideCount - 1;
        if(current >= last ) {
            $arrowNext.fadeOut(300);
        } else {
            $arrowNext.fadeIn(300);
        }

        // アニメーション停止再開処理
        switch(current + 1) {
        case 1:
            anim._setPauseAllAnim();
            anim.firstPage();
            break;
        case 2:
            anim._setPauseAllAnim();
            anim.secondPage();
            break;
        case 3:
            anim._setPauseAllAnim();
            anim.thirdPage();
            break;
        case 4:
            anim._setPauseAllAnim();
            anim.fourthPage();
            break;
        case 5:
            anim._setPauseAllAnim();
            anim.fifthPage();
            break;
        }

    });

    // 画像に替えた矢印ボタンにイベントをバインド
    $arrowNext.on('click', function() {
        slick.slick('slickNext');
    });

    // 非表示背景を再表示
    for (var i = 2; i < 6; i++) {
        $('.gq-tutorial__carousel--0' + i).css('display', 'block');
    }

    // アニメーション開始
    anim.initialize();

});

/**
 * アニメーションオブジェクト
 *
 * jquery.a3d.jsに依存
 *
 * 各アニメーションエレメントのz-index
 * <ol>
 * <li>背景
 * <li>後光
 * <li>テレビ
 * <li>コイン
 * <li>センセイ、カナリアちゃん、吹き出し
 * <li>コインの山
 * <li>＜未使用＞
 * <li>＜未使用＞
 * <li>説明文
 * <li>タイトル
 * </ol>
 *
 * @type {Object}
 */
var anim = {
    /**
     * 以下環境によって差し替える
     * リポジトリ側へは商用を有効にした状態にしておくこと
     */
    imageUrl: '//cdn-img.auone.jp/game/img/quiz/tutorial', // 商用
    //imageUrl: '//cdn-img.mdev.auone.jp/game/img/quiz/tutorial', // mdev
    //imageUrl: '//stg.cdn-img.auone.jp/game/img/quiz/tutorial', // STG
    //imageUrl: '../../../../src/img/quiz/tutorial', // ローカル

    $arrowNext: $('.gq-tutorial__carousel-arrow--next'),

    /**
     * 初期設定
     * @return void
     */
    initialize: function () {
        anim.firstPage();
    },

    /**
     * スライド01
     * @return void
     */
    firstPage: function () {

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            anim01Sensei: anim.imageUrl + '/01_sensei.png',
            anim01Canaria: anim.imageUrl + '/01_canaria.png',
            anim01Title: anim.imageUrl + '/01_title.png',
            anim01Description: anim.imageUrl + '/01_description.png',
            anim03DescriptionBase: anim.imageUrl + '/03_description_base.png',
            anim00Shadow: anim.imageUrl + '/00_shadow.png'

        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            //次へボタンを時限表示
            setTimeout(function() {
                anim.$arrowNext.fadeIn(300);
            }, 3000);

            /**
             * アニメーションエレメント
             */

            // タイトル
            $('<img>').attr({
                id: 'anim01Title',
                src: imgs.anim01Title
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '10px',
                top: '5px',
                zIndex: 5,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--01');

            // 説明テキストベース
            // 最初から表示しておきたいのでここで処理させる
            $('<img>').attr({
                id: 'anim03DescriptionBase',
                src: imgs.anim03DescriptionBase
            })
            .css({
                position: 'absolute',
                left: '0px',
                bottom: '0px',
                zIndex: 8,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--03');

            // 説明テキスト
            $('<img>').attr({
                id: 'anim01Description',
                src: imgs.anim01Description
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '138px',
                bottom: '32px',
                zIndex: 9,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--01');

            // センセイ影
            $('<img>').attr({
                id: 'anim01SenseiShadow',
                src: imgs.anim00Shadow
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '162px',
                bottom: '154px',
                zIndex: 5,
                zoom: 0.5
            })
            .appendTo('.gq-tutorial__carousel--01');

            // センセイ
            $('<img>').attr({
                id: 'anim01Sensei',
                src: imgs.anim01Sensei
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '30px',
                bottom: '162px',
                zIndex: 5,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--01');

            // カナリアちゃん影
            $('<img>').attr({
                id: 'anim01CanariaShadow',
                src: imgs.anim00Shadow
            })
            .css({
                display: 'none',
                position: 'absolute',
                right: '130px',
                bottom: '154px',
                zIndex: 5,
                zoom: 0.5
            })
            .appendTo('.gq-tutorial__carousel--01');

            // カナリアちゃん
            $('<img>').attr({
                id: 'anim01Canaria',
                src: imgs.anim01Canaria
            })
            .css({
                display: 'none',
                position: 'absolute',
                right: '88px',
                bottom: '162px',
                zIndex: 5,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--01');

            /**
             * モーションデータ
             */
            var motions = {
                anim01Title: {
                    frames: {
                        '0%': {
                            trans: {
                                scale: 1
                            }
                        },
                        '50%': {
                            trans: {
                                scale: 0.95
                            }
                        },
                        '100%': {
                            trans: {
                                scale: 1
                            }
                        }
                    },
                    config: {
                        duration: '2s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                },
                anim01Sensei: {
                    frames: {
                        '40%': {
                            trans: {
                                y: 0
                            }
                        },
                        '45%': {
                            trans: {
                                y: '-20px'
                            }
                        },
                        '50%': {
                            trans: {
                                y: 0
                            }
                        }
                    },
                    config: {
                        duration: '4s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                },
                anim01SenseiShadow: {
                    frames: {
                        '40%': {
                            trans: {
                                scale: 1
                            },
                            styles: {
                                opacity: 1
                            }
                        },
                        '45%': {
                            trans: {
                                scale: 0.9
                            },
                            styles: {
                                opacity: 0.4
                            }
                        },
                        '50%': {
                            trans: {
                                scale: 1
                            },
                            styles: {
                                opacity: 1
                            }
                        }
                    },
                    config: {
                        duration: '4s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                },
                anim01Canaria: {
                    frames: {
                        '42%': {
                            trans: {
                                y: 0
                            }
                        },
                        '46%': {
                            trans: {
                                y: '-26px'
                            }
                        },
                        '52%': {
                            trans: {
                                y: 0
                            }
                        }
                    },
                    config: {
                        duration: '4s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                },
                anim01CanariaShadow: {
                    frames: {
                        '42%': {
                            trans: {
                                scale: 1
                            },
                            styles: {
                                opacity: 1
                            }
                        },
                        '46%': {
                            trans: {
                                scale: 0.9
                            },
                            styles: {
                                opacity: 0.4
                            }
                        },
                        '52%': {
                            trans: {
                                scale: 1
                            },
                            styles: {
                                opacity: 1
                            }
                        }
                    },
                    config: {
                        duration: '4s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            setTimeout(function () {
                $('#anim01Title').fadeIn(500, function() {
                    $(this).a3d(motions.anim01Title);
                });
            }, 50);
            setTimeout(function () {
                $('#anim01Description').fadeIn(400);
            }, 1600);
            $('#anim01SenseiShadow').show().a3d(motions.anim01SenseiShadow);
            $('#anim01Sensei').show().a3d(motions.anim01Sensei);
            $('#anim01CanariaShadow').show().a3d(motions.anim01CanariaShadow);
            $('#anim01Canaria').show().a3d(motions.anim01Canaria);
        });

    },

    /**
     * スライド02
     * @return void
     */
    secondPage: function () {

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            anim02BaloonSensei: anim.imageUrl + '/02_balloon_sensei.png',
            anim02BaloonCanaria: anim.imageUrl + '/02_balloon_canaria.png',
            anim02Title: anim.imageUrl + '/02_title.png',
            anim02Description: anim.imageUrl + '/02_description.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // タイトル
            $('<img>').attr({
                id: 'anim02Title',
                src: imgs.anim02Title
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '32px',
                top: '5px',
                zIndex: 10,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--02');

            // 説明テキスト
            $('<img>').attr({
                id: 'anim02Description',
                src: imgs.anim02Description
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '19px',
                bottom: '20px',
                zIndex: 9,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--02');

            // センセイ吹き出し
            $('<img>').attr({
                id: 'anim02BaloonSensei',
                src: imgs.anim02BaloonSensei
            })
            .css({
                display: 'none',
                position: 'absolute',
                right: '24px',
                top: '220px',
                zIndex: 5,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--02');

            // カナリアちゃん吹き出し
            $('<img>').attr({
                id: 'anim02BaloonCanaria',
                src: imgs.anim02BaloonCanaria
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '20px',
                bottom: '350px',
                zIndex: 5,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--02');

            /**
             * モーションデータ
             */
            var motions = {
                anim02Title: {
                    frames: {
                        '0%': {
                            trans: {
                                scale: 1
                            }
                        },
                        '50%': {
                            trans: {
                                scale: 0.95
                            }
                        },
                        '100%': {
                            trans: {
                                scale: 1
                            }
                        }
                    },
                    config: {
                        duration: '2s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                },
                anim02BaloonSensei: {
                    frames: {
                        '0%': {
                            trans: {
                                scale: 1
                            }
                        },
                        '15%': {
                            trans: {
                                scale: 0.8
                            }
                        },
                        '30%': {
                            trans: {
                                scale: 1
                            }
                        }
                    },
                    config: {
                        duration: '6s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                },
                anim02BaloonCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                scale: 1
                            }
                        },
                        '15%': {
                            trans: {
                                scale: 0.8
                            }
                        },
                        '30%': {
                            trans: {
                                scale: 1
                            }
                        }
                    },
                    config: {
                        duration: '6s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            setTimeout(function () {
                $('#anim02Title').fadeIn(500, function() {
                    $(this).a3d(motions.anim02Title);
                });
            }, 50);
            setTimeout(function () {
                $('#anim02Description').fadeIn(400);
            }, 1000);

            setTimeout(function () {
                $('#anim02BaloonSensei').fadeIn(300, function() {
                    $(this).a3d(motions.anim02BaloonSensei);
                });
            }, 500);
            setTimeout(function () {
                $('#anim02BaloonCanaria').fadeIn(300, function() {
                    $(this).a3d(motions.anim02BaloonCanaria);
                });
            }, 1800);
        });

    },

    /**
     * スライド03
     * @return void
     */
    thirdPage: function () {

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            anim03Title: anim.imageUrl + '/03_title.png',
            anim03Guys: anim.imageUrl + '/03_guys.png',
            anim03Description: anim.imageUrl + '/03_description.png',
            anim03Shuffle: anim.imageUrl + '/03_shuffle.png',
            anim03Coin: anim.imageUrl + '/03_coin.png',
            anim03Coinstack: anim.imageUrl + '/03_coinstack.png'
        };
        coinImg = imgs.anim03Coin;
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // タイトル
            $('<img>').attr({
                id: 'anim03Title',
                src: imgs.anim03Title
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '38px',
                top: '5px',
                zIndex: 10,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--03');

            // コインの山
            $('<img>').attr({
                id: 'anim03Coinstack',
                src: imgs.anim03Coinstack
            })
            .css({
                position: 'absolute',
                bottom: '60px',
                left: '-90px',
                zIndex: 6,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--03');

            // センセイとカナリアちゃん
            $('<img>').attr({
                id: 'anim03Guys',
                src: imgs.anim03Guys
            })
            .css({
                display: 'none',
                position: 'absolute',
                right: '-60px',
                bottom: '90px',
                zIndex: 5,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--03');

            // 抽選
            $('<div>').attr({
                id: 'anim03Shuffle',
                class: 'gq-tuto-shuffle1'
            })
            .css({
                position: 'absolute',
                width: '140px',
                height: '116px',
                left: '25px',
                top: '125px',
                zoom: 0.92,
                zIndex: 4
            })
            .appendTo('.gq-tutorial__carousel--03');

            // 説明テキスト
            $('<img>').attr({
                id: 'anim03Description',
                src: imgs.anim03Description
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '18px',
                bottom: '32px',
                zIndex: 9,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--03');

            /**
             * モーションデータ
             */
            var motions = {
                anim03Title: {
                    frames: {
                        '0%': {
                            trans: {
                                scale: 1
                            }
                        },
                        '50%': {
                            trans: {
                                scale: 0.95
                            }
                        },
                        '100%': {
                            trans: {
                                scale: 1
                            }
                        }
                    },
                    config: {
                        duration: '2s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            $('#anim03Title').fadeIn(500, function() {
                $(this).a3d(motions.anim03Title);
            });
            $('#anim03Guys').show();
            $('#anim03Shuffle').show();

            setTimeout(function () {
                $('#anim03Description').fadeIn(400);
            }, 1000);

            // シャッフル開始
            if(coinFall) {
                clearInterval(coinFall);
                $('.gq-coinfall').detach();
            }
            var tvShuffle = setInterval(function () {
                var rnd = _.random(0, 5) + 1;
                var shuffleClass = 'gq-shuffle__tutorial--' + rnd;
                $('#anim03Shuffle')
                    .removeClass() // 全クラス削除
                    .addClass(shuffleClass);
            }, 150);

            // シャッフルを止めてコインを降らせる
            setTimeout(function () {

                clearInterval(tvShuffle);
                tvShuffle = null;
                $('#anim03Shuffle').removeClass().addClass('gq-shuffle__tutorial--1');

                setTimeout(function () {
                    $('#anim03Shuffle').hide();
                    coinFall = setInterval(function () {
                        anim._coinfall({
                            parent: $('.gq-tutorial__carousel--03'),
                            imageSrc: imgs.anim03Coin
                        });
                    }, 250);
                }, 2000);

            }, 4000);

        });

    },

    /**
     * スライド02
     * @return void
     */
    fourthPage: function () {

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            anim04BaloonSensei: anim.imageUrl + '/04_balloon_sensei.png',
            anim04BaloonCanaria: anim.imageUrl + '/04_balloon_canaria.png',
            anim04Title: anim.imageUrl + '/04_title.png',
            anim04Description: anim.imageUrl + '/04_description.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // タイトル
            $('<img>').attr({
                id: 'anim04Title',
                src: imgs.anim04Title
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '32px',
                top: '5px',
                zIndex: 10,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--04');

            // 説明テキスト
            $('<img>').attr({
                id: 'anim04Description',
                src: imgs.anim04Description
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '37px',
                bottom: '32px',
                zIndex: 9,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--04');

            // センセイ吹き出し
            $('<img>').attr({
                id: 'anim04BaloonSensei',
                src: imgs.anim04BaloonSensei
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '50px',
                bottom: '270px',
                zIndex: 5,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--04');

            // カナリアちゃん吹き出し
            $('<img>').attr({
                id: 'anim04BaloonCanaria',
                src: imgs.anim04BaloonCanaria
            })
            .css({
                display: 'none',
                position: 'absolute',
                right: '54px',
                bottom: '256px',
                zIndex: 5,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--04');

            /**
             * モーションデータ
             */
            var motions = {
                anim04Title: {
                    frames: {
                        '0%': {
                            trans: {
                                scale: 1
                            }
                        },
                        '50%': {
                            trans: {
                                scale: 0.95
                            }
                        },
                        '100%': {
                            trans: {
                                scale: 1
                            }
                        }
                    },
                    config: {
                        duration: '2s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                },
                anim04BaloonSensei: {
                    frames: {
                        '0%': {
                            trans: {
                                y: 0
                            }
                        },
                        '15%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '30%': {
                            trans: {
                                y: 0
                            }
                        }
                    },
                    config: {
                        duration: '3s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                },
                anim04BaloonCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                y: 0
                            }
                        },
                        '15%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '30%': {
                            trans: {
                                y: 0
                            }
                        }
                    },
                    config: {
                        duration: '3s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */

            setTimeout(function () {
                $('#anim04Description').fadeIn(400);
            }, 1000);

            setTimeout(function () {
                $('#anim04Title').fadeIn(500, function() {
                    $(this).a3d(motions.anim04Title);
                });
            }, 50);
            setTimeout(function () {
                $('#anim04BaloonSensei').fadeIn(300, function() {
                    $(this).a3d(motions.anim04BaloonSensei);
                });
            }, 1800);
            setTimeout(function () {
                $('#anim04BaloonCanaria').fadeIn(300, function() {
                    $(this).a3d(motions.anim04BaloonCanaria);
                });
            }, 500);
        });

    },

    /**
     * スライド02
     * @return void
     */
    fifthPage: function () {

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            anim05Halo: anim.imageUrl + '/05_halo.png',
            anim05Title: anim.imageUrl + '/05_title.png',
            anim05Description: anim.imageUrl + '/05_description.png',
            anim05Guys: anim.imageUrl + '/05_guys.png',
            anim05Chevron: anim.imageUrl + '/05_chevron.png',
            anim00Shadow: anim.imageUrl + '/00_shadow.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // 後光
            $('<img>').attr({
                id: 'anim05Halo',
                src: imgs.anim05Halo
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '22px',
                top: '-70px',
                zIndex: 2,
                zoom: 0.5,
                transformOrigin: '50% 44%'
            })
            .appendTo('.gq-tutorial__carousel--05');

            // タイトル
            $('<img>').attr({
                id: 'anim05Title',
                src: imgs.anim05Title
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '178px',
                top: '54px',
                zIndex: 10,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--05');

            // 説明テキスト
            $('<img>').attr({
                id: 'anim05Description',
                src: imgs.anim05Description
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '88px',
                bottom: '70px',
                zIndex: 9,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--05');

            // センセイ影
            $('<img>').attr({
                id: 'anim05SenseiShadow',
                src: imgs.anim00Shadow
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '196px',
                bottom: '150px',
                zIndex: 5,
                zoom: 0.5
            })
            .appendTo('.gq-tutorial__carousel--05');

            // カナリアちゃん影
            $('<img>').attr({
                id: 'anim05CanariaShadow',
                src: imgs.anim00Shadow
            })
            .css({
                display: 'none',
                position: 'absolute',
                right: '154px',
                bottom: '150px',
                zIndex: 5,
                zoom: 0.5
            })
            .appendTo('.gq-tutorial__carousel--05');

            // センセイとカナリアちゃん
            $('<img>').attr({
                id: 'anim05Guys',
                src: imgs.anim05Guys
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '106px',
                bottom: '160px',
                zIndex: 10,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--05');

            // シェブロン
            $('<img>').attr({
                id: 'anim05Chevron',
                src: imgs.anim05Chevron
            })
            .css({
                display: 'none',
                position: 'absolute',
                left: '270px',
                bottom: '20px',
                zIndex: 5,
                zoom: 0.5,
                transformOrigin: '50% 50%'
            })
            .appendTo('.gq-tutorial__carousel--05');

            /**
             * モーションデータ
             */
            var motions = {
                anim05Halo: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '100%': {
                            trans: {
                                rotate: '359deg'
                            }
                        }
                    },
                    config: {
                        duration: '8s',
                        easing: 'linear',
                        count: 'infinite'
                    }
                },
                anim05Title: {
                    frames: {
                        '0%': {
                            trans: {
                                scale: 0
                            }
                        },
                        '100%': {
                            trans: {
                                scale: 1
                            }
                        }
                    },
                    config: {
                        duration: '1s',
                        easing: 'easeOutBack',
                        count: 1
                    }
                },
                anim05Chevron: {
                    frames: {
                        '0%': {
                            trans: {
                                y: '-10px'
                            },
                            styles: {
                                opacity: 1
                            }
                        },
                        '50%': {
                            trans: {
                                y: 0
                            }
                        },
                        '100%': {
                            styles: {
                                opacity: 0
                            }
                        }
                    },
                    config: {
                        duration: '1s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */

            setTimeout(function () {
                $('#anim05Description').fadeIn(400);
            }, 1000);

            setTimeout(function () {
                $('#anim05Halo').fadeIn(500, function() {
                    $(this).a3d(motions.anim05Halo);
                });
            }, 50);
            setTimeout(function () {
                $('#anim05Title').show().a3d(motions.anim05Title);
            }, 60);

            $('#anim05SenseiShadow').show();
            $('#anim05CanariaShadow').show();
            $('#anim05Guys').show();
            setTimeout(function () {
                $('#anim05Chevron').show().a3d(motions.anim05Chevron);
            }, 1800);
        });

    },


    /**
     * 以降、プライベートメソッド扱い
     * ※getter、setterを使用する
     */

    /**
     * 画像先読み
     * @param  {list}    images 画像パスを配列で指定
     * @return {promise}        promiseオブジェクトを返却
     */
    _preloadImages: function(imgs) {

        var promises = [];
        var returnDef = $.Deferred();

        // 画像リストをイテレート開始
        $.each(imgs, function() {
            var img = new Image();
            var defer = $.Deferred();
            // 画像ファイル読み込みイベント
            img.addEventListener('load', function () {
                defer.resolve();
                defer = null;
            });
            // 画像ファイル読み込み失敗イベント
            img.addEventListener('error', function () {
                defer.reject();
                defer = null;
            });
            // 実際の読み込み処理(thisは画像パス)
            img.src = this;
            // 各画像のプロミスオブジェクトを配列にスタック
            promises.push(defer.promise());
        });

        // 結果の評価
        $.when.apply(null, promises)
        .done(function () {
            returnDef.resolve();
        })
        .fail(function () {
            returnDef.reject();
        });

        // スタックしたプロミスオブジェクトを返却
        return returnDef.promise();
    },

    /**
     * 全スライドのアニメーションを停止させる
     *
     * return void
     */
    _setPauseAllAnim: function () {
        _.each([
            '#anim01Title',
            '#anim01Sensei',
            '#anim01Canaria'
        ], function (obj) {
            if($(obj)) {
                $(obj).a3dstate('paused');
            }
        });
        _.each([
            '#anim02Title',
            '#anim02BaloonSensei',
            '#anim02BaloonCanaria'
        ], function (obj) {
            if($(obj)) {
                $(obj).a3dstate('paused');
            }
        });
        _.each([
            '#anim03Title'
        ], function (obj) {
            if($(obj)) {
                $(obj).a3dstate('paused');
            }
        });
        if($('#anim03Shuffle')) {
            $('#anim03Shuffle').detach();
        }
        if(coinFall) {
            clearInterval(coinFall);
            $('.gq-coinfall').detach();
        }
        _.each([
            '#anim04Title',
            '#anim04BaloonSensei',
            '#anim04BaloonCanaria'
        ], function (obj) {
            if($(obj)) {
                $(obj).a3dstate('paused');
            }
        });
        _.each([
            '#anim05Halo',
            '#anim05Title',
            '#anim05Chevron'
        ], function (obj) {
            if($(obj)) {
                $(obj).a3dstate('paused');
            }
        });
        $('#anim05Halo').detach();
    },

    /**
     * コインフォール
     * @param  {object}  params 親エレメント
     * @return void
     */
    _coinfall: function (params) {

        // 回転(0〜360度)
        var rot = _.random(0, 360);
        // スケール(0.6〜1.0)
        var sca = _.random(8, 10) / 10 / 2;
        // 開始位置
        var lft = _.random(0, 260);
        // 落下までのミリ秒数(0.4〜1.0)
        var sec = _.random(15, 20) / 10;

        // モーションデータ
        var motion = {
            frames: {
                '0%': {
                    styles: {
                        top: '-30px',
                    },
                    trans: {
                        scale: sca
                    }
                },
                '100%': {
                    styles: {
                        top: '500px'
                    },
                    trans: {
                        rotate: rot + 'deg',
                        scale: sca
                    }
                }
            },
            config: {
                duration: sec + 's',
                easing: 'linear',
            },
            complete: function () {
                // 終了したら自分自身を消滅させる
                $(this).detach();
            }
        };

        // エレメントにバインド
        $('<img>').attr({
            src: params.imageSrc,
            class: 'gq-coinfall'
        }).css({
            position: 'absolute',
            left: lft,
            zIndex: 4
        }).appendTo(params.parent).a3d(motion);

    },

    /**
     * フォルトトレランス用
     * @return null
     */
    _faultTolerance: function () {
        console.log('404 image not found.');
        console.log(arguments);
    }
};
