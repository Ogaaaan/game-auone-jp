$(function(){

//通知の矢印の高さ
var BrowserWidth = window.innerWidth;
function height_arrow(){
    $('.notification img').css(
        "top",$('.notification').height()/2-5
        );
};
height_arrow();

// -------------------------
// ball
// -------------------------
var ball = $('#ball');
var bounce_ball = $('#bounce-ball');
var explosion = $('#explosion');
var fire =  $('#fire');

function cannon(){
    ball.animate({
        'right': $('.point-back').innerWidth() + 30
    }, 2000,'linear',
    function() {
        explosion.show().delay(1000).fadeOut(2000,
            function(){
                ball.delay(3000).css('right','22px');
                cannon();
            }
            );
    });
};

cannon();


// -------------------------
// ball
// -------------------------

function bounce(){
    bounce_ball.animate({
        "margin-top":"-30px"
    }, 150,"swing",
    function(){
        bounce_ball.animate({
            "margin-top":"0px"
        }, 1000,"easeOutBounce");
    }
    );
};

setInterval(bounce,2000);


// -------------------------
// hero
// -------------------------

var hero_box =  $('#hero-box');
var fire_box =  $('#fire');
var objects = [hero_box,fire_box];
var class_switch = [
['hero-box-attack','hero-box'],
['fire-disappear','fire']
];

var count = 0;

function img_switch(){
    for (i=0;i < objects.length;i++){
        objects[i].attr({
            'class' : class_switch[i][count%2]
        });
    };
    count = count + 1;
};

setInterval(img_switch,1000);


// -------------------------
// car
// -------------------------

var car1 = $('#car1');
var car2 = $('#car2');
var car3 = $('#car3');
var car4 = $('#car4');
var car5 = $('#car5');

function car_drive(car_div,time){
    car_div.animate({
        'marginRight': window.innerWidth
    },
    time,
    'linear',
    function(){
        car_div.css('margin-right','0px');
        car_drive(car_div,time);
    });
};

car_drive(car1,1800);
car_drive(car2,2000);
car_drive(car3,2800);
car_drive(car4,1500);
car_drive(car5,1600);


// -------------------------
// toggle
// -------------------------

$(".toggle").on("click", function() {
    $(this).css(
        "display","none"
        );
    $(this).next().slideToggle();
});

// // -------------------------
// // 文字をたたむ処理
// // -------------------------

// var rank_app_text = $(".index .ranking .app-text p");
// var $target = rank_app_text;
// // オリジナルの文章を取得する
// var html = $target.html();

// function text_resize (){rank_app_text.each(function() {

// var $target = rank_app_text;
// // オリジナルの文章を取得する
// var html = $target.html();
// // 対象の要素を、高さにautoを指定し非表示で複製する
// var $clone = $target.clone();
// $clone
// .css({
//     display: 'none',
//     position : 'absolute',
//     overflow : 'visible'
// })
// .width($target.width())
// .height('auto');

// // DOMを一旦追加
// $target.after($clone);

// // 指定した高さになるまで、1文字ずつ消去していく
// while((html.length > 0) && ($clone.height() > $target.height())) {
//     html = html.substr(0, html.length - 1);
//     $clone.html(html + '...');
// }

// // 文章を入れ替えて、複製した要素を削除する
// $target.html($clone.html());
// $clone.remove();
// });
// };

// text_resize();

$(window).resize(function(){
    // h2_resize();
    // text_resize();
    height_arrow();
    car_drive();
});

});

