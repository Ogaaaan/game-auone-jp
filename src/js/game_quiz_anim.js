/**
 * クイズ本番用アニメーション
 *
 * @author    Seiji Ogawa [s-ogawa@mediba.jp]
 * @copyright Mediba.inc 2015
 */

$(function() {

    /**
     * クイズ結果判定
     */
    if(resultData.ans) {
        // 正解の処理
        anim.correct();
    } else {
        // 不正解の処理
        anim.incorrect();
    }

 });

/**
 * アニメーションステージ内を背景と前景で分ける
 * 前景は背景に対して子要素
 * 前景は常に背景のセンター揃え
 *
 * gq-quiz__result-anim : 前景
 * gq-quiz__result-frame: 背景
 */
 $('<div>')
     .addClass('gq-quiz__result-anim')
     .appendTo('.gq-quiz__result-frame');

/**
 * アニメーションオブジェクト
 *
 * jquery.a3d.jsに依存
 *
 * 各アニメーションエレメントのz-index
 * <ol>
 * <li>背景
 * <li>＜未使用＞
 * <li>後光
 * <li>＜未使用＞
 * <li>抽選のシャッフル画像、あたり画像
 * <li>＜未使用＞
 * <li>＜未使用＞
 * <li>スポットライト、コインフォール、ショックライン
 * <li>＜未使用＞
 * <li>センセイ、カナリアちゃん
 * </ol>
 *
 * @type {Object}
 */
var anim = {
    $background: $('.gq-quiz__result-frame'),
    $foreground: $('.gq-quiz__result-anim'),
    /**
     * 以下環境によって差し替える
     * リポジトリ側へは商用を有効にした状態にしておくこと
     */
    imageUrl: '//cdn-img.auone.jp/game/img/quiz', // 商用
    //imageUrl: '//cdn-img.mdev.auone.jp/game/img/quiz', // mdev
    //imageUrl: '//stg.cdn-img.auone.jp/game/img/quiz', // STG
    //imageUrl: '../../src/img/quiz', // ローカル

    /**
     * 正解
     * @return void
     */
    correct: function() {

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            animBack: anim.imageUrl + '/anim_back_correct.png',
            animSensei: anim.imageUrl + '/anim_sensei_correct.png',
            animCanaria: anim.imageUrl + '/anim_canaria_correct.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // 背景
            $(anim.$background)
            .css({
                backgroundImage: 'url(' + imgs.animBack + ')',
                backgroundSize: '440px 150px',
                backgroundPosition: 'center',
                zIndex: 1
            });

            // センセイ
            $('<img>').attr({
                id: 'animSensei',
                src: imgs.animSensei
            })
            .css({
                left: -58,
                bottom: -45,
                zIndex: 10,
                transformOrigin: '50% 200%'
            })
            .appendTo(anim.$foreground);

            // カナリアちゃん
            $('<img>').attr({
                id: 'animCanaria',
                src: imgs.animCanaria
            })
            .css({
                right: -36,
                bottom: -60,
                zIndex: 10,
                transformOrigin: '50% 200%'
            })
            .appendTo(anim.$foreground);

            /**
             * モーションデータ
             */
            var motions = {
                animSensei: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '-90deg'
                            }
                        },
                        '10%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '30%': {
                            trans: {
                                y: 0
                            }
                        },
                        '35%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '40%': {
                            trans: {
                                y: 0
                            }
                        },
                        '45%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '50%': {
                            trans: {
                                y: 0
                            }
                        },
                        '55%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '60%': {
                            trans: {
                                y: 0
                            }
                        },
                        '90%': {
                            trans: {
                                 rotate: '0deg'
                            }
                        },
                        '100%': {
                            trans: {
                                rotate: '-90deg'
                            }
                        }
                    },
                    config: {
                        duration: '2.5s',
                        easing: 'easeOutBack'
                    },
                    complete: function () {
                        // 抽選開始へ
                        anim._wait(anim.shuffle, 1000);
                    }
                },
                animCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '90deg'
                            }
                        },
                        '10%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '32%': {
                            trans: {
                                y: 0
                            }
                        },
                        '37%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '42%': {
                            trans: {
                                y: 0
                            }
                        },
                        '47%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '52%': {
                            trans: {
                                y: 0
                            }
                        },
                        '57%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '62%': {
                            trans: {
                                y: 0
                            }
                        },
                        '90%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '100%': {
                            trans: {
                                rotate: '90deg'
                            }
                        },
                    },
                    config: {
                        duration: '2.5s',
                        easing: 'easeOutBack'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            $('#animSensei').a3d(motions.animSensei);
            $('#animCanaria').a3d(motions.animCanaria);

        });

    },

    /**
     * 抽選前
     * @return void
     */
    lottery: function() {

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            animBack: anim.imageUrl + '/anim_back_lottery.png',
            animSensei: anim.imageUrl + '/anim_sensei_lottery.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // 背景
            $(anim.$background)
            .css({
                backgroundImage: 'url(' + imgs.animBack + ')'
            });


            // センセイ
            $('#animSensei').attr({
                src: imgs.animSensei
            });

            /**
             * モーションデータ
             */
            var motions = {
                animSensei: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '-90deg'
                            }
                        },
                        '10%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '30%': {
                            trans: {
                                y: 0
                            }
                        },
                        '35%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '40%': {
                            trans: {
                                y: 0
                            }
                        },
                        '45%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '50%': {
                            trans: {
                                y: 0
                            }
                        },
                        '90%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '100%': {
                            trans: {
                                rotate: '-90deg'
                            }
                        }
                    },
                    config: {
                        duration: '2.5s',
                        easing: 'easeOutBack'
                    },
                    complete: function() {
                        // シャッフル画面へ
                        anim._wait(anim.shuffle, 1000);
                    }
                },
                animCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '90deg'
                            }
                        },
                        '10%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '32%': {
                            trans: {
                                y: 0
                            }
                        },
                        '37%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '42%': {
                            trans: {
                                y: 0
                            }
                        },
                        '47%': {
                            trans: {
                                y: '-10px'
                            }
                        },
                        '52%': {
                            trans: {
                                y: 0
                            }
                        },
                        '90%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '100%': {
                            trans: {
                                rotate: '90deg'
                            }
                        },
                    },
                    config: {
                        duration: '2.5s',
                        easing: 'easeOutBack'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            $('#animSensei').a3d(motions.animSensei);
            $('#animCanaria').a3d(motions.animCanaria);

        });

    },

    /**
     * 抽選開始
     * @return void
     */
    shuffle: function () {

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            animBack: anim.imageUrl + '/anim_back_shuffle.png',
            animSpot: anim.imageUrl + '/anim_spotlight.png',
            animSensei: anim.imageUrl + '/anim_sensei_shuffle.png',
            animCanaria: anim.imageUrl + '/anim_canaria_shuffle.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            // スポットライト移動用パラメータ
            var params = {
                sp1: {
                    degree: 0,
                    radius: 320,
                    posX: 150,
                    posY: 150
                },
                sp2: {
                    degree: 180,
                    radius: 320,
                    posX: 190,
                    posY: 190
                }
            };

            // 背景
            $(anim.$background)
            .css({
                backgroundImage: 'url(' + imgs.animBack + ')'
            });


            // 抽選
            $('<div>').attr({
                id: 'animShuffle',
                class: 'gq-shuffle--7'
            })
            .css({
                position: 'absolute',
                width: '140px',
                height: '116px',
                left: '70px',
                top: '10px',
                zIndex: 5
            })
            .appendTo(anim.$foreground);

            // センセイ
            $('#animSensei').attr({
                src: imgs.animSensei
            })
            .css({
                left: -44,
                bottom: -2,
                zIndex: 10,
                transformOrigin: '50% 200%'
            });

            // カナリアちゃん
            $('#animCanaria').attr({
                src: imgs.animCanaria
            })
            .css({
                right: -15,
                bottom: -14,
                zIndex: 10,
                transformOrigin: '50% 200%'
            });

            // スポットライト1
            $('<img>').attr({
                id: 'animSpot1',
                src: imgs.animSpot
            })
            .css({
                left: 0,
                top: 0,
                zIndex: 8,
                transformOrigin: '50% 50%'
            })
            .appendTo(anim.$foreground);

            // スポットライト2
            $('<img>').attr({
                id: 'animSpot2',
                src: imgs.animSpot
            })
            .css({
                right: 0,
                bottom: 0,
                zIndex: 8,
                transformOrigin: '50% 50%'
            })
            .appendTo(anim.$foreground);

            /**
             * モーションデータ
             */
            var motions = {
                animSensei: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '25%': {
                            trans: {
                                rotate: '1deg'
                            }
                        },
                        '50%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '75%': {
                            trans: {
                                rotate: '-1deg'
                            }
                        },
                        '100%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                    },
                    config: {
                        duration: '1s',
                        easing: 'linear',
                        count: 'infinite'
                    }
                },
                animCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '25%': {
                            trans: {
                                rotate: '1deg'
                            }
                        },
                        '50%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '75%': {
                            trans: {
                                rotate: '-1deg'
                            }
                        },
                        '100%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                    },
                    config: {
                        duration: '0.8s',
                        easing: 'linear',
                        count: 'infinite'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            $('#animSensei').a3d(motions.animSensei);
            $('#animCanaria').a3d(motions.animCanaria);

            // シャッフル開始
            var tvShuffle = setInterval(function () {
                var rnd = _.random(0, 5) + 1;
                var shuffleClass = 'gq-shuffle--' + rnd;
                $('#animShuffle')
                    .removeClass() // 全クラス削除
                    .addClass(shuffleClass);
            }, 150);

            // スポットライト1移動開始
            var spLight1 = setInterval(function() {
                // 角度に5を追加
                params.sp1.degree += 5;
                // 360度超えないように360で割った余りにしておく
                params.sp1.degree %= 360;
                // 八の字運動関数からパラメータを取得
                var p = anim._swivel8(
                    params.sp1.degree,
                    params.sp1.radius,
                    params.sp1.posX,
                    params.sp1.posY
                );
                // 取得したパラメータをCSSに適用
                $('#animSpot1').css({
                    left: p.getX(),
                    top:  p.getY()
                });
            }, 45);

            // スポットライト2移動開始
            var spLight2 = setInterval(function() {
                // 角度に5を追加
                params.sp2.degree -= 5;
                // 360度超えないように360で割った余りにしておく
                params.sp2.degree %= 360;
                // 八の字運動関数からパラメータを取得
                var p = anim._swivel8(
                    params.sp2.degree,
                    params.sp2.radius,
                    params.sp2.posX,
                    params.sp2.posY
                );
                // 取得したパラメータをCSSに適用
                $('#animSpot2').css({
                    left: p.getX(),
                    top:  p.getY()
                });
            }, 45);

            // シーン開始4秒後に起動
            setTimeout(function () {

                // スポットライトを止める
                clearInterval(spLight1);
                clearInterval(spLight2);

                // シャッフルを止める
                clearInterval(tvShuffle);
                $('#animShuffle')
                    .removeClass()
                    .addClass('gq-shuffle--7');

                // スポットライトの現在位置から移動距離を抽出
                var targetX = 180;
                var targetY = 40;
                var sp1x = targetX - parseInt($('#animSpot1').css('left'));
                var sp1y = targetY - parseInt($('#animSpot1').css('top'));
                var sp2x = targetX - parseInt($('#animSpot2').css('left'));
                var sp2y = targetY - parseInt($('#animSpot2').css('top'));

                /**
                 * モーションデータ
                 */
                var motions = {
                    spLight1: {
                        frames: {
                            '100%': {
                                trans: {
                                    x: sp1x,
                                    y: sp1y
                                }
                            }
                        },
                        config: {
                            duration: '1s',
                            easing: 'easeOutBack'
                        }
                    },
                    spLight2: {
                        frames: {
                            '100%': {
                                trans: {
                                    x: sp2x,
                                    y: sp2y
                                }
                            },
                        },
                        config: {
                            duration: '1s',
                            easing: 'easeOutBack'
                        },
                        complete: function() {

                            // 抽選の当選判定
                            if(resultData.rank > 0) {
                                // あたり画面へ
                                anim._wait(anim.strike, 1800);
                            } else {
                                // はずれ画面へ
                                anim._wait(anim.miss, 1800);
                            }
                        }
                    }
                };

                /**
                 * モーションをエレメントにバインド
                 */
                $('#animSpot1').a3d(motions.spLight1);
                $('#animSpot2').a3d(motions.spLight2);

            }, 3600);

        });

    },

    /**
     * 抽選あたり
     * @return void
     */
    strike: function () {

        // スポットライトとランクを削除
        $('#animSpot1').detach();
        $('#animSpot2').detach();
        $('#animShuffle').detach();

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            animBack: anim.imageUrl + '/anim_back_strike.png',
            animSensei: anim.imageUrl + '/anim_sensei_strike.png',
            animCanaria: anim.imageUrl + '/anim_canaria_strike.png',
            animHaloStrike: anim.imageUrl + '/anim_halo_strike.png',
            animStrike: anim.imageUrl + '/anim_strike.png',
            animCoin: anim.imageUrl + '/anim_coin_strike.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // 背景
            $(anim.$background)
            .css({
                backgroundImage: 'url(' + imgs.animBack + ')'
            });

            // 後光
            $('<img>').attr({
                id: 'animHaloStrike',
                src: imgs.animHaloStrike
            }).css({
                position: 'absolute',
                width: '266px',
                height: '266px',
                left: '150px',
                top: '0px',
                zIndex: 3,
                transformOrigin: '50% 50%'
            })
            .appendTo(anim.$foreground);

            // あたり
            $('<div>').attr({
                id: 'animStrike',
                class: 'gq-strike--' + resultData.rank
            })
            .css({
                position: 'absolute',
                width: '115px',
                height: '96px',
                left: '85px',
                top: '23px',
                zIndex: 5

            })
            .appendTo(anim.$foreground);

            /**
             * コインを降らせる
             */
            var coinFall = setInterval(function () {
                anim._coinfall({
                    parent: anim.$foreground,
                    imageSrc: imgs.animCoin
                });
            }, 250);

            // センセイ
            $('#animSensei').attr({
                src: imgs.animSensei
            })
            .css({
                left: -52,
                bottom: -45,
                zIndex: 10,
                transformOrigin: '50% 200%'
            });

            // カナリアちゃん
            $('#animCanaria').attr({
                src: imgs.animCanaria
            })
            .css({
                right: -25,
                bottom: -60,
                zIndex: 10,
                transformOrigin: '50% 200%'
            });

            /**
             * モーションデータ
             */
            var motions = {
                animSensei: {
                    frames: {
                        '0%': {
                            trans: {
                                y: 0
                            }
                        },
                        '50%': {
                            trans: {
                                y: '-10'
                            }
                        },
                        '100%': {
                            trans: {
                                y: 0
                            }
                        }
                    },
                    config: {
                        duration: '0.6s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                },
                animCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                y: 0
                            }
                        },
                        '50%': {
                            trans: {
                                y: '-10'
                            }
                        },
                        '100%': {
                            trans: {
                                y: 0
                            }
                        }
                    },
                    config: {
                        duration: '0.5s',
                        easing: 'easeOutBack',
                        count: 10
                    },
                    complete: function () {
                        // コインフォールを停止
                        clearInterval(coinFall);
                        // 最後の画面へ
                        anim._wait(anim.point, 100);
                    }
                },
                animHalo: {
                    frames: {
                        '100%': {
                            trans: {
                                rotate: '360deg'
                            }
                        }
                    },
                    config: {
                        duration: '6s',
                        easing: 'linear',
                        count: 'infinite'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            $('#animHaloStrike').a3d(motions.animHalo);
            $('#animSensei').a3d(motions.animSensei);
            $('#animCanaria').a3d(motions.animCanaria);

        });

    },

    /**
     * ポイントゲット
     * @return void
     */
    point: function () {

        // 不要エレメントを消去
        $('#animHaloStrike').detach();
        $('#animStrike').detach();
        $('.gq-coinfall').detach();

        // 看板入れ替え
        anim._setBanner();

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            animBack: anim.imageUrl + '/anim_back_point.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // 背景
            $(anim.$background)
            .css({
                backgroundImage: 'url(' + imgs.animBack + ')'
            });

            /**
             * ポイント数テキスト
             */
            $('<div>').attr({
                id: 'animPointText'
            })
            .css({
                position: 'absolute',
                left: '96px',
                top: '30px',
                width: '90px',
                height: '30px',
                padding: '6px 0',
                textAlign: 'center',
                fontSize: '20px',
                color: '#ff0',
                fontWeight: 'bold',
                zIndex: 5
            })
            .text(anim._numberFormat(resultData.point))
            .appendTo(anim.$foreground);

            /**
             * モーションデータ
             */
            var motions = {
                animCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                y: 0
                            }
                        },
                        '50%': {
                            trans: {
                                y: '-10'
                            }
                        },
                        '100%': {
                            trans: {
                                y: 0
                            }
                        }
                    },
                    config: {
                        duration: '0.5s',
                        easing: 'easeOutBack',
                        count: 'infinite'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            $('#animCanaria').a3d(motions.animCanaria);

        });

    },

    /**
     * 抽選外れ
     * @return void
     */
    miss: function () {

        // 不要エレメントを消去
        $('#animSpot1').detach();
        $('#animSpot2').detach();
        $('#animShuffle').detach();

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            animBack: anim.imageUrl + '/anim_back_miss.png',
            animSensei: anim.imageUrl + '/anim_sensei_miss.png',
            animCanaria: anim.imageUrl + '/anim_canaria_miss.png',
            animShock: anim.imageUrl + '/anim_shock_miss.png',
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // 背景
            $(anim.$background)
            .css({
                backgroundImage: 'url(' + imgs.animBack + ')'
            });

            // ショックライン
            $('<img>').attr({
                id: 'animShock',
                src: imgs.animShock
            })
            .css({
                position: 'absolue',
                left: 0,
                top: '-50px'
            })
            .appendTo(anim.$background);

            // センセイ
            $('#animSensei').attr({
                src: imgs.animSensei
            })
            .css({
                left: -38,
                bottom: -10,
                zIndex: 10,
                transformOrigin: '50% 200%'
            });

            // カナリヤちゃん
            $('#animCanaria').attr({
                src: imgs.animCanaria
            })
            .css({
                right: -36,
                bottom: -24,
                zIndex: 10,
                transformOrigin: '50% 200%'
            });

            /**
             * モーションデータ
             */
            var motions = {
                animShock: {
                    frames: {
                        '0%': {
                            trans: {
                                y: '-30'
                            }
                        },
                        '100%': {
                            trans: {
                                y: 50
                            }
                        }
                    },
                    config: {
                        duration: '2s',
                        count: 1,
                        easing: 'easeOutBack'
                    },
                    complete: function () {
                        // 明日挑戦画面へ
                        anim._wait(anim.tomorrow, 2000);
                    }
                },
                animSensei: {
                    frames: {
                        '0%': {
                            trans: {
                                y: '150px'
                            }
                        },
                        '40%': {
                            trans: {
                                y: '0px'
                            }
                        },
                        '59%': {
                            trans: {
                                x: 0
                            }
                        },
                        '60%': {
                            trans: {
                                x: -4
                            }
                        },
                        '62%': {
                            trans: {
                                x: 0
                            }
                        },
                        '64%': {
                            trans: {
                                x: 4
                            }
                        },
                        '66%': {
                            trans: {
                                x: 0
                            }
                        },
                        '68%': {
                            trans: {
                                x: -4
                            }
                        },
                        '70%': {
                            trans: {
                                x: 0
                            }
                        },
                        '72%': {
                            trans: {
                                x: 4
                            }
                        },
                        '74%': {
                            trans: {
                                x: 0
                            }
                        },
                        '76%': {
                            trans: {
                                x: -4
                            }
                        },
                        '78%': {
                            trans: {
                                x: 0
                            }
                        },
                        '80%': {
                            trans: {
                                x: 4
                            }
                        },
                        '82%': {
                            trans: {
                                x: 0
                            }
                        },
                        '84%': {
                            trans: {
                                x: -4
                            }
                        },
                        '86%': {
                            trans: {
                                x: 0
                            }
                        },
                        '88%': {
                            trans: {
                                x: 4
                            }
                        },
                        '90%': {
                            trans: {
                                x: 0
                            }
                        },
                        '92%': {
                            trans: {
                                x: -4
                            }
                        },
                        '94%': {
                            trans: {
                                x: 0
                            }
                        },
                        '96%': {
                            trans: {
                                x: 4
                            }
                        },
                        '98%': {
                            trans: {
                                x: 0
                            }
                        }
                    },
                    config: {
                        duration: '2.5s',
                        count: 1,
                        easing: 'easeOutBack'
                    }
                },
                animCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                y: '150px'
                            }
                        },
                        '40%': {
                            trans: {
                                y: 0
                            }
                        },
                        '59%': {
                            trans: {
                                x: 0
                            }
                        },
                        '62%': {
                            trans: {
                                x: -4
                            }
                        },
                        '64%': {
                            trans: {
                                x: 0
                            }
                        },
                        '66%': {
                            trans: {
                                x: 4
                            }
                        },
                        '68%': {
                            trans: {
                                x: 0
                            }
                        },
                        '70%': {
                            trans: {
                                x: -4
                            }
                        },
                        '72%': {
                            trans: {
                                x: 0
                            }
                        },
                        '74%': {
                            trans: {
                                x: 4
                            }
                        },
                        '76%': {
                            trans: {
                                x: 0
                            }
                        },
                        '78%': {
                            trans: {
                                x: -4
                            }
                        },
                        '80%': {
                            trans: {
                                x: 0
                            }
                        },
                        '82%': {
                            trans: {
                                x: 4
                            }
                        },
                        '84%': {
                            trans: {
                                x: 0
                            }
                        },
                        '86%': {
                            trans: {
                                x: -4
                            }
                        },
                        '88%': {
                            trans: {
                                x: 0
                            }
                        },
                        '90%': {
                            trans: {
                                x: 4
                            }
                        },
                        '92%': {
                            trans: {
                                x: 0
                            }
                        },
                        '94%': {
                            trans: {
                                x: -4
                            }
                        },
                        '96%': {
                            trans: {
                                x: 0
                            }
                        },
                        '98%': {
                            trans: {
                                x: 4
                            }
                        },
                        '100%': {
                            trans: {
                                x: 0
                            }
                        }
                    },
                    config: {
                        duration: '2.5s',
                        count: 1,
                        easing: 'easeOutBack'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            $('#animShock').a3d(motions.animShock);
            $('#animSensei').a3d(motions.animSensei);
            $('#animCanaria').a3d(motions.animCanaria);

        });

    },

    /**
     * 明日も挑戦
     * @return void
     */
    tomorrow: function () {

        // 不要エレメントを消去
        $('#animShock').detach();

        // 看板入れ替え
        anim._setBanner();

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            animBack: anim.imageUrl + '/anim_back_tomorrow.png',
            animTomorrow: anim.imageUrl + '/anim_tomorrow.png',
            animSensei: anim.imageUrl + '/anim_sensei_tomorrow.png',
            animCanaria: anim.imageUrl + '/anim_canaria_correct.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // 背景
            $(anim.$background)
            .css({
                backgroundImage: 'url(' + imgs.animBack + ')'
            });

            // メッセージ
            $('<img>').attr({
                id: 'animTomorrow',
                src: imgs.animTomorrow
            })
            .css({
                position: 'absolute',
                left: '160px',
                top: '68px',
                zIndex: 8
            })
            .appendTo(anim.$foreground);

            // センセイ
            $('#animSensei').attr({
                src: imgs.animSensei
            })
            .css({
                left: -58,
                bottom: -45,
                zIndex: 10,
                transformOrigin: '50% 200%'
            });

            // カナリヤちゃん
            $('#animCanaria').attr({
                src: imgs.animCanaria
            })
            .css({
                right: -36,
                bottom: -60,
                zIndex: 10,
                transformOrigin: '50% 200%'
            });

            /**
             * モーションデータ
             */
            var motions = {
                animTomorrow: {
                    frames: {
                        '0%': {
                            trans: {
                                scale: 0
                            }
                        },
                        '100%': {
                            trans: {
                                scale: 0.95
                            }
                        }
                    },
                    config: {
                        duration: '0.5s',
                        easing: 'easeOutBack'
                    }
                },
                animSensei: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '-45deg'
                            }
                        },
                        '12%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '70%': {
                            trans: {
                                y: 0
                            }
                        },
                        '80%': {
                            trans: {
                                y: -15
                            }
                        },
                        '90%': {
                            trans: {
                                y: 0
                            }
                        }
                    },
                    config: {
                        duration: '1.5s',
                        easing: 'easeOutBack'
                    }
                },
                animCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '45deg'
                            }
                        },
                        '12%': {
                            trans: {
                                rotate: '0deg'
                            }
                        },
                        '73%': {
                            trans: {
                                y: 0
                            }
                        },
                        '83%': {
                            trans: {
                                y: -15
                            }
                        },
                        '93%': {
                            trans: {
                                y: 0
                            }
                        }
                    },
                    config: {
                        duration: '1.5s',
                        easing: 'easeOutBack'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            $('#animTomorrow').a3d(motions.animTomorrow);
            $('#animSensei').a3d(motions.animSensei);
            $('#animCanaria').a3d(motions.animCanaria);

        });

    },

    /**
     * 不正解
     *
     */
    incorrect: function () {

        /**
         * 画像プリロード
         */

        // 画像セット
        var imgs = {
            animBack: anim.imageUrl + '/anim_back_incorrect.png',
            animSensei: anim.imageUrl + '/anim_sensei_incorrect.png',
            animCanaria: anim.imageUrl + '/anim_canaria_incorrect.png'
        };
        // 読み込み開始
        var promise = anim._preloadImages(imgs);
        // 読み込み失敗時
        promise.fail(function () {

            // エラー時の退避処理
            anim._faultTolerance();

        });
        // 読み込み成功時
        promise.done(function () {

            /**
             * アニメーションエレメント
             */

            // 背景
            $(anim.$background)
            .css({
                backgroundImage: 'url(' + imgs.animBack + ')',
                backgroundSize: '440px 150px',
                backgroundPosition: 'center',
                zIndex: 1
            });

            // センセイ
            $('<img>').attr({
                id: 'animSensei',
                src: imgs.animSensei
            })
            .css({
                left: -58,
                bottom: -45,
                transformOrigin: '50% 200%',
                zIndex: 10
            })
            .appendTo(anim.$foreground);

            // カナリアちゃん
            $('<img>').attr({
                id: 'animCanaria',
                src: imgs.animCanaria
            })
            .css({
                right: -36,
                bottom: -62,
                transformOrigin: '50% 200%',
                zIndex: 10
            })
            .appendTo(anim.$foreground);

            /**
             * モーションデータ
             */
            var motions = {
                animSensei: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '-90deg'
                            }
                        },
                        '100%': {
                            trans: {
                                rotate: '0deg'
                            }
                        }
                    },
                    complete: function () {
                        // 明日挑戦画面へ
                        anim._wait(anim.tomorrow, 3000);
                    },
                    config: {
                        duration: '0.8s',
                        easing: 'easeOutBack'
                    }
                },
                animCanaria: {
                    frames: {
                        '0%': {
                            trans: {
                                rotate: '90deg'
                            }
                        },
                        '100%': {
                            trans: {
                                rotate: '0deg'
                            }
                        }
                    },
                    config: {
                        duration: '0.8s',
                        easing: 'easeOutBack'
                    }
                }
            };

            /**
             * モーションをエレメントにバインド
             */
            $('#animSensei').a3d(motions.animSensei);
            $('#animCanaria').a3d(motions.animCanaria);

        });

    },

    /**
     * 以降、プライベートメソッド扱い
     * ※getter、setterを使用する
     */

    /**
     * 画像先読み
     * @param  {list}    images 画像パスを配列で指定
     * @return {promise}        promiseオブジェクトを返却
     */
    _preloadImages: function(imgs) {

        var promises = [];
        var returnDef = $.Deferred();

        // 画像リストをイテレート開始
        $.each(imgs, function() {
            var img = new Image();
            var defer = $.Deferred();
            // 画像ファイル読み込みイベント
            img.addEventListener('load', function () {
                defer.resolve();
                defer = null;
            });
            // 画像ファイル読み込み失敗イベント
            img.addEventListener('error', function () {
                defer.reject();
                defer = null;
            });
            // 実際の読み込み処理(thisは画像パス)
            img.src = this;
            // 各画像のプロミスオブジェクトを配列にスタック
            promises.push(defer.promise());
        });

        // 結果の評価
        $.when.apply(null, promises)
        .done(function () {
            returnDef.resolve();
        })
        .fail(function () {
            returnDef.reject();
        });

        // スタックしたプロミスオブジェクトを返却
        return returnDef.promise();
    },

    /**
     * 待機
     * @param  {Function} callback 待機後に実行する関数
     * @param  {Integer}   time     待機するミリ秒
     * @return void
     */
    _wait: function (callback, time) {
        setTimeout(callback, time);
    },

    /**
     * スプライト画像用シャッフルインデックス生成
     * @param  {integer} loop 1〜5位までを何回シャッフルさせるか
     * @return {object}       s
     */
    _shuffle: function (loop) {
        var frameIndexes = [];
        for(var i = 0; i < loop; i++) {
            b = _.shuffle(_.range(0, 6));
            frameIndexes = frameIndexes.concat(b);
        }
        return {
            getIndexList: function () {
                return frameIndexes;
            }
        };
    },

    /**
     * 八の字運動
     * @param  {integer} degree 現在の角度
     * @param  {integer} radius 運動範囲の半径
     * @param  {integer} posX   現在のX座標
     * @param  {integer} posY   現在のY座標
     * @return {object}         次のX座標とY座標
     */
    _swivel8: function (degree, radius, posX, posY) {
        var radian = Math.PI / 180 * degree;
        var _x = posX + radius * Math.cos(radian);
        var _y = posY + radius * Math.sin(radian + radian);
        return {
            getX: function () {
                return _x;
            },
            getY: function () {
                return _y;
            }
        };
    },

    /**
     * コインフォール
     * @param  {object}  params 親エレメント
     * @return void
     */
    _coinfall: function (params) {

        // 回転(0〜360度)
        var rot = _.random(0, 180);
        // スケール(0.6〜1.0)
        var sca = _.random(5, 8) / 10;
        // 開始位置
        var lft = _.random(0, 540);
        // 落下までのミリ秒数(0.4〜1.0)
        var sec = _.random(8, 12) / 10;

        // モーションデータ
        var motion = {
            frames: {
                '0%': {
                    trans: {
                        scale: sca
                    },
                    styles: {
                        top: '-30px'
                    }
                },
                '100%': {
                    trans: {
                        rotate: rot + 'deg',
                        scale: sca
                    },
                    styles: {
                        top: '300px'
                    }
                }
            },
            config: {
                duration: sec + 's',
                easing: 'linear',
            },
            complete: function () {
                // 終了したら自分自身を消滅させる
                $(this).detach();
            }
        };

        // エレメントにバインド
        $('<img>').attr({
            src: params.imageSrc,
            class: 'gq-coinfall'
        }).css({
            position: 'absolute',
            left: lft,
            zIndex: 8
        }).appendTo(params.parent).a3d(motion);

    },

    /**
     * 3桁毎にカンマを追加する
     * @param  {string} str 対象文字列
     * @return {string}     カンマ付きの文字列
     */
    _numberFormat: function (str) {
        var num = String(str).replace(/,/g, '');
        while(num !== (num = num.replace(/^(-?\d+)(\d{3})/, '$1,$2')));
        return num;
    },

    /**
     * フォルトトレランス用
     * @return null
     */
    _faultTolerance: function () {
        console.log('404 image not found.');
        console.log(arguments);
    },

    /**
     * アニメ終了後に看板を差し替える
     * @return void
     */
    _setBanner: function () {
        $('.gq-quiz__lottery_bnr').css({
            display: 'none'
        });
        $('.gq-quiz__challenge_bnr').css({
            display: 'block',
            margin: '0 auto 10px'
        });
    }

};
