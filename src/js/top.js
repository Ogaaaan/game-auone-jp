$(function(){
    $.when(
        $.ajax({
            // url : "sp/app/game_ajax_contents.html",//local(gulpfileから見た階層)
            // url : "./app/game_ajax_contents.html",//pixeltoy
            url : "/app/game_ajax_contents.html",//stg,dev,商用
            dataType: "html",
            async: true,
            cache: false
        })
    ).done(function(contentsData){
        $('.js-ajax_contents').html(contentsData);
    })
    .fail(function() {
        var errorText = '<p>コンテンツを取得できませんでした。</p>';
        $('.js-ajax_contents').html(errorText);
    });
});
