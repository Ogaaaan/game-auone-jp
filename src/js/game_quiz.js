/**
 * ゲームクイズ用js
 *
 * @author    Seiji Ogawa [s-ogawa@mediba.jp]
 * @copyright Mediba.inc 2015
 */

$(function () {

    /**
     * ボット表示処理
     * 古い機種で処理時間かかりすぎて
     * ボットが表示されないことがあるので対策
     */
    var botShow = setInterval(function () {
        var $bot = $('.js-t-drawer__bot');
        if($bot[0]) {
            clearInterval(botShow);
            $bot.css({
                display: 'block',
                visibility: 'visible'
            });
        }
    }, 500);

    /**
     * 参加ルールボタンのクリックイベント
     * 参加ルール一覧の高さをアニメーションでトグルさせる
     */
    var $quizTerms = $('.js-gq-terms');
    var $btnArrow = $('.js-gq-btn__rule').children('span');

    $('.js-gq-btn__rule').on('click', function() {
        if($quizTerms.is(':hidden')) {
            $btnArrow.removeClass('gq-btn__arrow--down').addClass('gq-btn__arrow--up');
        } else {
            $btnArrow.removeClass('gq-btn__arrow--up').addClass('gq-btn__arrow--down');
        }
        $quizTerms.animate({
            height: 'toggle'
        }, 'slow' );
    });

    /**
     * クイズページで解答を選択した際の処理
     * 選択された解答以外をダークにし、解答ボタンを使用可能にする
     */
    $('.js-gq-hidden-form').on('click', function() {
        // 選択肢
        $('.gq-btn__ans-base').addClass('unselected');
        $(this).parent('label').removeClass('unselected');
        // 解答ボタン
        $('.gq-btn__ans-submit').show();
        $('.gq-btn__ans-submit--disabled').hide();
    });

    /**
     * チュートリアルページ閲覧フラグ確認
     * ボタンの表示位置変更
     */
    if(!$.cookie('tutorial')) {
        // クッキーがない場合
        var $tutorialBanner = $('.js-gq-banner--tutorial').clone();
        $('.js-gq-banner--tutorial').detach();
        $('.gq-main-title').after(
            $tutorialBanner
                .clone(true)
                .show()
        );
    } else {
        // クッキーがある場合
        $('.js-gq-banner--tutorial').show();
    }

});

/**
 * ローディング完了後の処理
 * @return [type] [description]
 */
function clearOverlay() {
    $('.js-gq-loading').remove();
    $('.js-t-drawer__bot').css({
        visibility: 'visible'
    });
}
