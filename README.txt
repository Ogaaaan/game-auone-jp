==============================================================================
0722 16:30 update -----------------------------

①ファイル構成について
②set.phpについて
③画像パスについて

--------------------------------------------------------
①ファイル構成について
--------------------------------------------------------
↓html代わりのphpです。
htdocs/sp/app/lp/dlcam/_template/index.php
/_template/フォルダごとコピーして、
htdocs/sp/app/lp/dlcam/任意のフォルダ名/index.php
のindex.phpを編集してください。
"任意のフォルダ名"のところは各キャンペーン名で適宜作成してください。
（ひとつのフォルダにひとつのindex.phpを格納してください）
共通パーツを一元管理するために、phpでincludeさせています。
includeされている中身は、
htdocs/sp/app/inc/内のフォルダに格納されていますので、
それぞれご確認ください。

↓画像ファイルはココに格納してください。
htdocs/src/img/lp/dlcam/任意のフォルダ名/～

↓cssやjsを外部ファイル化する場合はココに格納してください。
htdocs/src/css/～
htdocs/src/js/～


--------------------------------------------------------
②set.phpについて
--------------------------------------------------------
下記のset.phpの2か所、
「★★★確認環境のドメイン★★★」の部分を、
御社で利用されている確認用テストサーバーのドメインに書き換えてください。
以下、ご参考ください。
　↓　↓　↓
<?php
/* htmlへ記入するためのglobal設定 */
$PATH;
/* url出し分け */
if ($_SERVER['HTTP_HOST'] == '★★★確認環境のドメイン★★★') {
    /* 確認用テストサーバー */
    $PATH['url_game'] = '//★★★確認環境のドメイン★★★/htdocs/src/';
    $PATH['url_tentomushi'] = '//cdn-img.auone.jp/pass/asset/tentomushi/';
} elseif (strpos($_SERVER['HTTP_HOST'], 'stg.game') !== false
|| strpos($_SERVER['HTTP_HOST'], 'dev.game') !== false) {
    /*dev or ステージング環境*/
    $PATH['url_game'] = '//cdn-img.mdev.auone.jp/pass/asset/sp/game/';
    $PATH['url_tentomushi'] = '//cdn-img.mdev.auone.jp/pass/asset/tentomushi/';
} else {
    /*デフォルト商用 */
    $PATH['url_game'] = '//cdn-img.auone.jp/pass/asset/sp/game/';
    $PATH['url_tentomushi'] = '//cdn-img.auone.jp/pass/asset/tentomushi/';
}

function inc($file){
    global $PATH;
    include(dirname(__FILE__) . "/" . $file . ".php");
}

?>

--------------------------------------------------------
③画像のパスについて
--------------------------------------------------------
↓以下を参考に「src」の指定と、「class」の指定をしてください。
<img src="<?php print($PATH['url_game']);?>img/lp/dlcam/xxxx/dammy.png" class="img_width-full">

②で指定した、
「★★★確認環境のドメイン★★★」と、
弊社でアップロードしているテスト用の画像サーバーと、商用の画像サーバーが、
それぞれ、環境に応じて書き換えられるようにしています。
