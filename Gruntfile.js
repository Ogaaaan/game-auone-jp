/**
 * auゲーム用Gruntfile
 *
 * @author    Seiji Ogawa [s-ogawa@mediba.jp]
 * @copyright Mediba.inc 2015
 */
'use strict';

module.exports = function(grunt) {

    var pkg = grunt.file.readJSON('package.json');
    var taskName = '';

    // 各パッケージ設定
    grunt.initConfig({
        pkg: pkg,
        dir: {
                js: 'src/js',
                scss: 'src/scss',
                css: 'src/css'
        },
        uglify: {
            dist: {
                files: {
                    '<%= dir.js %>/game_quiz.min.js': '<%= dir.js %>/game_quiz.js',
                    '<%= dir.js %>/game_quiz_anim.min.js': '<%= dir.js %>/game_quiz_anim.js',
                    '<%= dir.js %>/game_quiz_anim_prac.min.js': '<%= dir.js %>/game_quiz_anim_prac.js',
                    '<%= dir.js %>/game_quiz_anim_tuto.min.js': '<%= dir.js %>/game_quiz_anim_tuto.js',
                    '<%= dir.js %>/jquery.a3d.min.js': '<%= dir.js %>/jquery.a3d.js'
                }
            }
        },
        compass: {
            dist: {
                options: {
                    config: 'config.rb'
                }
            }
        },
        cssmin: {
            dist: {
                src: '<%= dir.css %>/game_quiz.css',
                dest: '<%= dir.css %>/game_quiz.min.css'
            }
        },
        watch: {
            uglify: {
                files: [
                    '<%= dir.js %>/game_quiz.js',
                    '<%= dir.js %>/game_quiz_anim.js',
                    '<%= dir.js %>/game_quiz_anim_prac.js',
                    '<%= dir.js %>/game_quiz_anim_tuto.js',
                    '<%= dir.js %>/jquery.a3d.js'
                ],
                tasks: 'uglify'
            },
            compass: {
                files: '<%= dir.scss %>/game_quiz.scss',
                tasks: 'compass'
            },
            cssmin: {
                files: '<%= dir.css %>/game_quiz.css',
                tasks: 'cssmin'
            }
        }
    });

    // package.jsonを元にモジュールを読み込む
    for (taskName in pkg.devDependencies) {
        if (taskName.substring(0, 6) === 'grunt-') {
            grunt.loadNpmTasks(taskName);
        }
    }

    // タスクの設定
    grunt.registerTask('default', 'watch');
    grunt.registerTask('release', ['uglify', 'compass', 'cssmin']);

};
