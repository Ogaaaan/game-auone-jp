<?php include("../../inc/set.php"); ?>
<!DOCTYPE html>
<html lang="ja_JP">
<head>
    <meta charset="UTF-8">
    <title>「au SHINJUKU」グッズプレゼントキャンペーン - auゲーム</title>
    <meta name="keywords" content="サンプルキャンペーン,au SHINJUKU,au新宿,モンスターストライク,モンスト,白猫プロジェクト,グッズプレゼント,ポイントゲーム,スマパス,auスマートパス" />
    <meta name="description" content="2015年8月21日～23日の3日間、「au SHINJUKU」にご来場いただいた方を対象にモンスターストライク、白猫プロジェクトのグッズをプレゼント！" />
    <link rel="shortcut icon" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="//cdn-img.auone.jp/pass/asset/sp/common/img/favicon/favicon.png">
    <?php inc("head"); ?>
    <link rel="stylesheet" href="<?php print($PATH['url_game']);?>css/lp_201410.css">
</head>

<body class="all-app">
    <div class="js-t-wrapper">
        <div class="contens-body">

            <?php inc("header"); ?>
            <div class="t-ad--sp"><?php inc("adSP_iOS"); ?></div>

            <div class="all-games special-games">
                <!-- operation  -->
                <div class="container-min lp_201410">
                    <div class="pb_15">
                        <div class="top-title">
                            <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/lp_20150821_main.png" class="img_width-full">
                        </div>

                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png">
                                </div>
                                <h3 style="text-indent:5px;">キャンペーン期間</h3>
                            </div>
                            <div class="pd_10"><span>
                                8/21(金)から8/23(日)の各日、下記の時間帯にて開催いたします。<br>
                                ・8/21(金) 12:00～19:00<br>
                                ・8/22(土) 10:00～19:00<br>
                                ・8/23(日) 10:00～19:00<br>
                            </span>
                            </div>
                        </div>
                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png">
                                </div>
                                <h3 style="text-indent:5px;">受け取り条件</h3>
                            </div>
                            <div class="pd_10"><span>
                                キャンペーン期間中、「au SHINJUKU」にご来店された全てのお客様にプレゼントいたします。
                            </span>
                            </div>
                        </div>
                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png">
                                </div>
                                <h3 style="text-indent:5px;">開催場所</h3>
                            </div>
                            <div class="pd_10"><span>「au SHINJUKU」店</span>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3240.371992754929!2d139.70205789999997!3d35.6924625!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5573d9090da77769!2sau+SHINJUKU!5e0!3m2!1sja!2sjp!4v1439367356268" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen class="mt_10"></iframe>
                            </div>
                        </div>


                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png">
                                </div>
                                <h3 style="text-indent:5px;">グッズのご案内</h3>
                            </div>
                            <div class="pd_10">
                                <div class="t-grid">
                                    <ul class="t-grid__2columns">
                                        <li class="t-grid__column">
                                            <div class="t-grid__inner">
                                                <span class="gift-title" style="font-size:11px;">モンスターストライク<br>クリアファイル</span>
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/lp_shinjukucam_monsto.png" style="width: 100%; height:auto;">
                                            </div>
                                        </li>
                                        <li class="t-grid__column">
                                            <div class="t-grid__inner">
                                                <span class="gift-title" style="font-size:11px;">白猫プロジェクト<br>ポストカード(2枚1セット)</span>
                                                <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/lp/lp_shinjukucam_sironeko.png" style="width: 100%; height:auto;">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            <span>
                                ※プレゼントはお一人様1つまでとさせて頂きます。
                            </span>
                            </div>
                        </div>

                        <div class="list" style="margin-top:10px;">
                            <div class="headline">
                                <div class="middle">
                                    <img src="//cdn-img.auone.jp/pass/asset/sp/game/img/gift.png">
                                </div>
                                <h3 style="text-indent:5px;">グッズの配布状況について</h3>
                            </div>
                            <div class="pd_10"><span>
                                「au SHINJUKU」公式twitterにて、ご確認いただけます。<br>
                                <div class="t-link">
                                    <a href="https://twitter.com/auSHINJUKU">こちらから</a>
                                </div>
                            </span>
                            </div>
                        </div>

                    </div>
                </div><!-- container-min lp_201410--><!-- Bg_orange -->
            </div><!-- all-games special-games-->

            <div class="all-games ranking container-min lp_201410">
                <div class="list mb_10">
                    <div class="special-gift">
                        <div class="headline">注意事項</div>
                        <div class="pd_10">
                            <span>
                                ・数量に限りがございますため、ご了承の程お願いいたします。
                            </span>
                        </div>
                    </div>
                </div>
                <?php inc("adRect_iOS"); ?>
            </div><!--all-games ranking container-min lp_201410-->
        </div><!--contens-body-->

        <?php inc("footer"); ?>

    </div><!--js-t-wrapper-->
    <?php inc("script"); ?>
    <?php inc("gtm"); ?>

</body>
</html>
