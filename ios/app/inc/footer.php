    <!-- footer -->
    <footer>
        <ul class="t-footer__nav">
            <li>
                <a href="http://pass.auone.jp/pass_help/?aspref=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','ヘルプ',event);">ヘルプ</a>
                <a href="//game.auone.jp/app/faq/" onclick="trEventBe(this,'auゲーム','トップページ','FAQ',event);">FAQ</a>
            </li>
            <li>
                <a href="http://www.mediba.jp/privacy/sp/advertising.html" onclick="trEventBe(this,'auゲーム','トップページ','広告について',event);">広告について</a>
                <a href="http://pass.auone.jp/inquiry/?aspref=gameportal" onclick="trEventBe(this,'auゲーム','トップページ','お問い合わせ',event);">お問い合わせ</a>
            </li>
        </ul>
    </footer>

    <footer class="t-footer__copyright">
        <small>©KDDI</small>
    </footer>
    <!-- /footer -->
